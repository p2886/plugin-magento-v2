<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Api\Data;

interface IssueReporterInterface
{
    const ENTITY_ID = 'entity_id';
    const DESCRIPTION = 'description';
    const COPY_TO = 'copy_to';
    const CREATED_AT = 'created_at';
    const DATE_SENT = 'date_sent';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return string
     */
    public function getCopyTo(): string;

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * @return string|null
     */
    public function getDateSent(): ?string;

    /**
     * @param int $value
     * @return IssueReporterInterface
     */
    public function setId($value);

    /**
     * @param string|null $description
     * @return IssueReporterInterface
     */
    public function setDescription(?string $description): IssueReporterInterface;

    /**
     * @param string $copyTo
     * @return IssueReporterInterface
     */
    public function setCopyTo(string $copyTo): IssueReporterInterface;

    /**
     * @param string|null $createdAt
     * @return IssueReporterInterface
     */
    public function setCreatedAt(?string $createdAt): IssueReporterInterface;

    /**
     * @param string|null $dateSent
     * @return IssueReporterInterface
     */
    public function setDateSent(?string $dateSent): IssueReporterInterface;
}
