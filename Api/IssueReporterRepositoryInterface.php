<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use PeachPayments\Hosted\Api\Data\IssueReporterInterface;

interface IssueReporterRepositoryInterface
{
    /**
     * @param IssueReporterInterface $issueReporter
     * @return IssueReporterInterface
     * @throws CouldNotSaveException
     */
    public function save(IssueReporterInterface $issueReporter): IssueReporterInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * @param int $entityId
     * @return IssueReporterInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $entityId): IssueReporterInterface;
}
