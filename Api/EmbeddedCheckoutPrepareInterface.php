<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2023 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Api;

/**
 * @api
 */
interface EmbeddedCheckoutPrepareInterface
{
    /**
     * Create embedded checkout.
     *
     * @param  string $cartId
     * @return string
     */
    public function execute(string $cartId): string;
}
