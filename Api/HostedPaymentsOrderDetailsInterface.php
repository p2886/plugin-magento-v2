<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

namespace PeachPayments\Hosted\Api;

/**
 * @api
 */
interface HostedPaymentsOrderDetailsInterface
{
    /**
     * Retrieve all required order data to successfully place order on peach side.
     *
     * @return string
     */
    public function execute(): string;
}
