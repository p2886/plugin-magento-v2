<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Api;

/**
 * @api
 */
interface AvailableHostedPaymentMethodsTitleInterface
{
    /**
     * Retrieve a list of payment methods
     *
     * @return string
     */
    public function execute(): string;
}
