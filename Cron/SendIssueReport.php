<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Cron;

use DateTime;
use DateTimeZone;
use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\DataObject;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Io\File;
use PeachPayments\Hosted\Api\Data\IssueReporterInterface;
use PeachPayments\Hosted\Api\IssueReporterRepositoryInterface;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\IssueReporter;
use PeachPayments\Hosted\Model\ResourceModel\IssueReporter\Collection;
use PeachPayments\Hosted\Model\ResourceModel\IssueReporter\CollectionFactory;
use PeachPayments\Hosted\Service\MagentoDataCollection;
use PeachPayments\Hosted\Service\SendIssueReport as SendIssueReportService;

class SendIssueReport
{
    /**
     * @var MagentoDataCollection
     */
    private $dataCollectionService;

    /**
     * @var SendIssueReportService
     */
    private $sendIssueReportService;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var File
     */
    private $file;

    /**
     * @var IssueReporterRepositoryInterface
     */
    private $issueReporterRepository;

    /**
     * @param MagentoDataCollection $dataCollectionService
     * @param SendIssueReportService $sendIssueReportService
     * @param CollectionFactory $collectionFactory
     * @param Filesystem $filesystem
     * @param File $file
     * @param IssueReporterRepositoryInterface $issueReporterRepository
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        MagentoDataCollection $dataCollectionService,
        SendIssueReportService $sendIssueReportService,
        CollectionFactory $collectionFactory,
        Filesystem $filesystem,
        File $file,
        IssueReporterRepositoryInterface $issueReporterRepository,
        private UnifiedLogger $unifiedLogger
    ) {
        $this->dataCollectionService = $dataCollectionService;
        $this->sendIssueReportService = $sendIssueReportService;
        $this->collectionFactory = $collectionFactory;
        $this->filesystem = $filesystem;
        $this->file = $file;
        $this->issueReporterRepository = $issueReporterRepository;
    }

    /**
     * @return void
     */
    public function execute()
    {
        /** @var IssueReporter $item */
        $item = $this->getIssueReporterItem();

        if (!$item->getEntityId()) {
            return;
        }

        $logDir = $this->filesystem->getDirectoryRead(DirectoryList::LOG)->getAbsolutePath();
        $archivePath = $logDir . 'logs_and_modules.zip';

        try {
            $data = $this->dataCollectionService->processCollection($item, $archivePath, $logDir);

            if ($data) {
                $this->sendIssueReportService->sendEmail($data, $item->getCopyTo(), $archivePath);
            }

            $currentDateTime = new DateTime('now', new DateTimeZone('UTC'));
            $item->setDateSent($currentDateTime->format('Y-m-d H:i:s'));
            $this->issueReporterRepository->save($item);
        } catch (Exception $e) {
            $this->unifiedLogger->critical(
                __('Error during sending issue report to Peach Payments. Exception: %1', $e->getMessage())
            );
        }

        if (file_exists($archivePath)) {
            $this->file->rm($archivePath);
        }
    }

    /**
     * @return DataObject
     */
    private function getIssueReporterItem(): DataObject
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter(IssueReporterInterface::DATE_SENT, ['null' => true]);

        return $collection->getFirstItem();
    }
}
