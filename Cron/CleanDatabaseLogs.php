<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Cron;

use Magento\Framework\App\ResourceConnection;

class CleanDatabaseLogs
{
    /**
     * @param ResourceConnection $resource
     */
    public function __construct(
        private ResourceConnection $resource
    ) {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $connection = $this->resource->getConnection();
        $tableName = $this->resource->getTableName('peachpayments_hosted_logs');

        $connection->delete($tableName, 'created_at < NOW() - INTERVAL 14 DAY');
        $select = $connection->select()
            ->from('information_schema.TABLES', ['table_size_mb' => new \Zend_Db_Expr('(DATA_LENGTH + INDEX_LENGTH) / 1024 / 1024')])
            ->where('TABLE_NAME = ?', $tableName)
            ->where('TABLE_SCHEMA = DATABASE()');
        $sizeInfo = $connection->fetchRow($select);

        $size = $sizeInfo['table_size_mb'] ?? 0;

        if ($size > 5) {
            $connection->truncateTable($tableName);
        }
    }
}
