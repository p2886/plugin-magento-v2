<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Cron;

use PeachPayments\Hosted\Command\Order\Process as OrderProcessor;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks\Collection;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks\CollectionFactory;

class ProcessWebhook
{
    /**
     * @param CollectionFactory $collectionFactory
     * @param OrderProcessor $orderProcessor
     */
    public function __construct(
        private CollectionFactory $collectionFactory,
        private OrderProcessor $orderProcessor
    ) {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('is_processed', 'false');

        foreach ($collection as $webhook) {
            $this->orderProcessor->execute($webhook);
        }
    }
}
