<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y Development team
 */

namespace PeachPayments\Hosted\Query;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Api\Data\CartItemInterface;

/**
 * @spi
 */
class GetSubscriptionExpiryAndFrequency
{
    /**
     * @var TimezoneInterface
     */
    private $dateProcessor;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @param TimezoneInterface $dateProcessor
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        TimezoneInterface $dateProcessor,
        ObjectManagerInterface $objectManager
    ) {
        $this->dateProcessor = $dateProcessor;
        $this->objectManager = $objectManager;
    }

    public function execute(CartInterface $cart): array
    {
        $result = ['', ''];
        $itemManager = $this->objectManager->get('ParadoxLabs\Subscriptions\Model\Service\ItemManager');
        $dateNow = $this->dateProcessor->date(null, null, false);

        foreach ($cart->getItems() as $item) {
            if ($itemManager->isSubscription($item) !== true) {
                continue;
            }

            /** @var \ParadoxLabs\Subscriptions\Api\Data\SubscriptionInterface $subscription */
            $subscription = $this->createSubscription($cart, $item);

            // If no next run than subscription is Until-Cancelled.
            if (!$subscription->getNextRun()) {
                return ['', ''];
            }

            $nextRunTimeAdjusted = strtotime('+10days', strtotime($subscription->getNextRun()));
            $nextRunDate = $this->dateProcessor->date($nextRunTimeAdjusted, null, false);
            if ((empty($result[0]) ? $result[0] : $dateNow) < $nextRunDate) {
                $result[0] = $nextRunDate;
            }

            if ((int) $result[1] < $itemManager->getLength($item)) {
                $result[1] = $itemManager->getLength($item);
            }
        }

        if ($result[0]) {
            $result[0] = $result[0]->format('Y-m-d');
        }

        if ($result[1]) {
            $result[1] = sprintf("%04.0f", $result[1]);
        }

        return $result;
    }

    private function createSubscription(CartInterface $cart, CartItemInterface $item)
    {
        /** @var \ParadoxLabs\Subscriptions\Model\Subscription $subscription */
        $subscription = $this->objectManager->create('ParadoxLabs\Subscriptions\Model\Subscription');
        $itemManager = $this->objectManager->get('ParadoxLabs\Subscriptions\Model\Service\ItemManager');

        $subscription->setStoreId($cart->getStoreId());
        $subscription->setQuote($cart);
        $subscription->setFrequencyCount($itemManager->getFrequencyCount($item));
        $subscription->setFrequencyUnit($itemManager->getFrequencyUnit($item));
        $subscription->setLength($itemManager->getLength($item));

        for ($freq = $subscription->getLength(); $freq > 0; $freq--) {
            $subscription->calculateNextRun();
        }

        return $subscription;
    }
}
