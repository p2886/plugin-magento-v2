<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2023 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Query;

use Magento\Framework\App\CacheInterface;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\ConverterException;
use Magento\Payment\Gateway\Http\TransferBuilderFactory as HttpTransferBuilder;
use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Logger\UnifiedLogger;

/**
 * @spi
 */
class GetAccessToken
{
    public const TOKEN_CACHE_IDENTIFIER = 'pp_embedded_checkout_token';
    /**
     * @var CurlClient
     */
    private $curlClient;
    /**
     * @var HttpTransferBuilder
     */
    private $httpTransferBuilder;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @param CurlClient $curlClient
     * @param HttpTransferBuilder $httpTransferBuilder
     * @param ConfigHelper $configHelper
     * @param CacheInterface $cache
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        CurlClient $curlClient,
        HttpTransferBuilder $httpTransferBuilder,
        ConfigHelper $configHelper,
        CacheInterface $cache,
        private UnifiedLogger $unifiedLogger
    ) {
        $this->curlClient = $curlClient;
        $this->httpTransferBuilder = $httpTransferBuilder;
        $this->configHelper = $configHelper;
        $this->cache = $cache;
    }

    /**
     * Get access Bearer token.
     *
     * @throws ConverterException
     * @throws ClientException
     */
    public function execute(): string
    {
        if ($token = $this->cache->load(self::TOKEN_CACHE_IDENTIFIER)) {
            return $token;
        }

        $transfer = $this->httpTransferBuilder
            ->create()
            ->setUri($this->configHelper->getAuthenticationApiUrl())
            ->setMethod('POST')
            ->setBody(json_encode([
                "clientId" => $this->configHelper->getClientId(),
                "clientSecret" => $this->configHelper->getClientSecret(),
                "merchantId" => $this->configHelper->getMerchantId()
            ]))
            ->setHeaders([
                'Content-Type' => 'application/json'
            ])
            ->build();

        $result = $this->curlClient->placeRequest($transfer);

        if (!isset($result['access_token']) && !isset($result['expires_in'])) {
            $this->unifiedLogger->debug(
                [
                    'message' => 'Error getting access token',
                    'response' => $result
                ],
                null,
                true
            );

            throw new ClientException(__('No access token.'));
        }

        $this->cache->save(
            $result['access_token'],
            self::TOKEN_CACHE_IDENTIFIER,
            ['access_token', 'peach_payments'],
            $result['expires_in']
        );

        return $this->cache->load(self::TOKEN_CACHE_IDENTIFIER);
    }
}
