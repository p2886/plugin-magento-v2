<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Plugin;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Session\SessionManager;
use PeachPayments\Hosted\Plugin\SetSessionId;
use PHPUnit\Framework\TestCase;

class SetSessionIdTest extends TestCase
{
    private RequestInterface $requestMock;
    private SetSessionId $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->requestMock = $this->getMockBuilder(RequestInterface::class)
            ->addMethods(['getPathInfo'])
            ->getMockForAbstractClass();
        $this->object = new SetSessionId($this->requestMock);

        $this->sessionManagerMock = $this->createMock(SessionManager::class);
    }

    /**
     * @return void
     */
    public function testBeforeStart(): void
    {
        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->with('customParameters')
            ->willReturn(['PHPSESSID' => 'test_sess_id']);
        $this->requestMock->expects($this->once())
            ->method('getPathInfo')
            ->willReturn('/pp-hosted/secure/payment/');

        $this->object->beforeStart($this->sessionManagerMock);
    }
}
