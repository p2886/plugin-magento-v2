<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Plugin;

use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Payment\Api\Data\PaymentMethodInterface;
use Magento\Payment\Api\PaymentMethodListInterface;
use PeachPayments\Hosted\Helper\Config;
use PeachPayments\Hosted\Plugin\PaymentMethodListPlugin;
use PHPUnit\Framework\TestCase;

class PaymentMethodListPluginTest extends TestCase
{
    private CookieManagerInterface $cookieManagerMock;
    private Config $configMock;
    private PaymentMethodListPlugin $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->cookieManagerMock = $this->createMock(CookieManagerInterface::class);
        $this->configMock = $this->createMock(Config::class);
        $this->object = new PaymentMethodListPlugin(
            $this->cookieManagerMock,
            $this->configMock
        );

        $this->paymentMethodListMock = $this->createMock(PaymentMethodListInterface::class);
    }

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testAfterGetActiveList(): void
    {
        $paymentMethod1 = $this->createMock(PaymentMethodInterface::class);
        $paymentMethod1->expects($this->once())
            ->method('getCode')
            ->willReturn('peach_payment_1');
        $paymentMethod2 = $this->createMock(PaymentMethodInterface::class);
        $paymentMethod2->expects($this->once())
            ->method('getCode')
            ->willReturn('other_payment_2');

        $this->cookieManagerMock->expects($this->once())
            ->method('getCookie')
            ->with('pay-with-peach-payment')
            ->willReturn(true);
        $this->configMock->expects($this->once())
            ->method('getPayWithPeachMethods')
            ->willReturn('peach_payment');

        $result = $this->object->afterGetActiveList(
            $this->paymentMethodListMock,
            [$paymentMethod1, $paymentMethod2]
        );

        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertSame($paymentMethod1, $result[0]);
    }

    /**
     * @return void
     */
    public function testAfterGetActiveListNoCookie(): void
    {
        $this->cookieManagerMock->expects($this->once())
            ->method('getCookie')
            ->with('pay-with-peach-payment')
            ->willReturn(false);

        $result = $this->object->afterGetActiveList($this->paymentMethodListMock, ['method1', 'method2']);
        $this->assertEquals(['method1', 'method2'], $result);
    }
}
