<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Plugin;

use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Gateway\Http\Client as HttpClient;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHelper;
use PeachPayments\Hosted\Model\Ui\ConfigProvider;
use PeachPayments\Hosted\Plugin\DeletePaymentRegistrationPlugin;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class DeletePaymentRegistrationPluginTest extends TestCase
{
    private ConfigHelper $configHelperMock;
    private PeachPaymentsHelper $peachPaymentsHelperMock;
    private HttpClient $httpClientMock;
    private HttpTransferObject $httpTransferObjectMock;
    private DeletePaymentRegistrationPlugin $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->peachPaymentsHelperMock = $this->createMock(PeachPaymentsHelper::class);
        $this->httpClientMock = $this->createMock(HttpClient::class);
        $this->httpTransferObjectMock = $this->createMock(HttpTransferObject::class);

        $this->object = new DeletePaymentRegistrationPlugin(
            $this->configHelperMock,
            $this->peachPaymentsHelperMock,
            $this->httpClientMock,
            $this->httpTransferObjectMock
        );

        $this->paymentTokenMock = $this->createMock(PaymentTokenInterface::class);
        $this->paymentTokenRepositoryMock = $this->createMock(PaymentTokenRepositoryInterface::class);
        $this->transfer = $this->createMock(TransferInterface::class);
    }

    /**
     * @return void
     */
    public function testBeforeDeleteSuccess(): void
    {
        $this->paymentTokenMock->expects($this->once())
            ->method('getGatewayToken')
            ->willReturn('test_token');
        $this->paymentTokenMock->expects($this->once())
            ->method('getPaymentMethodCode')
            ->willReturn(ConfigProvider::CODE);
        $this->peachPaymentsHelperMock->expects($this->once())
            ->method('getEntityId')
            ->willReturn(123456);
        $this->configHelperMock->expects($this->once())
            ->method('getApiDeleteRegistrationUri')
            ->willReturn('https://mock.peachpayments.com/delete/test_token');
        $this->httpTransferObjectMock->expects($this->once())
            ->method('create')
            ->willReturn($this->transfer);
        $this->httpClientMock->expects($this->once())
            ->method('placeRequest')
            ->with($this->transfer);

        $result = $this->object->beforeDelete($this->paymentTokenRepositoryMock, $this->paymentTokenMock);

        $this->assertEquals([$this->paymentTokenMock], $result);
    }

    /**
     * @return void
     */
    public function testBeforeDeleteFailure(): void
    {
        $this->paymentTokenMock->expects($this->once())
            ->method('getGatewayToken')
            ->willReturn('test_token');
        $this->paymentTokenMock->expects($this->once())
            ->method('getPaymentMethodCode')
            ->willReturn(ConfigProvider::CODE);
        $this->peachPaymentsHelperMock->expects($this->once())
            ->method('getEntityId')
            ->willReturn(123456);
        $this->configHelperMock->expects($this->once())
            ->method('getApiDeleteRegistrationUri')
            ->willReturn('https://mock.peachpayments.com/delete/test_token');
        $this->httpTransferObjectMock->expects($this->once())
            ->method('create')
            ->willReturn($this->transfer);
        $this->httpClientMock->expects($this->once())
            ->method('placeRequest')
            ->willThrowException(new \Exception('Error'));

        $result = $this->object->beforeDelete($this->paymentTokenRepositoryMock, $this->paymentTokenMock);

        $this->assertEquals([$this->paymentTokenMock], $result);
    }

    /**
     * @return void
     */
    public function testBeforeDeleteNonPeachPaymentMethod(): void
    {
        $this->paymentTokenMock->expects($this->once())
            ->method('getPaymentMethodCode')
            ->willReturn('other_payment_method_code');

        $result = $this->object->beforeDelete($this->paymentTokenRepositoryMock, $this->paymentTokenMock);

        $this->assertEquals([$this->paymentTokenMock], $result);
    }
}
