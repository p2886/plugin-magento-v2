<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Service;

use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order\Payment;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as HelperData;
use PeachPayments\Hosted\Service\FormPostCheckout;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class FormPostCheckoutTest extends TestCase
{
    private HelperData|MockObject $helperData;
    private ConfigHelper|MockObject $configHelper;
    private RemoteAddress|MockObject $remoteAddress;
    private CheckoutSession|MockObject $checkoutSession;
    private UrlInterface|MockObject $urlBuilder;
    private CookieManagerInterface $cookieManagerMock;
    private SerializerInterface $serializerMock;
    private FormPostCheckout $formPostCheckout;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->helperData = $this->createMock(HelperData::class);
        $this->configHelper = $this->createMock(ConfigHelper::class);
        $this->remoteAddress = $this->createMock(RemoteAddress::class);
        $this->checkoutSession = $this->createMock(CheckoutSession::class);
        $this->urlBuilder = $this->createMock(UrlInterface::class);
        $this->cookieManagerMock = $this->createMock(CookieManagerInterface::class);
        $this->serializerMock = $this->createMock(SerializerInterface::class);

        $this->formPostCheckout = new FormPostCheckout(
            $this->helperData,
            $this->remoteAddress,
            $this->configHelper,
            $this->checkoutSession,
            $this->urlBuilder,
            $this->cookieManagerMock,
            $this->serializerMock
        );
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testGetFormData(): void
    {
        $order = $this->createMock(Order::class);
        $address = $this->createMock(Address::class);
        $payment = $this->createMock(Payment::class);
        $methodInstance = $this->createMock(MethodInterface::class);

        $this->checkoutSession->method('getLastRealOrder')->willReturn($order);
        $order->method('getBillingAddress')->willReturn($address);
        $order->method('getPayment')->willReturn($payment);
        $order->method('getOrderCurrencyCode')->willReturn('USD');
        $order->method('getIncrementId')->willReturn('100000001');
        $order->method('getCustomerIsGuest')->willReturn(true);

        $address->method('getStreet')->willReturn(['Street Line 1']);
        $address->method('getCountryId')->willReturn('US');
        $address->method('getFirstname')->willReturn('John');
        $address->method('getLastname')->willReturn('Doe');
        $address->method('getTelephone')->willReturn('123456789');
        $address->method('getEmail')->willReturn('john.doe@example.com');
        $address->method('getCity')->willReturn('New York');
        $address->method('getPostcode')->willReturn('10001');

        $payment->method('getAmountOrdered')->willReturn(100.00);
        $payment->method('getMethodInstance')->willReturn($methodInstance);
        $methodInstance->method('getCode')->willReturn('PAYMENT');

        $this->remoteAddress->method('getRemoteAddress')->willReturn('127.0.0.1');
        $this->urlBuilder->method('getUrl')->willReturn('https://example.com/return-url');

        $unsortedFormData = [
            'authentication.entityId' => 'entity-id-3d-secure',
            'amount' => '100.00',
            'paymentType' => 'DB',
            'currency' => 'USD',
            'shopperResultUrl' => 'https://example.com/return-url',
            'customParameters[PHPSESSID]' => 'session-id',
            'merchantTransactionId' => '100000001',
            'plugin' => 'platform-name',
            'customer.givenName' => 'John',
            'customer.surname' => 'Doe',
            'customer.mobile' => '123456789',
            'customer.email' => 'john.doe@example.com',
            'customer.status' => 'NEW',
            'customer.ip' => '127.0.0.1',
            'billing.street1' => 'US Street Line 1',
            'billing.street2' => 'N/A',
            'billing.city' => 'New York',
            'billing.country' => 'US',
            'billing.postcode' => '10001',
            'defaultPaymentMethod' => 'PAYMENT',
            'forceDefaultMethod' => ''
        ];

        $this->configHelper->method('getEntityId3DSecure')->willReturn('entity-id-3d-secure');
        $this->checkoutSession->method('getSessionId')->willReturn('session-id');
        $this->helperData->method('getPlatformName')->willReturn('platform-name');

        $this->helperData->expects($this->once())
            ->method('signData')
            ->with($unsortedFormData)
            ->willReturn(['signedData' => 'signedValue']);

        $formData = $this->formPostCheckout->getFormData();

        $this->assertEquals(['signedData' => 'signedValue'], $formData);
    }

}
