<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Service;

use Magento\Framework\Filesystem\Directory\ReadFactory;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Magento\Framework\Filesystem\Io\File;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Mail\MessageInterface;
use Magento\Store\Api\Data\StoreInterface;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mime\Part;
use Laminas\Mime\Mime;
use PeachPayments\Hosted\Service\SendIssueReport;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SendIssueReportTest extends TestCase
{
    private TransportBuilder|MockObject $transportBuilderMock;
    private StoreManagerInterface|MockObject $storeManagerMock;
    private Emulation|MockObject $emulationMock;
    private ScopeConfigInterface|MockObject $scopeConfigMock;
    private TransportInterface|MockObject $transportMock;
    private MessageInterface|MockObject $messageMock;
    private MockObject|StoreInterface $storeMock;
    private ReadFactory $readFactoryMock;
    private ReadInterface $readerMock;
    private File $fileMock;
    private SendIssueReport $sendIssueReport;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->transportBuilderMock = $this->createMock(TransportBuilder::class);
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->emulationMock = $this->createMock(Emulation::class);
        $this->scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $this->transportMock = $this->createMock(TransportInterface::class);
        $this->messageMock = $this->createMock(MessageInterface::class);
        $this->storeMock = $this->getMockForAbstractClass(StoreInterface::class);
        $this->readFactoryMock = $this->createMock(ReadFactory::class);
        $this->readerMock = $this->createMock(ReadInterface::class);
        $this->fileMock = $this->createMock(File::class);
        $this->sendIssueReport = new SendIssueReport(
            $this->transportBuilderMock,
            $this->storeManagerMock,
            $this->emulationMock,
            $this->scopeConfigMock,
            $this->readFactoryMock,
            $this->fileMock
        );
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testSendEmail(): void
    {
        $data = ['some' => 'data'];
        $copyTo = 'copy@example.com';
        $storeId = 1;
        $this->storeManagerMock->expects($this->any())
            ->method('getStore')
            ->willReturn($this->storeMock);
        $this->storeMock->expects($this->any())
            ->method('getId')
            ->willReturn($storeId);
        $this->emulationMock->expects($this->once())
            ->method('startEnvironmentEmulation')
            ->with($storeId, 'frontend', true);
        $this->transportBuilderMock->expects($this->once())
            ->method('setTemplateIdentifier')
            ->with('peach_payments_issue_report')
            ->willReturnSelf();
        $this->transportBuilderMock->expects($this->once())
            ->method('setTemplateOptions')
            ->with(['area' => 'frontend', 'store' => $storeId])
            ->willReturnSelf();
        $this->transportBuilderMock->expects($this->once())
            ->method('setTemplateVars')
            ->with(['magento_data' => $data])
            ->willReturnSelf();
        $this->transportBuilderMock->expects($this->once())
            ->method('addTo')
            ->with('support@peachpayments.com')
            ->willReturnSelf();
        $this->transportBuilderMock->expects($this->once())
            ->method('setFromByScope')
            ->with($this->isType('string'))
            ->willReturnSelf();
        $this->transportBuilderMock->expects($this->once())
            ->method('addCc')
            ->with(trim($copyTo))
            ->willReturnSelf();
        $this->transportBuilderMock->expects($this->once())
            ->method('getTransport')
            ->willReturn($this->transportMock);
        $this->transportMock->expects($this->any())
            ->method('getMessage')
            ->willReturn($this->messageMock);
        $body = new MimeMessage();
        $this->messageMock->expects($this->once())
            ->method('getBody')
            ->willReturn($body);
        $this->fileMock->expects($this->once())
            ->method('getPathInfo')
            ->willReturn(['dirname' => '/test/test', 'basename' => 'test.zip']);
        $this->readFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->readerMock);
        $this->readerMock->expects($this->once())
            ->method('readFile')
            ->willReturn('file contents');

        $fileContents = 'file contents';
        $attachment = new Part($fileContents);
        $attachment->type = 'application/zip';
        $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
        $attachment->encoding = Mime::ENCODING_BASE64;
        $attachment->filename = 'logs_modules_versions.zip';

        $mimeMessage = new MimeMessage();
        $mimeMessage->setParts($body->getParts());
        $mimeMessage->addPart($attachment);

        $this->messageMock->expects($this->once())
            ->method('setBody')
            ->with($mimeMessage);
        $this->transportMock->expects($this->once())
            ->method('sendMessage');
        $this->emulationMock->expects($this->once())
            ->method('stopEnvironmentEmulation');

        $this->sendIssueReport->sendEmail($data, $copyTo, '/test/test/logs_modules_versions.zip');
    }
}
