<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Query;

use Magento\Payment\Gateway\Http\ConverterException;
use Magento\Payment\Gateway\Http\TransferBuilder;
use Magento\Payment\Gateway\Http\TransferInterface;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Magento\Framework\App\CacheInterface;
use Magento\Payment\Gateway\Http\TransferBuilderFactory as HttpTransferBuilder;
use Magento\Payment\Gateway\Http\ClientException;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use PeachPayments\Hosted\Query\GetAccessToken;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class GetAccessTokenTest extends TestCase
{
    private MockObject|CacheInterface $cacheMock;
    private HttpTransferBuilder|MockObject $httpTransferBuilderMock;
    private ConfigHelper|MockObject $configHelperMock;
    private CurlClient|MockObject $curlClientMock;
    private UnifiedLogger|MockObject $unifiedLogger;
    private GetAccessToken $getAccessToken;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->cacheMock = $this->createMock(CacheInterface::class);
        $this->httpTransferBuilderMock = $this->createMock(HttpTransferBuilder::class);
        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->curlClientMock = $this->createMock(CurlClient::class);
        $this->unifiedLogger = $this->createMock(UnifiedLogger::class);

        $this->getAccessToken = new GetAccessToken(
            $this->curlClientMock,
            $this->httpTransferBuilderMock,
            $this->configHelperMock,
            $this->cacheMock,
            $this->unifiedLogger
        );
    }

    /**
     * @return void
     * @throws \Magento\Payment\Gateway\Http\ClientException
     * @throws \Magento\Payment\Gateway\Http\ConverterException
     */
    public function testExecuteReturnsTokenFromCache(): void
    {
        $cachedToken = 'cached_token';

        $this->cacheMock->method('load')
            ->with(GetAccessToken::TOKEN_CACHE_IDENTIFIER)
            ->willReturn($cachedToken);

        $this->assertEquals($cachedToken, $this->getAccessToken->execute());
    }

    /**
     * @return void
     * @throws \Magento\Payment\Gateway\Http\ClientException
     * @throws \Magento\Payment\Gateway\Http\ConverterException
     */
    public function testExecuteFetchesTokenWhenNotInCache(): void
    {
        $transferMock = $this->createMock(TransferInterface::class);

        $apiUrl = 'https://api.example.com/auth';
        $clientId = 'testClientId';
        $clientSecret = 'testClientSecret';
        $merchantId = 'testMerchantId';
        $apiResponse = [
            'access_token' => 'new_token',
            'expires_in' => 3600
        ];

        $this->configHelperMock->method('getAuthenticationApiUrl')->willReturn($apiUrl);
        $this->configHelperMock->method('getClientId')->willReturn($clientId);
        $this->configHelperMock->method('getClientSecret')->willReturn($clientSecret);
        $this->configHelperMock->method('getMerchantId')->willReturn($merchantId);

        $body = [
            "clientId" => $clientId,
            "clientSecret" => $clientSecret,
            "merchantId" => $merchantId
        ];

        $transferBuilder = $this->createMock(TransferBuilder::class);
        $this->httpTransferBuilderMock->method('create')->willReturn($transferBuilder);
        $transferBuilder->method('setUri')->with($apiUrl)->willReturnSelf();
        $transferBuilder->method('setMethod')->with('POST')->willReturnSelf();
        $transferBuilder->method('setBody')->with(json_encode($body))->willReturnSelf();
        $transferBuilder->method('setHeaders')->with(['Content-Type' => 'application/json'])->willReturnSelf();
        $transferBuilder->method('build')->willReturn($transferMock);

        $this->curlClientMock->method('placeRequest')->with($transferMock)->willReturn($apiResponse);

        $this->cacheMock->method('save')
            ->with(
                $apiResponse['access_token'],
                GetAccessToken::TOKEN_CACHE_IDENTIFIER,
                ['access_token', 'peach_payments'],
                $apiResponse['expires_in']
            );

        $this->cacheMock->method('load')
            ->with(GetAccessToken::TOKEN_CACHE_IDENTIFIER)
            ->willReturn($apiResponse['access_token']);

        $this->assertEquals($apiResponse['access_token'], $this->getAccessToken->execute());
    }

    /**
     * @return void
     * @throws \Magento\Payment\Gateway\Http\ClientException
     * @throws \Magento\Payment\Gateway\Http\ConverterException
     */
    public function testExecuteThrowsExceptionWhenTokenNotRetrieved(): void
    {
        $this->cacheMock->method('load')
            ->with(GetAccessToken::TOKEN_CACHE_IDENTIFIER)
            ->willReturn(false);

        $transferMock = $this->createMock(TransferInterface::class);

        $apiUrl = 'https://api.example.com/auth';
        $clientId = 'testClientId';
        $clientSecret = 'testClientSecret';
        $merchantId = 'testMerchantId';

        $this->configHelperMock->method('getAuthenticationApiUrl')->willReturn($apiUrl);
        $this->configHelperMock->method('getClientId')->willReturn($clientId);
        $this->configHelperMock->method('getClientSecret')->willReturn($clientSecret);
        $this->configHelperMock->method('getMerchantId')->willReturn($merchantId);

        $body = [
            "clientId" => $clientId,
            "clientSecret" => $clientSecret,
            "merchantId" => $merchantId
        ];

        $transferBuilder = $this->createMock(TransferBuilder::class);
        $this->httpTransferBuilderMock->method('create')->willReturn($transferBuilder);
        $transferBuilder->method('setUri')->with($apiUrl)->willReturnSelf();
        $transferBuilder->method('setMethod')->with('POST')->willReturnSelf();
        $transferBuilder->method('setBody')->with(json_encode($body))->willReturnSelf();
        $transferBuilder->method('setHeaders')->with(['Content-Type' => 'application/json'])->willReturnSelf();
        $transferBuilder->method('build')->willReturn($transferMock);

        $apiResponse = [];

        $this->curlClientMock->method('placeRequest')->with($transferMock)->willReturn($apiResponse);

        $this->unifiedLogger->expects($this->once())
            ->method('debug')
            ->with(
                ['message' => 'Error getting access token', 'response' => $apiResponse],
                null,
                true
            );

        $this->expectException(ClientException::class);
        $this->expectExceptionMessage('No access token.');

        $this->getAccessToken->execute();
    }

    /**
     * @return void
     * @throws \Magento\Payment\Gateway\Http\ClientException
     * @throws \Magento\Payment\Gateway\Http\ConverterException
     */
    public function testExecuteThrowsConverterException(): void
    {
        $this->cacheMock->method('load')
            ->with(GetAccessToken::TOKEN_CACHE_IDENTIFIER)
            ->willReturn(false);

        $transferMock = $this->createMock(TransferInterface::class);

        $apiUrl = 'https://api.example.com/auth';
        $clientId = 'testClientId';
        $clientSecret = 'testClientSecret';
        $merchantId = 'testMerchantId';

        $this->configHelperMock->method('getAuthenticationApiUrl')->willReturn($apiUrl);
        $this->configHelperMock->method('getClientId')->willReturn($clientId);
        $this->configHelperMock->method('getClientSecret')->willReturn($clientSecret);
        $this->configHelperMock->method('getMerchantId')->willReturn($merchantId);

        $body = [
            "clientId" => $clientId,
            "clientSecret" => $clientSecret,
            "merchantId" => $merchantId
        ];

        $transferBuilder = $this->createMock(TransferBuilder::class);
        $this->httpTransferBuilderMock->method('create')->willReturn($transferBuilder);
        $transferBuilder->method('setUri')->with($apiUrl)->willReturnSelf();
        $transferBuilder->method('setMethod')->with('POST')->willReturnSelf();
        $transferBuilder->method('setBody')->with(json_encode($body))->willReturnSelf();
        $transferBuilder->method('setHeaders')->with(['Content-Type' => 'application/json'])->willReturnSelf();
        $transferBuilder->method('build')->willReturn($transferMock);

        $this->curlClientMock
            ->method('placeRequest')
            ->with($transferMock)
            ->willThrowException(new ConverterException(__('Conversion error')));

        $this->expectException(ConverterException::class);
        $this->expectExceptionMessage('Conversion error');

        $this->getAccessToken->execute();
    }
}
