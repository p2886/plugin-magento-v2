<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Command\CreditMemo;

use Magento\Sales\Model\Order\Payment;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Magento\Framework\HTTP\ClientInterface;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Framework\HTTP\ClientFactory as HttpClient;
use PeachPayments\Hosted\Command\CreditMemo\Process;
use PeachPayments\Hosted\Helper\Config;
use PeachPayments\Hosted\Helper\Data;

class ProcessTest extends TestCase
{
    private HttpClient|MockObject $httpClient;
    private Config|MockObject $config;
    private Data|MockObject $helper;
    private UnifiedLogger|MockObject $unifiedLogger;
    private Process $process;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->httpClient = $this->createMock(HttpClient::class);
        $this->config = $this->createMock(Config::class);
        $this->helper = $this->createMock(Data::class);
        $this->unifiedLogger = $this->createMock(UnifiedLogger::class);

        $this->process = new Process(
            $this->httpClient,
            $this->config,
            $this->helper,
            $this->unifiedLogger
        );
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $payment = $this->createMock(Payment::class);
        $creditMemo = $this->createMock(Creditmemo::class);
        $client = $this->createMock(ClientInterface::class);

        $payment->expects($this->once())
            ->method('getData')
            ->with('creditmemo')
            ->willReturn($creditMemo);

        $payment->expects($this->once())
            ->method('getParentTransactionId')
            ->willReturn('123-refund');

        $creditMemo->expects($this->once())
            ->method('getOrderCurrencyCode')
            ->willReturn('USD');

        $this->config->expects($this->once())
            ->method('getEntityId3DSecure')
            ->willReturn('entity-id-3d-secure');

        $params = [
            'authentication.entityId' => 'entity-id-3d-secure',
            'amount' => '10.00',
            'paymentType' => 'RF',
            'currency' => 'USD',
            'id' => '123',
        ];

        $this->helper->expects($this->once())
            ->method('signData')
            ->with($params, false)
            ->willReturn($params);

        $this->httpClient->expects($this->once())
            ->method('create')
            ->willReturn($client);

        $client->expects($this->once())
            ->method('post')
            ->with($this->config->getApiUrl('refund'), $params);

        $client->expects($this->once())
            ->method('getBody')
            ->willReturn(json_encode(['id' => 'refund-transaction-id']));

        $payment->expects($this->once())
            ->method('setTransactionId')
            ->with('refund-transaction-id');

        $this->unifiedLogger->expects($this->exactly(2))
            ->method('debug')
            ->withConsecutive(
                [['message' => 'Refund start process.', 'params' => $params]],
                [['message' => 'Refund processed.', 'response' => ['id' => 'refund-transaction-id']]]
            );

        $this->process->execute($payment, 10);
    }

    /**
     * @return void
     */
    public function testExecuteWithException(): void
    {
        $payment = $this->createMock(Payment::class);
        $creditMemo = $this->createMock(Creditmemo::class);
        $client = $this->createMock(ClientInterface::class);

        $payment->expects($this->once())
            ->method('getData')
            ->with('creditmemo')
            ->willReturn($creditMemo);

        $payment->expects($this->once())
            ->method('getParentTransactionId')
            ->willReturn('123-refund');

        $creditMemo->expects($this->once())
            ->method('getOrderCurrencyCode')
            ->willReturn('USD');

        $this->config->expects($this->once())
            ->method('getEntityId3DSecure')
            ->willReturn('entity-id-3d-secure');

        $params = [
            'authentication.entityId' => 'entity-id-3d-secure',
            'amount' => '10.00',
            'paymentType' => 'RF',
            'currency' => 'USD',
            'id' => '123',
        ];

        $this->helper->expects($this->once())
            ->method('signData')
            ->with($params, false)
            ->willReturn($params);

        $this->httpClient->expects($this->once())
            ->method('create')
            ->willReturn($client);

        $exception = new \Exception('Error message');

        $client->expects($this->once())
            ->method('post')
            ->with($this->config->getApiUrl('refund'), $params)
            ->willThrowException($exception);

        $this->unifiedLogger->expects($this->exactly(2))
            ->method('debug')
            ->withConsecutive(
                [['message' => 'Refund start process.', 'params' => $params]],
                [['message' => 'Refund error processing.', 'exception' => 'Error message', 'trace' => $exception->getTrace()]]
            );

        $this->process->execute($payment, 10);
    }
}
