<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Command;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use PeachPayments\Hosted\Model\Ui\ConfigProvider;
use PeachPayments\Hosted\Command\GetVaultGatewayTokensForCart;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class GetVaultGatewayTokensForCartTest extends TestCase
{
    private PaymentTokenRepositoryInterface $paymentTokenRepository;
    private SearchCriteriaBuilderFactory|MockObject $criteriaBuilderFactory;
    private SearchCriteriaBuilder|MockObject $criteriaBuilder;
    private SearchCriteriaInterface|MockObject $searchCriteria;
    private SearchResultsInterface|MockObject $searchResults;
    private GetVaultGatewayTokensForCart $getVaultGatewayTokensForCart;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->paymentTokenRepository = $this->createMock(PaymentTokenRepositoryInterface::class);
        $this->criteriaBuilderFactory = $this->createMock(SearchCriteriaBuilderFactory::class);
        $this->criteriaBuilder = $this->createMock(SearchCriteriaBuilder::class);
        $this->searchCriteria = $this->createMock(SearchCriteriaInterface::class);
        $this->searchResults = $this->createMock(SearchResultsInterface::class);

        $this->criteriaBuilderFactory->method('create')->willReturn($this->criteriaBuilder);
        $this->criteriaBuilder->method('create')->willReturn($this->searchCriteria);

        $this->getVaultGatewayTokensForCart = new GetVaultGatewayTokensForCart(
            $this->paymentTokenRepository,
            $this->criteriaBuilderFactory
        );
    }

    /**
     * @return void
     */
    public function testExecuteWithCart(): void
    {
        $cart = $this->createMock(CartInterface::class);
        $customer = $this->createMock(CustomerInterface::class);
        $paymentToken = $this->createMock(PaymentTokenInterface::class);

        $cart->method('getCustomer')->willReturn($customer);
        $customer->method('getId')->willReturn(1);
        $paymentToken->method('getGatewayToken')->willReturn('token');

        $this->criteriaBuilder->expects($this->exactly(4))->method('addFilter')
            ->withConsecutive(
                [PaymentTokenInterface::PAYMENT_METHOD_CODE, ConfigProvider::CODE],
                [PaymentTokenInterface::CUSTOMER_ID, 1],
                [PaymentTokenInterface::IS_ACTIVE, '1'],
                [PaymentTokenInterface::IS_VISIBLE, '1']
            )->willReturnSelf();

        $this->paymentTokenRepository->expects($this->once())->method('getList')->with($this->searchCriteria)->willReturn($this->searchResults);
        $this->searchResults->expects($this->once())->method('getItems')->willReturn([$paymentToken]);

        $result = $this->getVaultGatewayTokensForCart->execute($cart);

        $this->assertEquals(['token'], $result);
    }

    /**
     * @return void
     */
    public function testExecuteWithCustomerId(): void
    {
        $customerId = 1;
        $paymentToken = $this->createMock(PaymentTokenInterface::class);

        $paymentToken->method('getGatewayToken')->willReturn('token');

        $this->criteriaBuilder->expects($this->exactly(4))->method('addFilter')
            ->withConsecutive(
                [PaymentTokenInterface::PAYMENT_METHOD_CODE, ConfigProvider::CODE],
                [PaymentTokenInterface::CUSTOMER_ID, $customerId],
                [PaymentTokenInterface::IS_ACTIVE, '1'],
                [PaymentTokenInterface::IS_VISIBLE, '1']
            )->willReturnSelf();

        $this->paymentTokenRepository->expects($this->once())->method('getList')->with($this->searchCriteria)->willReturn($this->searchResults);
        $this->searchResults->expects($this->once())->method('getItems')->willReturn([$paymentToken]);

        $result = $this->getVaultGatewayTokensForCart->execute(null, $customerId);

        $this->assertEquals(['token'], $result);
    }
}
