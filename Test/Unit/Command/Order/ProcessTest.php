<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Command\Order;

use PeachPayments\Hosted\Logger\UnifiedLogger;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender as InvoiceEmailSender;
use Magento\Sales\Model\Order\Email\Sender\OrderSender as OrderEmailSender;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Model\Web\Hooks;
use PeachPayments\Hosted\Command\Order\Process;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProcessTest extends TestCase
{
    private OrderRepositoryInterface|MockObject $orderRepository;
    private ManagerInterface|MockObject $eventManager;
    private OrderEmailSender|MockObject $orderEmailSender;
    private InvoiceEmailSender|MockObject $invoiceEmailSender;
    private ConfigHelper|MockObject $configHelper;
    private OrderFactory|MockObject $orderFactory;
    private UnifiedLogger|MockObject $unifiedLogger;
    private Process $process;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->orderRepository = $this->createMock(OrderRepositoryInterface::class);
        $this->eventManager = $this->createMock(ManagerInterface::class);
        $this->orderEmailSender = $this->createMock(OrderEmailSender::class);
        $this->invoiceEmailSender = $this->createMock(InvoiceEmailSender::class);
        $this->configHelper = $this->createMock(ConfigHelper::class);
        $this->orderFactory = $this->createMock(OrderFactory::class);
        $this->unifiedLogger = $this->createMock(UnifiedLogger::class);

        $this->process = new Process(
            $this->orderRepository,
            $this->eventManager,
            $this->orderEmailSender,
            $this->invoiceEmailSender,
            $this->configHelper,
            $this->orderFactory,
            $this->unifiedLogger
        );
    }

    /**
     * @return void
     */
    public function testExecuteSuccessResultCode(): void
    {
        $webhook = $this->createMock(Hooks::class);
        $order = $this->createMock(Order::class);

        $webhook->method('getData')->with('order_increment_id')->willReturn('100000001');

        $order->method('getEntityId')->willReturn(1);
        $order->method('getPayment')->willReturn(true);
        $this->orderFactory->method('create')->willReturn($order);

        $order->expects($this->once())->method('loadByIncrementId')->with('100000001');

        $this->process->execute($webhook, '000.000.000');
    }

    /**
     * @return void
     */
    public function testExecuteCancelOrder(): void
    {
        $webhook = $this->createMock(Hooks::class);
        $order = $this->createMock(Order::class);

        $webhook->method('getData')->with('order_increment_id')->willReturn('100000001');

        $order->method('getEntityId')->willReturn(1);
        $this->orderFactory->method('create')->willReturn($order);

        $order->expects($this->once())->method('loadByIncrementId')->with('100000001');
        $order->expects($this->once())->method('cancel');
        $this->orderRepository->expects($this->once())->method('save')->with($order);

        $this->process->execute($webhook, '999.999.999');
    }
}
