<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Command\Webhook;

use PeachPayments\Hosted\Logger\UnifiedLogger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use PeachPayments\Hosted\Helper\Data as DataHelper;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use PeachPayments\Hosted\Model\Web\Hooks as Webhook;
use PeachPayments\Hosted\Command\Webhook\ProcessNonEncrypted;

class ProcessNonEncryptedTest extends TestCase
{
    private DataHelper|MockObject $dataHelper;
    private WebhooksFactory|MockObject $webhooksFactory;
    private WebhooksResource|MockObject $webhooksResource;
    private JsonSerializer|MockObject $jsonSerializer;
    private UnifiedLogger|MockObject $unifiedLogger;
    private ProcessNonEncrypted $processNonEncrypted;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->dataHelper = $this->createMock(DataHelper::class);
        $this->webhooksFactory = $this->createMock(WebhooksFactory::class);
        $this->webhooksResource = $this->createMock(WebhooksResource::class);
        $this->jsonSerializer = $this->createMock(JsonSerializer::class);
        $this->unifiedLogger = $this->createMock(UnifiedLogger::class);

        $this->processNonEncrypted = new ProcessNonEncrypted(
            $this->dataHelper,
            $this->webhooksFactory,
            $this->webhooksResource,
            $this->jsonSerializer,
            $this->unifiedLogger
        );
    }

    /**
     * @return void
     */
    public function testExecuteWithInvalidSignature(): void
    {
        $data = [
            'merchantTransactionId' => '100000001',
            'customParameters[param1]' => 'value1',
            'signature' => 'valid_signature'
        ];

        $this->dataHelper->expects($this->once())
            ->method('signData')
            ->with($data, false)
            ->willReturn($data);

        $webhook = $this->createMock(Webhook::class);
        $this->webhooksFactory->expects($this->once())
            ->method('create')
            ->willReturn($webhook);

        $this->webhooksResource->expects($this->once())
            ->method('load')
            ->with($webhook, '100000001', 'order_increment_id');

        $webhook->expects($this->any())
            ->method('getId')
            ->willReturn(null);

        $this->processNonEncrypted->execute($data);
    }

    /**
     * @return void
     */
    public function testExecuteWithValidSignature(): void
    {
        $data = [
            'merchantTransactionId' => '100000001',
            'customParameters[param1]' => 'value1',
            'signature' => 'valid_signature',
            'result_code' => '000.200.100'
        ];

        $this->dataHelper->expects($this->once())
            ->method('signData')
            ->with($data, false)
            ->willReturn($data);

        $webhook = $this->getMockBuilder(Webhook::class)
            ->disableOriginalConstructor()
            ->setMethods(['getId', 'setData'])
            ->getMock();
        $this->webhooksFactory->expects($this->once())
            ->method('create')
            ->willReturn($webhook);

        $this->webhooksResource->expects($this->once())
            ->method('load')
            ->with($webhook, '100000001', 'order_increment_id');

        $webhook->expects($this->once())
            ->method('getId')
            ->willReturn(1);

        $this->dataHelper->expects($this->once())
            ->method('mapWebhookData')
            ->with($data)
            ->willReturn(['key1' => 'value1']);
        $webhook->expects($this->exactly(3))
            ->method('setData');

        $this->jsonSerializer->expects($this->once())
            ->method('serialize')
            ->with($data)
            ->willReturn(json_encode($data));

        $this->webhooksResource->expects($this->once())
            ->method('save')
            ->with($webhook);

        $this->processNonEncrypted->execute($data);
    }
}
