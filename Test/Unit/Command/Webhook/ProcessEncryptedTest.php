<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Command\Webhook;

use Laminas\Http\Header\HeaderInterface;
use Laminas\Http\Headers;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as DataHelper;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;
use PeachPayments\Hosted\Command\Webhook\ProcessEncrypted;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ProcessEncryptedTest extends TestCase
{
    private ConfigHelper|MockObject $configHelper;
    private DataHelper|MockObject $dataHelper;
    private WebhooksFactory|MockObject $webhooksFactory;
    private WebhooksResource|MockObject $webhooksResource;
    private JsonSerializer|MockObject $jsonSerializer;
    private UnifiedLogger|MockObject $unifiedLogger;
    private ProcessEncrypted $processEncrypted;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->configHelper = $this->createMock(ConfigHelper::class);
        $this->dataHelper = $this->createMock(DataHelper::class);
        $this->webhooksFactory = $this->createMock(WebhooksFactory::class);
        $this->webhooksResource = $this->createMock(WebhooksResource::class);
        $this->jsonSerializer = $this->createMock(JsonSerializer::class);
        $this->unifiedLogger = $this->createMock(UnifiedLogger::class);

        $this->processEncrypted = new ProcessEncrypted(
            $this->configHelper,
            $this->dataHelper,
            $this->webhooksFactory,
            $this->webhooksResource,
            $this->jsonSerializer,
            $this->unifiedLogger
        );
    }

    /**
     * @return void
     */
    public function testExecuteDecryptionFails(): void
    {
        $requestData = '{"encryptedBody":"6162636465666768696a6b6c6d6e6f70"}';

        $headers = $this->createMock(Headers::class);
        $headerIv = $this->createMock(HeaderInterface::class);
        $headerIv->expects($this->once())
            ->method('getFieldValue')
            ->willReturn(bin2hex(openssl_random_pseudo_bytes(12)));
        $headerAuthTag = $this->createMock(HeaderInterface::class);
        $headerAuthTag->expects($this->once())
            ->method('getFieldValue')
            ->willReturn(bin2hex(openssl_random_pseudo_bytes(12)));

        $headers->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(['X-Initialization-Vector'], ['X-Authentication-Tag'])
            ->willReturnOnConsecutiveCalls($headerIv, $headerAuthTag);

        $this->jsonSerializer->expects($this->once())
            ->method('unserialize')
            ->with($requestData)
            ->willReturn(['encryptedBody' => '6162636465666768696a6b6c6d6e6f70']);

        $this->configHelper->expects($this->once())
            ->method('getDecryptionKey')
            ->willReturn(bin2hex(openssl_random_pseudo_bytes(16)));

        $this->unifiedLogger->expects($this->exactly(1))
            ->method('debug')
            ->with(['message' => 'Decrypted webhook is empty.', 'class' => ProcessEncrypted::class . '::execute()', 'data' => $requestData])
            ->willReturnSelf();

        $this->processEncrypted->execute($requestData, $headers);
    }
}
