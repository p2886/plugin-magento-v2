<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Http;

use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Model\Method\Logger;
use PeachPayments\Hosted\Gateway\Http\Client;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    private JsonSerializer|MockObject $jsonSerializerMock;
    private UnifiedLogger|MockObject $unifiedLogger;
    private Client $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->jsonSerializerMock = $this->createMock(JsonSerializer::class);
        $this->unifiedLogger = $this->createMock(UnifiedLogger::class);
        $this->object = $this->getMockBuilder(Client::class)
            ->setConstructorArgs([$this->jsonSerializerMock, $this->unifiedLogger])
            ->setMethods(['get', 'post', 'makeRequest', 'getBody', 'setHeaders'])
            ->getMock();

        $this->transferObjectMock = $this->createMock(TransferInterface::class);
    }

    /**
     * @return void
     * @throws ClientException
     * @throws \Magento\Payment\Gateway\Http\ConverterException
     */
    public function testPlaceRequestGet(): void
    {
        $uri = 'http://example.com';
        $headers = ['Content-Type' => 'application/json'];
        $responseBody = '{"key": "value"}';
        $responseArray = ['key' => 'value'];

        $this->transferObjectMock->expects($this->any())
            ->method('getUri')
            ->willReturn($uri);
        $this->transferObjectMock->expects($this->once())
            ->method('getMethod')
            ->willReturn('GET');
        $this->transferObjectMock->expects($this->once())
            ->method('getHeaders')
            ->willReturn($headers);

        $this->object->expects($this->once())
            ->method('setHeaders')
            ->with($headers);
        $this->object->expects($this->once())
            ->method('get')
            ->with($uri);
        $this->object->expects($this->once())
            ->method('getBody')
            ->willReturn($responseBody);
        $this->jsonSerializerMock->expects($this->once())
            ->method('unserialize')
            ->with($responseBody)
            ->willReturn($responseArray);
        $this->unifiedLogger->expects($this->once())
            ->method('debug')
            ->with(
                ['Request uri' => $uri, 'Response' => $responseBody]
            );

        $result = $this->object->placeRequest($this->transferObjectMock);
        $this->assertEquals($responseArray, $result);
    }

    /**
     * @return void
     * @throws ClientException
     * @throws \Magento\Payment\Gateway\Http\ConverterException
     */
    public function testPlaceRequestPost(): void
    {
        $uri = 'http://example.com';
        $method = 'POST';
        $headers = ['Content-Type' => 'application/json'];
        $body = ['param' => 'value'];
        $responseBody = '{"key": "value"}';
        $responseArray = ['key' => 'value'];

        $this->transferObjectMock->expects($this->any())
            ->method('getUri')
            ->willReturn($uri);
        $this->transferObjectMock->expects($this->once())
            ->method('getMethod')
            ->willReturn($method);
        $this->transferObjectMock->expects($this->once())
            ->method('getHeaders')
            ->willReturn($headers);
        $this->transferObjectMock->expects($this->once())
            ->method('getBody')
            ->willReturn($body);

        $this->object->expects($this->once())
            ->method('setHeaders')
            ->with($headers);
        $this->object->expects($this->once())
            ->method('post')
            ->with($uri, $body);
        $this->object->expects($this->once())
            ->method('getBody')
            ->willReturn($responseBody);
        $this->jsonSerializerMock->expects($this->once())
            ->method('unserialize')
            ->with($responseBody)
            ->willReturn($responseArray);
        $this->unifiedLogger->expects($this->once())
            ->method('debug')
            ->with(
                ['Request uri' => $uri, 'Response' => $responseBody]
            );

        $result = $this->object->placeRequest($this->transferObjectMock);
        $this->assertEquals($responseArray, $result);
    }

    /**
     * @return void
     * @throws ClientException
     * @throws \Magento\Payment\Gateway\Http\ConverterException
     */
    public function testPlaceRequestNoMethods(): void
    {
        $uri = 'http://example.com';
        $method = 'UPDATE';
        $headers = ['Content-Type' => 'application/json'];
        $body = ['param' => 'value'];
        $responseBody = '{"key": "value"}';
        $responseArray = ['key' => 'value'];

        $this->transferObjectMock->expects($this->any())
            ->method('getUri')
            ->willReturn($uri);
        $this->transferObjectMock->expects($this->atLeast(2))
            ->method('getMethod')
            ->willReturn($method);
        $this->transferObjectMock->expects($this->once())
            ->method('getHeaders')
            ->willReturn($headers);
        $this->transferObjectMock->expects($this->once())
            ->method('getBody')
            ->willReturn($body);

        $this->object->expects($this->once())
            ->method('setHeaders')
            ->with($headers);
        $this->object->expects($this->once())
            ->method('makeRequest')
            ->with($method, $uri);
        $this->object->expects($this->once())
            ->method('getBody')
            ->willReturn($responseBody);
        $this->jsonSerializerMock->expects($this->once())
            ->method('unserialize')
            ->with($responseBody)
            ->willReturn($responseArray);
        $this->unifiedLogger->expects($this->once())
            ->method('debug')
            ->with(
                ['Request uri' => $uri, 'Response' => $responseBody]
            );

        $result = $this->object->placeRequest($this->transferObjectMock);
        $this->assertEquals($responseArray, $result);
    }

    /**
     * @return void
     * @throws ClientException
     * @throws \Magento\Payment\Gateway\Http\ConverterException
     */
    public function testPlaceRequestThrowsException(): void
    {
        $uri = 'http://example.com';
        $method = 'GET';
        $headers = ['Content-Type' => 'application/json'];

        $this->transferObjectMock->expects($this->any())
            ->method('getUri')
            ->willReturn($uri);
        $this->transferObjectMock->expects($this->once())
            ->method('getMethod')
            ->willReturn($method);
        $this->transferObjectMock->expects($this->once())
            ->method('getHeaders')
            ->willReturn($headers);

        $this->object->expects($this->once())
            ->method('setHeaders')
            ->with($headers);
        $this->object->expects($this->once())
            ->method('get')
            ->with($uri);
        $this->object->expects($this->once())
            ->method('getBody')
            ->willThrowException(new \Exception('Error message'));

        $this->unifiedLogger->expects($this->once())
            ->method('debug')
            ->with(
                ['Request uri' => $uri, 'Exception' => 'Error message']
            );

        $this->expectException(ClientException::class);
        $this->expectExceptionMessage('Error message');
        $this->object->placeRequest($this->transferObjectMock);
    }
}
