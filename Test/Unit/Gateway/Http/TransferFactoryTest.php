<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Http;

use Magento\Payment\Gateway\Http\TransferInterface;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Gateway\Http\TransferFactory;
use PeachPayments\Hosted\Gateway\Request\RegistrationDataBuilder;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class TransferFactoryTest extends TestCase
{
    private ConfigHelper $configHelperMock;
    private HttpTransferObject $httpTransferObjectMock;
    private TransferFactory $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->httpTransferObjectMock = $this->createMock(HttpTransferObject::class);
        $this->object = new TransferFactory($this->configHelperMock, $this->httpTransferObjectMock);

        $this->transferMock = $this->createMock(TransferInterface::class);
    }

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testCreateWithToken(): void
    {
        $request = [
            RegistrationDataBuilder::TOKEN => 'sample_token',
            'amount' => 100,
            'currency' => 'USD'
        ];
        $expectedUri = 'https://api.example.com/payments';

        $this->configHelperMock->expects($this->once())
            ->method('getApiRegistrationUri')
            ->with('sample_token')
            ->willReturn($expectedUri);
        $this->httpTransferObjectMock->expects($this->once())
            ->method('create')
            ->with($expectedUri, 'POST', ['amount' => 100, 'currency' => 'USD'])
            ->willReturn($this->transferMock);

        $result = $this->object->create($request);
        $this->assertEquals($this->transferMock, $result);
    }

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testCreateWithTransactionId(): void
    {
        $request = [
            AuthorizationTrxIdHandler::KEY_TNX_ID => '123456',
            'amount' => 100,
            'currency' => 'USD'
        ];
        $expectedUri = 'https://api.example.com/payments';

        $this->configHelperMock->expects($this->once())
            ->method('getApiPaymentsUri')
            ->with('123456')
            ->willReturn($expectedUri);
        $this->httpTransferObjectMock->expects($this->once())
            ->method('create')
            ->with($expectedUri, 'POST', ['amount' => 100, 'currency' => 'USD'])
            ->willReturn($this->transferMock);

        $result = $this->object->create($request);
        $this->assertEquals($this->transferMock, $result);
    }

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testCreateWithNoTokenOrTransactionId(): void
    {
        $request = [
            'amount' => 100,
            'currency' => 'USD'
        ];
        $expectedUri = 'https://api.example.com/payments';

        $this->configHelperMock->expects($this->once())
            ->method('getApiPaymentsUri')
            ->with(null)
            ->willReturn($expectedUri);
        $this->httpTransferObjectMock->expects($this->once())
            ->method('create')
            ->with($expectedUri, 'POST', $request)
            ->willReturn($this->transferMock);

        $result = $this->object->create($request);
        $this->assertEquals($this->transferMock, $result);
    }
}
