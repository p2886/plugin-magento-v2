<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Request;

use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Api\PaymentTokenManagementInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Request\RegistrationDataBuilder;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class RegistrationDataBuilderTest extends TestCase
{
    private SubjectReader $subjectReaderMock;
    private PaymentTokenManagementInterface $paymentTokenManagementMock;
    private RegistrationDataBuilder $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->subjectReaderMock = $this->createMock(SubjectReader::class);
        $this->paymentTokenManagementMock = $this->createMock(PaymentTokenManagementInterface::class);

        $this->object = new RegistrationDataBuilder(
            $this->subjectReaderMock,
            $this->paymentTokenManagementMock
        );

        $this->paymentDataObjectMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->createMock(InfoInterface::class);
        $this->orderMock = $this->createMock(OrderAdapterInterface::class);
        $this->paymentTokenMock = $this->createMock(PaymentTokenInterface::class);
    }

    /**
     * @return void
     */
    public function testBuildWithToken(): void
    {
        $publicHash = 'public_hash';
        $gatewayToken = 'gateway_token';
        $customerId = 1;

        $this->subjectReaderMock->expects($this->exactly(2))
            ->method('readPayment')
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getOrder')
            ->willReturn($this->orderMock);
        $this->paymentMock->expects($this->once())
            ->method('getAdditionalInformation')
            ->with('public_hash')
            ->willReturn($publicHash);
        $this->orderMock->expects($this->once())
            ->method('getCustomerId')
            ->willReturn($customerId);
        $this->paymentTokenManagementMock->expects($this->once())
            ->method('getByPublicHash')
            ->with($publicHash, $customerId)
            ->willReturn($this->paymentTokenMock);
        $this->paymentTokenMock->expects($this->once())
            ->method('getGatewayToken')
            ->willReturn($gatewayToken);

        $expectedResult = [RegistrationDataBuilder::TOKEN => $gatewayToken];
        $result = $this->object->build(['payment' => $this->paymentDataObjectMock]);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return void
     */
    public function testBuildWithoutToken(): void
    {
        $publicHash = 'public_hash';
        $customerId = 1;

        $this->subjectReaderMock->expects($this->exactly(2))
            ->method('readPayment')
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getOrder')
            ->willReturn($this->orderMock);
        $this->paymentMock->expects($this->once())
            ->method('getAdditionalInformation')
            ->with('public_hash')
            ->willReturn($publicHash);
        $this->orderMock->expects($this->once())
            ->method('getCustomerId')
            ->willReturn($customerId);
        $this->paymentTokenManagementMock->expects($this->once())
            ->method('getByPublicHash')
            ->with($publicHash, $customerId)
            ->willReturn(null);

        $expectedResult = [];
        $result = $this->object->build(['payment' => $this->paymentDataObjectMock]);
        $this->assertEquals($expectedResult, $result);
    }
}
