<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Request;

use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order\Creditmemo;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Request\PaymentDataBuilder;
use PeachPayments\Hosted\Gateway\Request\RefundDataBuilder;
use PHPUnit\Framework\TestCase;

class RefundDataBuilderTest extends TestCase
{
    private SubjectReader $subjectReaderMock;
    private RefundDataBuilder $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->subjectReaderMock = $this->createMock(SubjectReader::class);
        $this->object = new RefundDataBuilder($this->subjectReaderMock);

        $this->paymentDataObjectMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->getMockBuilder(InfoInterface::class)
            ->addMethods(['getCreditmemo'])
            ->getMockForAbstractClass();
        $this->creditMemoMock = $this->createMock(Creditmemo::class);
        $this->orderMock = $this->createMock(OrderAdapterInterface::class);
    }

    /**
     * @return void
     */
    public function testBuild(): void
    {
        $grandTotal = 100.00;
        $currencyCode = 'USD';

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->once())
            ->method('getCreditmemo')
            ->willReturn($this->creditMemoMock);
        $this->creditMemoMock->expects($this->once())
            ->method('getGrandTotal')
            ->willReturn($grandTotal);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getOrder')
            ->willReturn($this->orderMock);
        $this->orderMock->expects($this->once())
            ->method('getCurrencyCode')
            ->willReturn($currencyCode);

        $expectedResult = [
            PaymentDataBuilder::PAYMENT_TYPE => RefundDataBuilder::TYPE,
            PaymentDataBuilder::AMOUNT       => number_format($grandTotal, 2, '.', ''),
            PaymentDataBuilder::CURRENCY     => $currencyCode,
        ];
        $result = $this->object->build(['payment' => $this->paymentDataObjectMock]);
        $this->assertEquals($expectedResult, $result);
    }
}
