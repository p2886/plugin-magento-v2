<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Request;

use Magento\Framework\Module\Manager;
use Magento\Framework\ObjectManagerInterface;
use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Request\StandingInstructionDataBuilder;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PeachPayments\Hosted\Query\GetSubscriptionExpiryAndFrequency;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class StandingInstructionDataBuilderTest extends TestCase
{
    private SubjectReader $subjectReaderMock;
    private CartRepositoryInterface $cartRepositoryMock;
    private ObjectManagerInterface $objectManagerMock;
    private GetSubscriptionExpiryAndFrequency $expiryAndFrequencyQueryMock;
    private StandingInstructionDataBuilder $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->subjectReaderMock = $this->createMock(SubjectReader::class);
        $this->cartRepositoryMock = $this->createMock(CartRepositoryInterface::class);
        $this->objectManagerMock = $this->createMock(ObjectManagerInterface::class);
        $this->expiryAndFrequencyQueryMock = $this->createMock(GetSubscriptionExpiryAndFrequency::class);
        $this->moduleManagerMock = $this->createMock(Manager::class);

        /**
         * Behaviors for ParadoxLabs_Subscriptions module existing
         *  @note: it works only for ONE test, if another test method exist the class_exists(...) is always true
         */
        $this->quoteManager = $this->getQuoteManager();

        $this->moduleManagerMock->expects($this->once())
            ->method('isEnabled')
            ->with('ParadoxLabs_Subscriptions')
            ->willReturn(true);
        $this->objectManagerMock->method('get')
            ->with('ParadoxLabs\Subscriptions\Model\Service\QuoteManager')
            ->willReturn($this->quoteManager);

        $this->object = new StandingInstructionDataBuilder(
            $this->subjectReaderMock,
            $this->cartRepositoryMock,
            $this->objectManagerMock,
            $this->expiryAndFrequencyQueryMock,
            $this->moduleManagerMock
        );

        $this->paymentDataObjectMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->getMockBuilder(InfoInterface::class)
            ->addMethods(['getOrder'])
            ->getMockForAbstractClass();
        $this->orderMock = $this->getMockBuilder(OrderAdapterInterface::class)
            ->addMethods(['getQuoteId'])
            ->getMockForAbstractClass();
        $this->cartMock = $this->createMock(CartInterface::class);
    }

    /**
     * @return MockObject
     */
    private function getQuoteManager(): MockObject
    {
        if (class_exists('ParadoxLabs\Subscriptions\Model\Service\QuoteManager')) {
            return $this->createMock('ParadoxLabs\Subscriptions\Model\Service\QuoteManager');
        }
        return $this->getMockBuilder('ParadoxLabs\Subscriptions\Model\Service\QuoteManager')
            ->setMethods(['quoteContainsSubscription'])
            ->getMock();
    }

    public function testBuildWithSubscriptionWithoutTransaction()
    {
        $transactionId = "";
        $orderId = 1;

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->once())
            ->method('getOrder')
            ->willReturn($this->orderMock);
        $this->paymentMock->expects($this->once())
            ->method('getAdditionalInformation')
            ->with(AuthorizationTrxIdHandler::KEY_TNX_ID)
            ->willReturn($transactionId);
        $this->orderMock->expects($this->once())
            ->method('getQuoteId')
            ->willReturn($orderId);
        $this->cartRepositoryMock->expects($this->once())
            ->method('get')
            ->with($orderId)
            ->willReturn($this->cartMock);
        $this->quoteManager->expects($this->once())
            ->method('quoteContainsSubscription')
            ->willReturn(true);
        $this->expiryAndFrequencyQueryMock->expects($this->once())
            ->method('execute')
            ->willReturn(['2030-12-31', 'monthly']);

        $expectedResult = [
            'standingInstruction.mode'      => 'INITIAL',
            'standingInstruction.type'      => 'RECURRING',
            'standingInstruction.source'    => 'CIT',
            'standingInstruction.expiry'    => '2030-12-31',
            'standingInstruction.frequency' => 'monthly'
        ];
        $result = $this->object->build(['payment' => $this->paymentDataObjectMock]);
        $this->assertEquals($expectedResult, $result);
    }
}
