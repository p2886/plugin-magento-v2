<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Request;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Request\TransactionIdDataBuilder;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PHPUnit\Framework\TestCase;

class TransactionIdDataBuilderTest extends TestCase
{
    private SubjectReader $subjectReaderMock;
    private TransactionIdDataBuilder $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->subjectReaderMock = $this->createMock(SubjectReader::class);
        $this->object = new TransactionIdDataBuilder($this->subjectReaderMock);

        $this->paymentDataObjectMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->createMock(InfoInterface::class);
    }

    public function testBuildWithTransactionId()
    {
        $transactionId = '12345';

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->once())
            ->method('getAdditionalInformation')
            ->with(AuthorizationTrxIdHandler::KEY_TNX_ID)
            ->willReturn($transactionId);

        $expectedResult = [
            AuthorizationTrxIdHandler::KEY_TNX_ID => $transactionId
        ];
        $result = $this->object->build(['payment' => $this->paymentDataObjectMock]);
        $this->assertEquals($expectedResult, $result);
    }

    public function testBuildWithoutTransactionId()
    {
        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->once())
            ->method('getAdditionalInformation')
            ->with(AuthorizationTrxIdHandler::KEY_TNX_ID)
            ->willReturn(null);

        $expectedResult = [];
        $result = $this->object->build(['payment' => $this->paymentDataObjectMock]);
        $this->assertEquals($expectedResult, $result);
    }
}
