<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Request;

use Magento\Payment\Gateway\Data\AddressAdapterInterface;
use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Request\CardDataBuilder;
use PHPUnit\Framework\TestCase;

class CardDataBuilderTest extends TestCase
{
    private SubjectReader $subjectReaderMock;
    private CardDataBuilder $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->subjectReaderMock = $this->createMock(SubjectReader::class);
        $this->object = new CardDataBuilder($this->subjectReaderMock);

        $this->paymentDataObjectMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->createMock(InfoInterface::class);
        $this->orderMock = $this->createMock(OrderAdapterInterface::class);
        $this->billingAddressMock = $this->createMock(AddressAdapterInterface::class);
    }

    /**
     * @return void
     */
    public function testBuild()
    {
        $ccNumber = '4111111111111111';
        $ccCid = '123';
        $ccExpMonth = '12';
        $ccExpYear = '2030';
        $firstName = 'TestFirstName';
        $lastName = 'TestLastName';

        $this->subjectReaderMock->expects($this->exactly(2))
            ->method('readPayment')
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getOrder')
            ->willReturn($this->orderMock);
        $this->paymentMock->expects($this->any())
            ->method('getAdditionalInformation')
            ->willReturnMap([
                ['cc_number', $ccNumber],
                ['cc_cid', $ccCid],
                ['cc_exp_month', $ccExpMonth],
                ['cc_exp_year', $ccExpYear]
            ]);
        $this->orderMock->expects($this->exactly(2))
            ->method('getBillingAddress')
            ->willReturn($this->billingAddressMock);
        $this->billingAddressMock->expects($this->once())
            ->method('getFirstname')
            ->willReturn($firstName);
        $this->billingAddressMock->expects($this->once())
            ->method('getLastname')
            ->willReturn($lastName);

        $expectedResult = [
            CardDataBuilder::NUMBER => $ccNumber,
            CardDataBuilder::CVV => $ccCid,
            CardDataBuilder::EXP_MONTH => $ccExpMonth,
            CardDataBuilder::EXP_YEAR => $ccExpYear,
            CardDataBuilder::NAME => $firstName . ' ' . $lastName
        ];

        $result = $this->object->build(['payment' => $this->paymentDataObjectMock]);
        $this->assertEquals($expectedResult, $result);
    }
}
