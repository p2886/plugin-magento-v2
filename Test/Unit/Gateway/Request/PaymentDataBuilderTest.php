<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Request;

use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Request\PaymentDataBuilder;
use PHPUnit\Framework\TestCase;

class PaymentDataBuilderTest extends TestCase
{
    private SubjectReader $subjectReaderMock;
    private PaymentDataBuilder $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->subjectReaderMock = $this->createMock(SubjectReader::class);
        $this->object = new PaymentDataBuilder($this->subjectReaderMock, 'PA');

        $this->paymentDataObjectMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->getMockBuilder(InfoInterface::class)
            ->addMethods(['getAmountOrdered'])
            ->getMockForAbstractClass();
        $this->orderMock = $this->createMock(OrderAdapterInterface::class);
    }

    /**
     * @return void
     */
    public function testBuild(): void
    {
        $amountOrdered = 100.00;
        $currencyCode = 'USD';
        $orderIncrementId = '100001';

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->once())
            ->method('getAmountOrdered')
            ->willReturn($amountOrdered);
        $this->paymentDataObjectMock->expects($this->exactly(2))
            ->method('getOrder')
            ->willReturn($this->orderMock);
        $this->orderMock->expects($this->once())
            ->method('getCurrencyCode')
            ->willReturn($currencyCode);
        $this->orderMock->expects($this->once())
            ->method('getOrderIncrementId')
            ->willReturn($orderIncrementId);

        $expectedResult = [
            PaymentDataBuilder::PAYMENT_TYPE => 'PA',
            PaymentDataBuilder::AMOUNT => number_format($amountOrdered, 2, '.', ''),
            PaymentDataBuilder::CURRENCY => $currencyCode,
            'merchantTransactionId' => $orderIncrementId
        ];
        $result = $this->object->build(['payment' => $this->paymentDataObjectMock]);
        $this->assertEquals($expectedResult, $result);
    }
}
