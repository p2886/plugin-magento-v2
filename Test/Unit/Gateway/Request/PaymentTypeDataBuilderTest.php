<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);
namespace PeachPayments\Hosted\Test\Unit\Gateway\Request;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Request\PaymentTypeDataBuilder;
use PHPUnit\Framework\TestCase;

class PaymentTypeDataBuilderTest extends TestCase
{
    private SubjectReader $subjectReaderMock;
    private PaymentTypeDataBuilder $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->subjectReaderMock = $this->createMock(SubjectReader::class);
        $this->object = new PaymentTypeDataBuilder($this->subjectReaderMock);

        $this->paymentDataObjectMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->createMock(InfoInterface::class);
    }

    /**
     * @return void
     */
    public function testBuild(): void
    {
        $ccType = 'VI';
        $ccBrandCode = 'VISA';

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->once())
            ->method('getAdditionalInformation')
            ->with('cc_type')
            ->willReturn($ccType);

        $expectedResult = [
            PaymentTypeDataBuilder::CC_TYPE => $ccBrandCode,
        ];
        $result = $this->object->build(['payment' => $this->paymentDataObjectMock]);
        $this->assertEquals($expectedResult, $result);
    }
}
