<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Request;

use PeachPayments\Hosted\Gateway\Request\AuthDataBuilder;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHelper;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class AuthDataBuilderTest extends TestCase
{
    private PeachPaymentsHelper $peachPaymentsHelperMock;
    private AuthDataBuilder $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->peachPaymentsHelperMock = $this->createMock(PeachPaymentsHelper::class);
        $this->object = new AuthDataBuilder($this->peachPaymentsHelperMock);
    }

    /**
     * @return void
     */
    public function testBuild(): void
    {
        $entityId = 'test_entity_id';
        $this->peachPaymentsHelperMock->expects($this->once())
            ->method('getEntityId')
            ->willReturn($entityId);

        $result = $this->object->build([]);
        $this->assertEquals([AuthDataBuilder::ID => $entityId], $result);
    }
}
