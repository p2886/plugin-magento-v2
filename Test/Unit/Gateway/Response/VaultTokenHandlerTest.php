<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Response;

use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Api\Data\OrderPaymentExtensionInterface;
use Magento\Sales\Api\Data\OrderPaymentExtensionInterfaceFactory;
use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Response\VaultTokenHandler;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHelper;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class VaultTokenHandlerTest extends TestCase
{
    private SubjectReader $subjectReaderMock;
    private PaymentTokenFactoryInterface $paymentTokenFactoryMock;
    private JsonSerializer $jsonSerializerMock;
    private OrderPaymentExtensionInterfaceFactory $paymentExtensionFactoryMock;
    private ConfigHelper $configHelperMock;
    private PeachPaymentsHelper $peachPaymentsHelperMock;
    private VaultTokenHandler $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->subjectReaderMock = $this->createMock(SubjectReader::class);
        $this->paymentTokenFactoryMock = $this->createMock(PaymentTokenFactoryInterface::class);
        $this->jsonSerializerMock = $this->createMock(JsonSerializer::class);
        $this->paymentExtensionFactoryMock = $this->createMock(OrderPaymentExtensionInterfaceFactory::class);
        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->peachPaymentsHelperMock = $this->createMock(PeachPaymentsHelper::class);
        $this->object = new VaultTokenHandler(
            $this->subjectReaderMock,
            $this->paymentTokenFactoryMock,
            $this->jsonSerializerMock,
            $this->paymentExtensionFactoryMock,
            $this->configHelperMock,
            $this->peachPaymentsHelperMock
        );

        $this->paymentDOMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->getMockBuilder(InfoInterface::class)
            ->addMethods(['getExtensionAttributes', 'setExtensionAttributes'])
            ->getMockForAbstractClass();
        $this->paymentTockenMock = $this->createMock(PaymentTokenInterface::class);
        $this->extensionAttributesMock = $this->createMock(OrderPaymentExtensionInterface::class);
    }

    /**
     * @return void
     */
    public function testHandleWithVaultToken(): void
    {
        $response = [
            'registrationId' => 'token123',
            'card' => [
                'expiryYear' => '2030',
                'expiryMonth' => '12',
                'last4Digits' => '1234'
            ],
            'paymentBrand' => 'VISA'
        ];
        $expirationDate = '2030-12-31 23:59:59';

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->with(['payment' => $this->paymentDOMock])
            ->willReturn($this->paymentDOMock);
        $this->paymentDOMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);

        //getVaultToken START
        $this->peachPaymentsHelperMock->expects($this->once())
            ->method('getCardExpirationDateForVault')
            ->with('2030', '12')
            ->willReturn($expirationDate);
        $this->paymentTokenFactoryMock->expects($this->once())
            ->method('create')
            ->with(PaymentTokenFactoryInterface::TOKEN_TYPE_CREDIT_CARD)
            ->willReturn($this->paymentTockenMock);
        $this->paymentTockenMock->expects($this->once())
            ->method('setGatewayToken')
            ->with('token123');
        $this->paymentTockenMock->expects($this->once())
            ->method('setExpiresAt')
            ->with($expirationDate);
        $this->paymentTockenMock->expects($this->once())
            ->method('setIsVisible')
            ->with(true);
        $this->paymentTockenMock->expects($this->once())
            ->method('setTokenDetails')
            ->willReturnSelf();
        $this->configHelperMock->expects($this->once())
            ->method('getPaymentBrandByPeachPaymentsCode')
            ->with('VISA')
            ->willReturn('VISA');
        //getVaultToken END

        //getExtensionAttributes START
        $this->paymentMock->expects($this->once())
            ->method('getExtensionAttributes')
            ->willReturn(null);
        $this->paymentExtensionFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->extensionAttributesMock);
        $this->paymentMock->expects($this->once())
            ->method('setExtensionAttributes')
            ->with($this->extensionAttributesMock);
        //getExtensionAttributes END

        $this->extensionAttributesMock->expects($this->once())
            ->method('setVaultPaymentToken')
            ->with($this->paymentTockenMock);
        $this->paymentMock->expects($this->once())
            ->method('setAdditionalInformation')
            ->with('is_active_payment_token_enabler', true);

        $this->object->handle(['payment' => $this->paymentDOMock], $response);
    }
}
