<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Response;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PHPUnit\Framework\TestCase;

class AuthorizationTrxIdHandlerTest extends TestCase
{
    private SubjectReader $subjectReaderMock;
    private AuthorizationTrxIdHandler $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->subjectReaderMock = $this->createMock(SubjectReader::class);
        $this->object = new AuthorizationTrxIdHandler($this->subjectReaderMock);

        $this->paymentDataObjectMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->getMockBuilder(InfoInterface::class)
            ->addMethods(['setLastTransId', 'setTransactionId'])
            ->getMockForAbstractClass();
    }

    /**
     * @return void
     */
    public function testHandleWithTransactionId(): void
    {
        $transactionId = '123456';
        $response = ['id' => $transactionId];

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->with(['payment' => $this->paymentDataObjectMock])
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);

        $this->paymentMock->expects($this->once())
            ->method('setLastTransId')
            ->with($transactionId);
        $this->paymentMock->expects($this->once())
            ->method('setTransactionId')
            ->with($transactionId);
        $this->paymentMock->expects($this->once())
            ->method('setAdditionalInformation')
            ->with(AuthorizationTrxIdHandler::KEY_TNX_ID, $transactionId);

        $this->object->handle(['payment' => $this->paymentDataObjectMock], $response);
    }

    /**
     * @return void
     */
    public function testHandleWithoutTransactionId(): void
    {
        $response = [];

        $this->subjectReaderMock->expects($this->once())
            ->method('readPayment')
            ->with(['payment' => $this->paymentDataObjectMock])
            ->willReturn($this->paymentDataObjectMock);
        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);

        $this->paymentMock->expects($this->never())->method('setLastTransId');
        $this->paymentMock->expects($this->never())->method('setTransactionId');
        $this->paymentMock->expects($this->never())->method('setAdditionalInformation');

        $this->object->handle(['payment' => $this->paymentDataObjectMock], $response);
    }
}
