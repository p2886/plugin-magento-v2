<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway;

use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Model\InfoInterface;
use PeachPayments\Hosted\Gateway\CopyAndPaySale;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PeachPayments\Hosted\Gateway\Response\VaultTokenHandler;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class CopyAndPaySaleTest extends TestCase
{
    private VaultTokenHandler $vaultTokenHandlerMock;
    private JsonSerializer $jsonSerializerMock;
    private CopyAndPaySale $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->vaultTokenHandlerMock = $this->createMock(VaultTokenHandler::class);
        $this->jsonSerializerMock = $this->createMock(JsonSerializer::class);
        $this->object = new CopyAndPaySale(
            $this->vaultTokenHandlerMock,
            $this->jsonSerializerMock
        );

        $this->paymentDataObjectMock = $this->createMock(PaymentDataObjectInterface::class);
        $this->paymentMock = $this->getMockBuilder(InfoInterface::class)
            ->addMethods(['setTransactionId'])
            ->getMockForAbstractClass();
    }

    /**
     * @return void
     * @throws \Magento\Payment\Gateway\Command\CommandException
     */
    public function testExecuteWithRegistrationId(): void
    {
        $trxId = '123456';
        $response = [
            'registrationId' => 'reg123'
        ];

        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->atLeast(2))
            ->method('getAdditionalInformation')
            ->withConsecutive(
                [AuthorizationTrxIdHandler::KEY_TNX_ID],
                [AuthorizationTrxIdHandler::KEY_RESPONSE_DETAILS]
            )
            ->willReturnOnConsecutiveCalls($trxId, json_encode($response));
        $this->jsonSerializerMock->expects($this->once())
            ->method('unserialize')
            ->with(json_encode($response))
            ->willReturn($response);
        $this->vaultTokenHandlerMock->expects($this->once())
            ->method('handle');
        $this->paymentMock->expects($this->once())
            ->method('setTransactionId')
            ->with($trxId);

        $this->object->execute(['payment' => $this->paymentDataObjectMock]);
    }

    /**
     * @return void
     * @throws \Magento\Payment\Gateway\Command\CommandException
     */
    public function testExecuteWithoutRegistrationId(): void
    {
        $trxId = '123456';
        $response = [
            'someOtherKey' => 'value'
        ];

        $this->paymentDataObjectMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->atLeast(2))
            ->method('getAdditionalInformation')
            ->withConsecutive(
                [AuthorizationTrxIdHandler::KEY_TNX_ID],
                [AuthorizationTrxIdHandler::KEY_RESPONSE_DETAILS]
            )
            ->willReturnOnConsecutiveCalls($trxId, json_encode($response));
        $this->jsonSerializerMock->expects($this->once())
            ->method('unserialize')
            ->with(json_encode($response))
            ->willReturn($response);
        $this->paymentMock->expects($this->once())
            ->method('setTransactionId')
            ->with($trxId);

        $this->object->execute(['payment' => $this->paymentDataObjectMock]);
    }
}
