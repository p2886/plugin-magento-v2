<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Config;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Model\Quote;
use PeachPayments\Hosted\Gateway\Config\Active;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PHPUnit\Framework\TestCase;

class ActiveTest extends TestCase
{
    private ConfigHelper $configHelperMock;
    private CheckoutSession $checkoutSessionMock;
    private ObjectManagerInterface $objectManagerMock;
    private Active $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->checkoutSessionMock = $this->createMock(CheckoutSession::class);
        $this->objectManagerMock = $this->createMock(ObjectManagerInterface::class);

        $this->object = new Active(
            $this->configHelperMock,
            $this->checkoutSessionMock,
            $this->objectManagerMock
        );

        $this->quoteMock = $this->createMock(Quote::class);
    }

    /**
     * @return void
     */
    public function testGetValueEmbeddedCheckout(): void
    {
        $this->object->setMethodCode('peachpayments_embedded_checkout');
        $storeId = 1;

        $this->configHelperMock->expects($this->once())
            ->method('isEmbeddedCheckoutEnabled')
            ->with($storeId)
            ->willReturn(true);

        $result = $this->object->getValue('field', $storeId);
        $this->assertTrue($result);
    }

    /**
     * @return void
     */
    public function testGetValueOnlyForSubscription(): void
    {
        $this->object->setMethodCode('some_method');

        $this->configHelperMock->expects($this->once())
            ->method('isOnlyForSubscription')
            ->willReturn(true);
        $this->configHelperMock->expects($this->once())
            ->method('isModuleOutputEnabled')
            ->with('ParadoxLabs_Subscriptions')
            ->willReturn(true);
        $this->checkoutSessionMock->expects($this->once())
            ->method('getQuote')
            ->willReturn($this->quoteMock);
        $this->objectManagerMock->expects($this->once())
            ->method('get')
            ->with('ParadoxLabs\Subscriptions\Model\Service\QuoteManager')
            ->willThrowException(new \Exception("Test Error Message"));

        $result = $this->object->getValue('field');
        $this->assertFalse($result);
    }

    /**
     * @return void
     */
    public function testGetValueServerToServerEnabled(): void
    {
        $this->object->setMethodCode('some_method');

        $this->configHelperMock->expects($this->once())
            ->method('isOnlyForSubscription')
            ->willReturn(false);
        $this->configHelperMock->expects($this->once())
            ->method('isServerToServerEnabled')
            ->willReturn(true);

        $result = $this->object->getValue('field');
        $this->assertTrue($result);
    }
}
