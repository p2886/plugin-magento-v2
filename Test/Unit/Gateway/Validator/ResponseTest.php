<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Validator;

use Magento\Payment\Gateway\Validator\ResultInterface;
use Magento\Payment\Gateway\Validator\ResultInterfaceFactory;
use PeachPayments\Hosted\Gateway\Validator\Response;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    private ResultInterfaceFactory $resultFactoryMock;
    private Response $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->resultFactoryMock = $this->createMock(ResultInterfaceFactory::class);
        $this->object = new Response($this->resultFactoryMock);

        $this->resultMock = $this->createMock(ResultInterface::class);
    }

    /**
     * @return void
     */
    public function testValidateSuccess(): void
    {
        $this->resultFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->resultMock);

        $validationSubject = [
            'response' => [
                'result' => [
                    'code' => '000.000.000',
                    'description' => 'Transaction succeeded'
                ]
            ]
        ];
        $result = $this->object->validate($validationSubject);
        $this->assertSame($this->resultMock, $result);
    }

    /**
     * @return void
     */
    public function testValidateFailure(): void
    {
        $this->resultFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->resultMock);

        $validationSubject = [
            'response' => [
                'result' => [
                    'code' => '999.999.999',
                    'description' => 'Transaction failed'
                ]
            ]
        ];
        $result = $this->object->validate($validationSubject);
        $this->assertSame($this->resultMock, $result);
    }
}
