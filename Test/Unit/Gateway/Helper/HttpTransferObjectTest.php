<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Gateway\Helper;

use Magento\Framework\UrlInterface;
use Magento\Payment\Gateway\Http\TransferBuilderFactory as TransferBuilder;
use Magento\Payment\Gateway\Http\TransferInterface;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Query\GetAccessToken as AccessTokenQuery;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class HttpTransferObjectTest extends TestCase
{
    private TransferBuilder $transferBuilderFactoryMock;
    private ConfigHelper $configHelperMock;
    private AccessTokenQuery $accessTokenQueryMock;
    private UrlInterface $urlMock;
    private HttpTransferObject $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->transferBuilderFactoryMock = $this->createMock(TransferBuilder::class);
        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->accessTokenQueryMock = $this->createMock(AccessTokenQuery::class);
        $this->urlMock = $this->createMock(UrlInterface::class);

        $this->object = new HttpTransferObject(
            $this->transferBuilderFactoryMock,
            $this->configHelperMock,
            $this->accessTokenQueryMock,
            $this->urlMock
        );

        $this->transferBuilderMock = $this->createMock(\Magento\Payment\Gateway\Http\TransferBuilder::class);
        $this->transferMock = $this->createMock(TransferInterface::class);
    }

    /**
     * @return void
     */
    public function testCreateV2(): void
    {
        $uri = 'http://example.com';
        $method = 'POST';
        $body = ['key' => 'value'];
        $headers = [
            'Authorization' => 'Bearer test_token',
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_RETURNTRANSFER => true
        ];

        $this->transferBuilderFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->transferBuilderMock);
        $this->transferBuilderMock->expects($this->once())
            ->method('setUri')
            ->with($uri)
            ->willReturnSelf();
        $this->transferBuilderMock->expects($this->once())
            ->method('setMethod')
            ->with($method)
            ->willReturnSelf();
        $this->transferBuilderMock->expects($this->once())
            ->method('setBody')
            ->with($body)
            ->willReturnSelf();
        $this->transferBuilderMock->expects($this->once())
            ->method('setHeaders')
            ->with($headers)
            ->willReturnSelf();
        $this->transferBuilderMock->expects($this->once())
            ->method('build')
            ->willReturn($this->transferMock);

        $this->configHelperMock->expects($this->once())
            ->method('getAccessToken')
            ->willReturn('test_token');
        $this->configHelperMock->expects($this->once())
            ->method('isLiveMode')
            ->willReturn(true);

        $result = $this->object->create($uri, $method, $body);
        $this->assertEquals($this->transferMock, $result);
    }

    /**
     * @return void
     */
    public function testCreate(): void
    {
        $uri = 'http://example.com';
        $method = 'POST';
        $body = ['key' => 'value'];
        $headers = [
            'Authorization' => 'Bearer test_token_v2',
            'Content-Type' => 'application/json',
            'Origin' => 'http://baseurl.com',
            'Referer' => 'http://baseurl.com'
        ];

        $this->transferBuilderFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->transferBuilderMock);
        $this->transferBuilderMock->expects($this->once())
            ->method('setUri')
            ->with($uri)
            ->willReturnSelf();
        $this->transferBuilderMock->expects($this->once())
            ->method('setMethod')
            ->with($method)
            ->willReturnSelf();
        $this->transferBuilderMock->expects($this->once())
            ->method('setBody')
            ->willReturnSelf();
        $this->transferBuilderMock->expects($this->once())
            ->method('setHeaders')
            ->with($headers)
            ->willReturnSelf();
        $this->transferBuilderMock->expects($this->once())
            ->method('build')
            ->willReturn($this->transferMock);

        $this->accessTokenQueryMock->expects($this->once())
            ->method('execute')
            ->willReturn('test_token_v2');
        $this->urlMock->expects($this->exactly(2))
            ->method('getBaseUrl')
            ->willReturn('http://baseurl.com');

        $result = $this->object->createV2($uri, $method, $body);
        $this->assertEquals($this->transferMock, $result);
    }
}
