<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Observer;

use Magento\Framework\DataObject;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Event;
use Magento\Framework\Event\Observer;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Api\Data\PaymentInterface;
use PeachPayments\Hosted\Observer\AssignData;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class AssignDataTest extends TestCase
{
    private DataObjectFactory $dataObjectFactoryMock;
    private AssignData $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->dataObjectFactoryMock = $this->createMock(DataObjectFactory::class);
        $this->object = new AssignData($this->dataObjectFactoryMock);

        $this->observerMock = $this->createMock(Observer::class);
        $this->eventMock = $this->createMock(Event::class);
        $this->methodMock = $this->createMock(MethodInterface::class);
        $this->dataObjectMock = $this->createMock(DataObject::class);
        $this->paymentMock = $this->createMock(InfoInterface::class);
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    public function testExecute(): void
    {
        $this->observerMock->expects($this->exactly(3))
            ->method('getEvent')
            ->willReturn($this->eventMock);
        $this->eventMock->expects($this->exactly(3))
            ->method('getDataByKey')
            ->willReturnCallback(function ($key) {
                $data = [
                    'method' => $this->methodMock,
                    'data' => $this->dataObjectMock,
                    'payment_model' => $this->paymentMock
                ];
                return $data[$key];
            });
        $this->methodMock->expects($this->exactly(2))
            ->method('getCode')
            ->willReturn('peachpayments_server_to_server');

        $additionalData = [
            'cc_cid' => '123',
            'cc_type' => 'VI',
            'cc_exp_year' => '2030',
            'cc_exp_month' => '07',
            'cc_number' => '4111111111111111'
        ];
        $this->dataObjectMock->expects($this->once())
            ->method('getData')
            ->with(PaymentInterface::KEY_ADDITIONAL_DATA)
            ->willReturn($additionalData);
        $this->dataObjectFactoryMock->expects($this->once())
            ->method('create')
            ->with(['data' => $additionalData])
            ->willReturnOnConsecutiveCalls(new DataObject($additionalData));
        $this->paymentMock->expects($this->exactly(5))
            ->method('setAdditionalInformation')
            ->withConsecutive(
                ['cc_cid', '123'],
                ['cc_type', 'VI'],
                ['cc_exp_year', '2030'],
                ['cc_exp_month', '07'],
                ['cc_number', '4111111111111111']
            );

        $this->object->execute($this->observerMock);
    }
}
