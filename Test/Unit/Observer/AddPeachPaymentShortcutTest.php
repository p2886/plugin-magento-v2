<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Observer;

use Magento\Catalog\Block\ShortcutButtons;
use Magento\Framework\Event;
use Magento\Framework\Event\Observer;
use Magento\Framework\View\LayoutInterface;
use PeachPayments\Hosted\Block\Product\Shortcut\Button;
use PeachPayments\Hosted\Helper\Config;
use PeachPayments\Hosted\Observer\AddPeachPaymentShortcut;
use PHPUnit\Framework\TestCase;

class AddPeachPaymentShortcutTest extends TestCase
{
    private Config $configMock;
    private AddPeachPaymentShortcut $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        $this->configMock = $this->createMock(Config::class);
        $this->object = new AddPeachPaymentShortcut($this->configMock);

        $this->observerMock = $this->createMock(Observer::class);
        $this->eventMock = $this->getMockBuilder(Event::class)
            ->addMethods(['getContainer'])
            ->getMock();
        $this->shortcutButtonsMock = $this->createMock(ShortcutButtons::class);
        $this->layoutMock = $this->createMock(LayoutInterface::class);
        $this->buttonMock = $this->createMock(Button::class);
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function testExecute(): void
    {
        $this->configMock->expects($this->once())
            ->method('isEmbeddedCheckoutEnabled')
            ->willReturn(true);
        $this->configMock->expects($this->once())
            ->method('isBuyNowButtonEnabled')
            ->willReturn(true);
        $this->observerMock->expects($this->once())
            ->method('getData')
            ->with('is_catalog_product')
            ->willReturn(true);
        $this->observerMock->expects($this->once())
            ->method('getEvent')
            ->willReturn($this->eventMock);
        $this->eventMock->expects($this->once())
            ->method('getContainer')
            ->willReturn($this->shortcutButtonsMock);
        $this->shortcutButtonsMock->expects($this->once())
            ->method('getLayout')
            ->willReturn($this->layoutMock);
        $this->layoutMock->expects($this->once())
            ->method('createBlock')
            ->with(AddPeachPaymentShortcut::PEACH_PAYMENT_SHORTCUT_BLOCK)
            ->willReturn($this->buttonMock);
        $this->shortcutButtonsMock->expects($this->once())
            ->method('addShortcut')
            ->with($this->buttonMock);

        $this->object->execute($this->observerMock);
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function testExecuteWithDisabledConfig(): void
    {
        $this->configMock->expects($this->once())
            ->method('isEmbeddedCheckoutEnabled')
            ->willReturn(false);
        $this->configMock->expects($this->never())
            ->method('isBuyNowButtonEnabled');
        $this->observerMock->expects($this->never())
            ->method('getData');

        $this->object->execute($this->observerMock);
    }
}
