<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Model\Api;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Payment\Gateway\Http\TransferInterface;
use PeachPayments\Hosted\Command\GetVaultGatewayTokensForCart;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use PeachPayments\Hosted\Gateway\Request\AuthDataBuilder;
use PeachPayments\Hosted\Gateway\Request\PaymentDataBuilder;
use PeachPayments\Hosted\Gateway\Request\VaultDataBuilder;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Model\Api\CopyAndPayPrepareAddNewCartForm;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class CopyAndPayPrepareAddNewCartFormTest extends TestCase
{
    private ConfigHelper $configHelperMock;
    private CurlClient $curlClientMock;
    private JsonSerializer $jsonMock;
    private GetVaultGatewayTokensForCart $gatewayTokensForCartMock;
    private CustomerSession $customerSessionMock;
    private HttpTransferObject $httpTransferObjectMock;
    private CopyAndPayPrepareAddNewCartForm $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->curlClientMock = $this->createMock(CurlClient::class);
        $this->jsonMock = $this->createMock(JsonSerializer::class);
        $this->gatewayTokensForCartMock = $this->createMock(GetVaultGatewayTokensForCart::class);
        $this->customerSessionMock = $this->createMock(CustomerSession::class);
        $this->httpTransferObjectMock = $this->createMock(HttpTransferObject::class);

        $this->object = new CopyAndPayPrepareAddNewCartForm(
            $this->configHelperMock,
            $this->curlClientMock,
            $this->jsonMock,
            $this->gatewayTokensForCartMock,
            $this->customerSessionMock,
            $this->httpTransferObjectMock
        );

        $this->transferMock = $this->createMock(TransferInterface::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $uri = 'https://api.example.com/checkouts';
        $requestData = [
            AuthDataBuilder::ID => 'dummy_entity_id',
            PaymentDataBuilder::PAYMENT_TYPE => 'PA',
            PaymentDataBuilder::CURRENCY => 'ZAR',
            PaymentDataBuilder::AMOUNT => '1.00',
            VaultDataBuilder::VAULT_ON => true,
            VaultDataBuilder::MODE => 'INITIAL',
            VaultDataBuilder::TYPE => 'UNSCHEDULED',
            VaultDataBuilder::SOURCE => 'CIT'
        ];

        $this->configHelperMock->expects($this->once())
            ->method('getCheckoutsUri')
            ->willReturn($uri);
        $this->configHelperMock->expects($this->once())
            ->method('getEntityId3DSecure')
            ->willReturn('dummy_entity_id');
        $this->httpTransferObjectMock->expects($this->once())
            ->method('create')
            ->with($uri, 'POST', $requestData)
            ->willReturn($this->transferMock);
        $this->curlClientMock->expects($this->once())
            ->method('placeRequest')
            ->with($this->transferMock)
            ->willReturn(['id' => 'checkout_id']);
        $this->jsonMock->expects($this->once())
            ->method('serialize')
            ->willReturn('{"checkout_id":"checkout_id"}');

        $result = $this->object->execute();
        $this->assertEquals('{"checkout_id":"checkout_id"}', $result);
    }
}
