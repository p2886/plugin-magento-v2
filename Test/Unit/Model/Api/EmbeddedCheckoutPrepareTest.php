<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Model\Api;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Api\Data\CurrencyInterface;
use Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface as QuoteMaskToQuoteId;
use Magento\Quote\Model\Quote\Payment;
use PeachPayments\Hosted\Command\GetVaultGatewayTokensForCart as RegistrationIdsCommand;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as DataHelper;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\Api\EmbeddedCheckoutPrepare;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use PeachPayments\Hosted\Model\Web\Hooks;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;
use PeachPayments\Hosted\Query\GetSubscriptionExpiryAndFrequency;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class EmbeddedCheckoutPrepareTest extends TestCase
{
    private ConfigHelper $configHelperMock;
    private QuoteMaskToQuoteId $maskToQuoteIdMock;
    private CustomerSession $customerSessionMock;
    private JsonSerializer $jsonSerializerMock;
    private CartRepositoryInterface $cartRepositoryMock;
    private HttpTransferObject $httpTransferObjectMock;
    private CurlClient $curlClientMock;
    private CartInterface $cartMock;
    private RemoteAddress $remoteAddressMock;
    private WebhooksResource $webhookResourceMock;
    private WebhooksFactory $webhookFactoryMock;
    private RegistrationIdsCommand $registrationIdsCommandMock;
    private DataHelper $dataHelperMock;
    private UrlInterface $urlBuilderMock;
    private GetSubscriptionExpiryAndFrequency $expiryAndFrequencyQeuryMock;
    private CookieManagerInterface $cookieManagerMock;
    private UnifiedLogger|MockObject $unifiedLogger;
    private EmbeddedCheckoutPrepare $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->maskToQuoteIdMock = $this->createMock(QuoteMaskToQuoteId::class);
        $this->customerSessionMock = $this->createMock(CustomerSession::class);
        $this->jsonSerializerMock = $this->createMock(JsonSerializer::class);
        $this->cartRepositoryMock = $this->createMock(CartRepositoryInterface::class);
        $this->httpTransferObjectMock = $this->createMock(HttpTransferObject::class);
        $this->curlClientMock = $this->createMock(CurlClient::class);
        $this->remoteAddressMock = $this->createMock(RemoteAddress::class);
        $this->webhookResourceMock = $this->createMock(WebhooksResource::class);
        $this->webhookFactoryMock = $this->createMock(WebhooksFactory::class);
        $this->registrationIdsCommandMock = $this->createMock(RegistrationIdsCommand::class);
        $this->dataHelperMock = $this->createMock(DataHelper::class);
        $this->urlBuilderMock = $this->createMock(UrlInterface::class);
        $this->expiryAndFrequencyQeuryMock = $this->createMock(GetSubscriptionExpiryAndFrequency::class);
        $this->cookieManagerMock = $this->createMock(CookieManagerInterface::class);
        $this->unifiedLogger = $this->createMock(UnifiedLogger::class);

        $this->object = new EmbeddedCheckoutPrepare(
            $this->configHelperMock,
            $this->maskToQuoteIdMock,
            $this->customerSessionMock,
            $this->jsonSerializerMock,
            $this->cartRepositoryMock,
            $this->httpTransferObjectMock,
            $this->curlClientMock,
            $this->remoteAddressMock,
            $this->webhookResourceMock,
            $this->webhookFactoryMock,
            $this->registrationIdsCommandMock,
            $this->dataHelperMock,
            $this->urlBuilderMock,
            $this->expiryAndFrequencyQeuryMock,
            $this->cookieManagerMock,
            $this->unifiedLogger
        );

        $this->cartMock = $this->getMockBuilder(CartInterface::class)
            ->addMethods(['getGrandTotal', 'reserveOrderId', 'getPayment'])
            ->getMockForAbstractClass();
        $this->currencyMock = $this->createMock(CurrencyInterface::class);
        $this->transferMock = $this->createMock(TransferInterface::class);
        $this->paymentMock = $this->createMock(Payment::class);
        $this->webhookMock = $this->createMock(Hooks::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $this->configHelperMock->expects($this->once())
            ->method('isEmbeddedCheckoutEnabled')
            ->willReturn(true);

        //prepareRequest START
        $this->customerSessionMock->expects($this->once())
            ->method('isLoggedIn')
            ->willReturn(true);
        $this->cartRepositoryMock->expects($this->once())
            ->method('get')
            ->willReturn($this->cartMock);
        $this->cartMock->expects($this->any())
            ->method('getGrandTotal')
            ->willReturn(100.0000);
        $this->configHelperMock->expects($this->any())
            ->method('getEntityId3DSecure')
            ->willReturn('entity_id');
        $this->cartMock->expects($this->any())
            ->method('getCurrency')
            ->willReturn($this->currencyMock);
        $this->currencyMock->expects($this->any())
            ->method('getQuoteCurrencyCode')
            ->willReturn('USD');
        $this->cartMock->expects($this->once())
            ->method('reserveOrderId')
            ->willReturnSelf();
        $this->cartMock->expects($this->any())
            ->method('getReservedOrderId')
            ->willReturn('dummy_10001');
        //prepareRequest END

        $this->urlBuilderMock->expects($this->once())
            ->method('getUrl')
            ->willReturn('https://api.example.com/checkouts');
        $this->dataHelperMock->expects($this->once())
            ->method('getUuid')
            ->willReturn('dummy_uiid');
        $this->configHelperMock->expects($this->once())
            ->method('getCheckoutUrlV2')
            ->willReturn('https://api.example.com/checkouts');

        //makeRequestV2 START
        $this->httpTransferObjectMock->expects($this->once())
            ->method('createV2')
            ->willReturn($this->transferMock);
        $this->curlClientMock->expects($this->once())
            ->method('placeRequest')
            ->with($this->transferMock)
            ->willReturn(['id' => 'dummy_id']);
        $this->cartMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->once())
            ->method('setAdditionalInformation')
            ->willReturnSelf();
        $this->cartRepositoryMock->expects($this->once())
            ->method('save')
            ->willReturnSelf();
        $this->webhookFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->webhookMock);
        $this->webhookResourceMock->expects($this->once())
            ->method('load')
            ->willReturnSelf();
        $this->webhookMock->expects($this->once())
            ->method('getId')
            ->willReturn(null);
        $this->webhookMock->expects($this->once())
            ->method('setData')
            ->willReturnSelf();
        $this->webhookResourceMock->expects($this->once())
            ->method('save')
            ->willReturnSelf();
        $this->remoteAddressMock->expects($this->once())
            ->method('getRemoteAddress')
            ->willReturn('dummy_ip_address');
        //makeRequestV1 END

        $this->jsonSerializerMock->expects($this->once())
            ->method('serialize')
            ->willReturn('{"success": true}');

        $result = $this->object->execute('test_cart_id');
        $this->assertEquals('{"success": true}', $result);
    }

    /**
     * @return void
     */
    public function testExecuteEmbeddedCheckoutDisable(): void
    {
        $this->configHelperMock->expects($this->once())
            ->method('isEmbeddedCheckoutEnabled')
            ->willReturn(false);
        $this->jsonSerializerMock->expects($this->once())
            ->method('serialize')
            ->willReturn('[]');

        $result = $this->object->execute('test_cart_id');
        $this->assertEquals('[]', $result);
    }
}
