<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Model\Api;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use PeachPayments\Hosted\Model\Api\HostedPaymentsOrderDetails;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use PeachPayments\Hosted\Model\Web\Hooks;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;
use PeachPayments\Hosted\Service\FormPostCheckout;
use PHPUnit\Framework\TestCase;

class HostedPaymentsOrderDetailsTest extends TestCase
{
    private JsonSerializer $jsonMock;
    private WebhooksFactory $webhookFactoryMock;
    private WebhooksResource $webhookResourceMock;
    private CheckoutSession $checkoutSessionMock;
    private FormPostCheckout $formPostCheckoutMock;
    private HostedPaymentsOrderDetails $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->jsonMock = $this->createMock(JsonSerializer::class);
        $this->webhookFactoryMock = $this->createMock(WebhooksFactory::class);
        $this->webhookResourceMock = $this->createMock(WebhooksResource::class);
        $this->checkoutSessionMock = $this->createMock(CheckoutSession::class);
        $this->formPostCheckoutMock = $this->createMock(FormPostCheckout::class);

        $this->object = new HostedPaymentsOrderDetails(
            $this->jsonMock,
            $this->webhookResourceMock,
            $this->webhookFactoryMock,
            $this->checkoutSessionMock,
            $this->formPostCheckoutMock
        );

        $this->webhookMock = $this->createMock(Hooks::class);
    }

    /**
     * @return void
     */
    public function testExecuteNewWebhook(): void
    {
        $formData = ['key' => 'value'];
        $serializedData = '{"key":"value"}';

        //initWebHook START
        $orderId = 123;
        $orderIncrementId = '000123';
        $this->checkoutSessionMock->expects($this->atLeast(2))
            ->method('getData')
            ->will($this->returnValueMap([
                ['last_order_id', null, $orderId],
                ['last_real_order_id', null, $orderIncrementId]
            ]));
        $this->webhookFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->webhookMock);
        $this->webhookResourceMock->expects($this->once())
            ->method('load')
            ->willReturnSelf();
        $this->webhookMock->expects($this->once())
            ->method('getId')
            ->willReturn(null);
        $this->webhookMock->expects($this->once())
            ->method('addData')
            ->willReturnSelf();
        $this->webhookResourceMock->expects($this->once())
            ->method('save')
            ->with($this->webhookMock)
            ->willReturnSelf();
        //initWebHook END

        $this->formPostCheckoutMock->expects($this->once())
            ->method('getFormData')
            ->willReturn($formData);
        $this->jsonMock->expects($this->once())
            ->method('serialize')
            ->with($formData)
            ->willReturn($serializedData);

        $result = $this->object->execute();
        $this->assertEquals($serializedData, $result);
    }

    /**
     * @return void
     */
    public function testExecuteExistedWebhook(): void
    {
        $formData = ['key' => 'value'];
        $serializedData = '{"key":"value"}';

        //initWebHook START
        $orderId = 123;
        $orderIncrementId = '000123';
        $this->checkoutSessionMock->expects($this->atLeast(2))
            ->method('getData')
            ->will($this->returnValueMap([
                ['last_order_id', null, $orderId],
                ['last_real_order_id', null, $orderIncrementId]
            ]));
        $this->webhookFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->webhookMock);
        $this->webhookResourceMock->expects($this->once())
            ->method('load')
            ->willReturnSelf();
        $this->webhookMock->expects($this->once())
            ->method('getId')
            ->willReturn(123456);
        $this->webhookMock->expects($this->exactly(2))
            ->method('setData')
            ->willReturnSelf();
        $this->webhookResourceMock->expects($this->once())
            ->method('save')
            ->with($this->webhookMock)
            ->willReturnSelf();
        //initWebHook END

        $this->formPostCheckoutMock->expects($this->once())
            ->method('getFormData')
            ->willReturn($formData);
        $this->jsonMock->expects($this->once())
            ->method('serialize')
            ->with($formData)
            ->willReturn($serializedData);

        $result = $this->object->execute();
        $this->assertEquals($serializedData, $result);
    }
}
