<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Model\Api;

use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as DataHelper;
use PeachPayments\Hosted\Model\Api\AvailableHostedPaymentMethodsTitle;
use PeachPayments\Hosted\Query\GetAccessToken as AccessTokenQuery;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class AvailableHostedPaymentMethodsTitleTest extends TestCase
{
    private ConfigHelper $configHelperMock;
    private JsonSerializer $jsonMock;
    private HttpTransferObject $httpTransferObjectMock;
    private DataHelper $dataHelperMock;
    private ClientFactory $clientFactoryMock;
    private AccessTokenQuery $accessTokenQueryMock;
    private AvailableHostedPaymentMethodsTitle $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->jsonMock = $this->createMock(JsonSerializer::class);
        $this->httpTransferObjectMock = $this->createMock(HttpTransferObject::class);
        $this->dataHelperMock = $this->createMock(DataHelper::class);
        $this->clientFactoryMock = $this->createMock(ClientFactory::class);
        $this->accessTokenQueryMock = $this->createMock(AccessTokenQuery::class);

        $this->object = new AvailableHostedPaymentMethodsTitle(
            $this->configHelperMock,
            $this->jsonMock,
            $this->httpTransferObjectMock,
            $this->dataHelperMock,
            $this->clientFactoryMock,
            $this->accessTokenQueryMock
        );

        $this->client = $this->createMock(Client::class);
        $this->response = $this->getMockBuilder(ResponseInterface::class)
            ->addMethods(['getContents'])
            ->getMockForAbstractClass();
    }

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testExecute(): void
    {
        $jsonResponse = '{"success":true}';

        $this->clientFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->client);
        $this->configHelperMock->expects($this->once())
            ->method('getAvailablePaymentMethodsApiUrl')
            ->willReturn('https://api.example.com/payment-methods');
        $this->configHelperMock->expects($this->once())
            ->method('getEntityId3DSecure')
            ->willReturn('entity_id');
        $this->accessTokenQueryMock->expects($this->once())
            ->method('execute')
            ->willReturn('dummy_access_token');
        $this->configHelperMock->expects($this->once())
            ->method('isLiveMode')
            ->willReturn(true);
        $this->jsonMock->expects($this->atLeast(1))
            ->method('serialize')
            ->willReturn('{"data":"value"}');
        $this->client->expects($this->once())
            ->method('request')
            ->willReturn($this->response);
        $this->response->expects($this->once())
            ->method('getBody')
            ->willReturnSelf();
        $this->response->expects($this->once())
            ->method('getContents')
            ->willReturn($jsonResponse);

        $result = $this->object->execute();
        $this->assertEquals($jsonResponse, $result);
    }
}
