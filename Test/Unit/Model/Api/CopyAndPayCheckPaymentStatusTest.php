<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Test\Unit\Model\Api;

use Magento\Payment\Gateway\Http\TransferInterface;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHelper;
use PeachPayments\Hosted\Model\Api\CopyAndPayCheckPaymentStatus;
use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class CopyAndPayCheckPaymentStatusTest extends TestCase
{
    private PeachPaymentsHelper $dataHelperMock;
    private ConfigHelper $configHelperMock;
    private CurlClient $curlClientMock;
    private HttpTransferObject $httpTransferObjectMock;
    private CopyAndPayCheckPaymentStatus $object;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->dataHelperMock = $this->createMock(PeachPaymentsHelper::class);
        $this->configHelperMock = $this->createMock(ConfigHelper::class);
        $this->curlClientMock = $this->createMock(CurlClient::class);
        $this->httpTransferObjectMock = $this->createMock(HttpTransferObject::class);

        $this->object = new CopyAndPayCheckPaymentStatus(
            $this->dataHelperMock,
            $this->configHelperMock,
            $this->curlClientMock,
            $this->httpTransferObjectMock
        );

        $this->transfer = $this->createMock(TransferInterface::class);
    }

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testExecute(): void
    {
        $paymentId = 'test_payment_id';
        $responseBody = 'test_response';

        $this->configHelperMock->expects($this->once())
            ->method('getCheckoutsUri')
            ->with($paymentId)
            ->willReturn('https://api.example.com/checkouts/' . $paymentId);
        $this->configHelperMock->expects($this->once())
            ->method('getEntityId3DSecure')
            ->willReturn('entity_id');
        $this->httpTransferObjectMock->expects($this->once())
            ->method('create')
            ->willReturn($this->transfer);
        $this->curlClientMock->expects($this->once())
            ->method('placeRequest')
            ->with($this->transfer)
            ->willReturn($responseBody);

        $result = $this->object->execute($paymentId);
        $this->assertEquals($responseBody, $result);
    }
}
