/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

define([
    'jquery',
    'underscore',
    'mage/translate',
    'text!../../css/styles.min.css',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/model/customer',
    '../action/copy-and-pay-prepare',
    'Magento_Ui/js/model/messageList',
    'mage/url'
], function (
    $,
    _,
    $t,
    oneClickCheckoutStyles,
    quote,
    customer,
    copyAndPayCheckoutAction,
    messageContainer,
    urlBuilder
) {
    'use strict';

    var oneClickCheckout = {
        scriptSrcTest: 'https://sandbox-card.peachpayments.com/v1/paymentWidgets.js?checkoutId=',
        scriptSrcLive: 'https://card.peachpayments.com/v1/paymentWidgets.js?checkoutId=',
        redirect_url: '/pp-hosted/secure/CopyAndPayCheck'
    }

    return {
        configs: oneClickCheckout,

        /**
         * @param payment
         */
        initPayment: function (payment) {
            let self = this;

            $.when(copyAndPayCheckoutAction(quote.getQuoteId(), messageContainer))
                .done(function(checkoutDetails) {
                    checkoutDetails = JSON.parse(checkoutDetails);
                    if (checkoutDetails && checkoutDetails['checkout_id']) {
                        self._initializeIframe(payment, checkoutDetails);
                    } else {
                        self._showErrorMessage();
                    }
                }
            );
        },

        _showErrorMessage: function () {
            let errorMessage = $("<p />", {
                class: 'message error',
                text: 'The One-click payment method is not available at the moment. Please try again later.',
                style: 'padding-left: 10px'
            });

            $('#copy-and-pay-form').html(errorMessage);
        },

        /**
         * @param payment
         * @param checkoutDetails
         */
        _initializeIframe: function (payment, checkoutDetails) {
            let pp_script = this._getScriptHtml(payment.code, checkoutDetails['checkout_id']);
            let pp_form = this._getCCFormHtml(payment.code);
            let pp_styles = this._getIframeStyles();
            let iframe = document.createElement('iframe');
            let html = '<body>' + pp_script + pp_form + pp_styles + '</body>';

            iframe.setAttribute('id', 'one-click-checkout-' + checkoutDetails['checkout_id']);
            iframe.setAttribute('class', 'one-click-checkout-iframe');
            iframe.setAttribute('style', 'width: 100%;border: none;margin: 0 auto;display: block;');
            $(payment.formId).html(iframe);
            iframe.frameborder = 0;
            iframe.contentWindow.document.open();
            iframe.contentWindow.wpwlOptions = this._getWpwlOptions(payment, checkoutDetails['additional-data']);
            iframe.contentWindow.document.write(html);
            iframe.contentWindow.document.close();
        },

        /**
         * @param paymentCode
         * @param oneClickCheckoutID
         * @returns {string}
         * @private
         */
        _getScriptHtml: function (paymentCode, oneClickCheckoutID) {
            let scriptSrc = window.checkoutConfig.payment[paymentCode].isLive
                ? this.configs.scriptSrcLive
                : this.configs.scriptSrcTest;

            return '<script async src="' + scriptSrc + oneClickCheckoutID + '"></script>';
        },

        /**
         * @param paymentCode
         * @returns {string}
         * @private
         */
        _getCCFormHtml: function (paymentCode) {
            let availableCCTypes = window.checkoutConfig.payment[paymentCode].availableTypes;
            let ccTypes = '';
            _.each(availableCCTypes, function(type) {
                ccTypes = ccTypes + Object.values(type).join() + ' ';
            });

            let redirectUrl = urlBuilder.build(this.configs.redirect_url);

            return  '<form action="' + redirectUrl +'" class="paymentWidgets" data-brands="' + ccTypes +'"></form>';
        },

        /**
         * @returns {string}
         * @private
         */
        _getIframeStyles: function () {
            return '<style>' + oneClickCheckoutStyles + '</style>';
        },

        /**
         * @param paymentData
         * @param checkoutAdditionalCustomerData
         * @returns {{registrations: {hideInitialPaymentForms: *}, maskCvv: boolean, style: string, billingAddress: {country: (string|*), city: (boolean|string|*), postcode: (boolean|string|number|*), street1: *, state: (*|null), street2: (*|string)}, mandatoryBillingFields: {country: boolean, city: boolean, postcode: boolean, street1: boolean, state: boolean, street2: boolean}, labels: {showOtherPaymentMethods: string, cardHolder: string}, onReady: onReady}}
         * @private
         */
        _getWpwlOptions: function(paymentData, checkoutAdditionalCustomerData) {
            let self = this;
            return {
                maskCvv: true,
                style: "card",
                disableCardExpiryDateValidation: window.checkoutConfig.payment[paymentData.code].isLive,
                labels: {
                    cardHolder: "Card Holder",
                    showOtherPaymentMethods: "+ Add new card"
                },
                registrations: {
                    hideInitialPaymentForms: paymentData.hideInitialPaymentForms
                },
                onError: function(error) {
                    self._toggleLoader(false);
                    // check if shopper paid after 30 minutes aka checkoutId is invalid
                    if (error.name === "InvalidCheckoutIdError") {
                        alert('Looks like "checkoutId" is invalid!');
                        window.location.reload();
                    } else if (error.name === "WidgetError") {
                        console.log("here we have extra properties: ");
                        console.log(error.brand + " and " + error.event);
                    }
                    // read the error message
                    console.log(error.message);
                },
                onReady: function() {
                    let iframeDoc = $(paymentData.formId + ' .one-click-checkout-iframe').contents();

                    if (paymentData.hideInitialPaymentForms) {
                        $('body > .wpwl-form-registrations button', $(iframeDoc))
                            .removeClass('wpwl-button-pay');
                    } else {
                        $(iframeDoc).find('#wpwl-registrations').remove();
                    }

                    if (self._showSaveCardOption()) {
                        let createRegistrationHtml ='<div class="customInput">' +
                            '<input type="checkbox" name="createRegistration" value="true" checked />' +
                            '<div class="customLabel">Save card for future use</div>' +
                            '</div>';

                        $(createRegistrationHtml).insertBefore($('.wpwl-form-card .wpwl-button', $(iframeDoc)));
                    }

                    $(iframeDoc).on("click.wpwlEvent", function (e) {
                        if ($(e.target).data('action') && $(e.target).data('action') == "show-initial-forms") {
                            let htmlHeight = $('html', $(iframeDoc)).innerHeight() + 50;
                            $(paymentData.formId + ' .one-click-checkout-iframe').height(htmlHeight);
                            $('.wpwl-container.wpwl-container-card', $(iframeDoc)).get(0).scrollIntoView();
                        }
                    });

                    // Set dynamic height of an iframe, setTimeout is for waiting all resources are loaded.
                    setTimeout(function() {
                        let htmlHeight = $('html', $(iframeDoc)).height() + 25;
                        $(paymentData.formId + ' .one-click-checkout-iframe').height(
                            ((htmlHeight === 25) || (htmlHeight >= 395))
                                ? 395
                                : htmlHeight
                        );
                    }, 500);

                    _.each(_.extend(checkoutAdditionalCustomerData, self._getAdditionalParameters()), function (value, name) {
                        let hiddenInput ='<input type="hidden" name="' + name + '" value="' + value + '" />';
                        $('.wpwl-form-card', $(iframeDoc)).append(hiddenInput);
                    });

                    $('[data-action="blur-card-expiry"]', $(iframeDoc)).on('blur', function () {
                        $(this).removeClass('wpwl-has-warning');
                        $('.wpwl-hint-expiryMonthWarning', $(iframeDoc)).remove();

                        let expDate = $(this).val();
                        if (expDate && !$(this).hasClass('wpwl-has-error')) {
                            let ccExpMM = $('input[name="card.expiryMonth"]', $(iframeDoc)).val(),
                                ccExpYY = $('input[name="card.expiryYear"]', $(iframeDoc)).val(),
                                currentDate = new Date(),
                                ccDate = new Date(ccExpYY,ccExpMM);

                            ccDate.setMonth(ccDate.getMonth() - 1);
                            ccDate.setMonth(ccDate.getMonth() + 1, 1);

                            if (ccDate < currentDate) {
                                $(this).addClass('wpwl-has-warning');
                                $("<div>", {
                                    'class': "wpwl-hint wpwl-hint-expiryMonthWarning",
                                    'html': 'Warning: Expiry date is in the past.'
                                }).insertAfter($(this));
                            }
                        }
                    });
                }
            }
        },

        /**
         * @returns {{amount: *, "customer.email": string, currency: (string|*), "customer.givenName": (string|*)}}
         * @private
         */
        _getAdditionalParameters: function () {
            return {
                'customer.givenName': quote.billingAddress().firstname,
                'customer.email'    : (window.customerData) ? window.customerData.email : quote.guestEmail
            }
        },
        _showSaveCardOption: function () {
            let quoteDoesNotHaveSubscriptionChecker = typeof quote.containsSubscription !== "function";
            let quoteDoesNotHaveSubscription = typeof quote.containsSubscription === "function" &&
                !Boolean(quote.containsSubscription());

            return customer.isLoggedIn() &&
                (quoteDoesNotHaveSubscriptionChecker || quoteDoesNotHaveSubscription)
        },
    };
});
