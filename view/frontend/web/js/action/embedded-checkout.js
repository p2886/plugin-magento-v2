/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2023 X2Y.io (https://x2y.io/)
 */

define([
    'Magento_Checkout/js/model/url-builder',
    'mage/storage',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/full-screen-loader'
], function (
    urlBuilder,
    storage,
    errorProcessor,
    customer,
    fullScreenLoader
) {
    'use strict';

    return function (cartId, messageContainer) {
        let serviceUrl,
            payload;

        payload = {
            cartId: cartId
        };

        /**
         * Embedded Checkout for guest and registered customer.
         */
        if (!customer.isLoggedIn()) {
            serviceUrl = urlBuilder.createUrl('/guest-carts/:cartId/embedded-checkout', {
                cartId: cartId
            });
        } else {
            serviceUrl = urlBuilder.createUrl('/carts/mine/embedded-checkout', {});
        }

        fullScreenLoader.startLoader();

        return storage.post(
            serviceUrl,
            JSON.stringify(payload)
        ).fail(
            function (response) {
                errorProcessor.process(response, messageContainer);
            }
        ).always(
            function () {
                fullScreenLoader.stopLoader();
            }
        );
    };
});
