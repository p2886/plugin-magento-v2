/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y.io (https://x2y.io/)
 */

define([
    'jquery',
    'underscore',
    'Magento_Customer/js/customer-data',
    'mage/cookies'
], function ($, _, customerData) {
    'use strict';

    $.widget('peach.payWithButton', {
        options: {
            isListingPage: false,
            cookieName: 'pay-with-peach-payment',
            tracking: '',
            cookieLifetime: 86400 // 24h
        },

        /**
         * @private
         */
        _create: function () {
            (this.options.isListingPage)
                ? this._initListingButtons()
                : this._on({'click': '_proceedToCheckout'});

            this.options.tracking = this.options.isListingPage ? 'plp' : 'pdp';
        },

        _payWithPeachAction: function () {
            let self = this,
                allowPeachCheckoutRedirect = false;

            $(document).on('ajax:addToCart', function (event, data) {
                if (!_.isEmpty(data.response)) {
                    return;
                }

                let cookieExpires = new Date(new Date().getTime() + self.options.cookieLifetime * 1000);
                let cookieValue = JSON.stringify(
                    {
                        'status': true,
                        'tracking': self.options.tracking
                    }
                )
                $.mage.cookies.set(self.options.cookieName, cookieValue, {
                    expires: cookieExpires
                });

                allowPeachCheckoutRedirect = true;
            });

            let cartData = customerData.get('cart');
            cartData.subscribe(function () {
                if (allowPeachCheckoutRedirect) {
                    window.location.href = window.checkout.checkoutUrl;
                }
            });
        },

        _proceedToCheckout: function () {
            $('#product-addtocart-button').trigger('click');
            if (!$('#product_addtocart_form').valid()) {
                return;
            }

            this._payWithPeachAction();
            $(this.element).attr('disabled', true);
        },

        _initListingButtons: function () {
            let self = this,
                products = $('.product.product-item-actions'),
                payWithButton = $('#pay-with-peach-button-template').html();

            $.each(products, function (i, product) {
                $(product).prepend(payWithButton);
            });

            $('.product-item-actions .payWithPeachButton').on('click', function (event) {
                event.preventDefault();
                $(event.currentTarget).attr('disabled', true);
                $(".tocart.primary", $(event.currentTarget.parentElement)).trigger('click');

                self._payWithPeachAction();
            });
        },
    });

    return $.peach.payWithButton;
});
