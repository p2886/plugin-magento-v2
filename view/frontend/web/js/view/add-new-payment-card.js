/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

define([
    'jquery',
    'mage/storage',
    'mage/translate',
    'text!../../css/styles.min.css'
], function ($, storage, $t, widgetStyles) {
    'use strict';

    $.widget('peach.addNewPaymentCard', {
        options: {
            paymentFormUrl : '',
            paymentBrands  : 'VISA MASTER',
            addNewCardForm : '[data-template="peach-payments-new-card-form"]',
            redirectUrl    : ''
        },

        cache: {},

        /**
         * @private
         */
        _create: function () {
            let newCardFormPopupDOM = $(this.options.addNewCardForm)[0];
            this.cache.$addNewCardPopup = $(newCardFormPopupDOM.innerHTML.trim());

            $(this.cache.$addNewCardPopup).modal({
                title: $t('Add New Payment Card'),
                modalClass: '_js-add-new-card-modal',
                responsive: true,
                innerScroll: true,
                buttons: []
            }).trigger('contentUpdated').applyBindings();

            this._on(document, {'click [data-role="add-new-card"]': '_showModalWidget'});
        },

        /**
         * @private
         */
        _showModalWidget: function() {
            this._loadPaymentWidget();
            this.cache.$addNewCardPopup.modal('openModal');
            this._toggleLoader(true);
        },

        /**
         * @private
         */
        _loadPaymentWidget: function () {
            let self = this,
                prepareUrl = 'rest/V1/pp-hosted/cc/add-new';

            storage.get(
                window.BASE_URL + prepareUrl,
                true,
                'application/json',
                {}
            ).done(function (data) {
                data = JSON.parse(data);
                (data && data['checkout_id'])
                    ? self._initializeIframe(data)
                    : self._showErrorMessage();
            });
        },

        /**
         * @param checkoutDetails
         * @private
         */
        _initializeIframe: function (checkoutDetails) {
            let pp_styles = this._getIframeStyles();
            let pp_script = this._getScriptHtml(checkoutDetails['checkout_id']);
            let pp_form = this._getCCFormHtml();
            let iframe = document.createElement('iframe');
            let html = '<body>' + pp_styles + pp_script + pp_form + '</body>';

            iframe.setAttribute('id', 'add-new-one-click-checkout-' + checkoutDetails['checkout_id']);
            iframe.setAttribute('class', 'one-click-checkout-iframe');
            iframe.setAttribute('style', 'width: 100%;border: none;margin: 0 auto;display: block;height: 350px');
            $("#new-cart-form").html(iframe);
            iframe.frameborder = 0;
            iframe.contentWindow.document.open();
            iframe.contentWindow.wpwlOptions = this._getWpwlOptions();
            iframe.contentWindow.document.write(html);
            iframe.contentWindow.document.close();
        },

        /**
         * @returns {string}
         * @private
         */
        _getIframeStyles: function () {
            return '<style>' + widgetStyles + '</style>';
        },

        /**
         * @param oneClickCheckoutID
         * @returns {string}
         * @private
         */
        _getScriptHtml: function (oneClickCheckoutID) {
            return '<script async src="' + this.options.paymentFormUrl + oneClickCheckoutID + '"></script>';
        },

        /**
         * @returns {string}
         * @private
         */
        _getCCFormHtml: function () {
            let options = this.options;
            return '<form action="' + options.redirectUrl + '" class="paymentWidgets"' +
                ' data-brands="' + options.paymentBrands + '"></form>';
        },

        /**
         * @returns {{maskCvv: boolean, style: string, labels: {cardHolder: string}, onReady: onReady}}
         * @private
         */
        _getWpwlOptions: function() {
            let self = this;

            return {
                maskCvv: true,
                disableCardExpiryDateValidation: true,
                style: "card",
                labels: {
                    submit: $t("Add New Card"),
                    cardHolder: $t("Card Holder")
                },
                onReady: function() {
                    self._toggleLoader(false);
                    let iframeDoc = $('.one-click-checkout-iframe').contents();
                    $('[data-action="blur-card-expiry"]', $(iframeDoc)).on('blur', function () {
                        $(this).removeClass('wpwl-has-warning');
                        $('.wpwl-hint-expiryMonthWarning', $(iframeDoc)).remove();

                        let expDate = $(this).val();
                        if (expDate && !$(this).hasClass('wpwl-has-error')) {
                            let ccExpMM = $('input[name="card.expiryMonth"]', $(iframeDoc)).val(),
                                ccExpYY = $('input[name="card.expiryYear"]', $(iframeDoc)).val(),
                                currentDate = new Date(),
                                ccDate = new Date(ccExpYY,ccExpMM);

                            ccDate.setMonth(ccDate.getMonth() - 1);
                            ccDate.setMonth(ccDate.getMonth() + 1, 1);

                            if (ccDate < currentDate) {
                                $(this).addClass('wpwl-has-warning');
                                $("<div>", {
                                    'class': "wpwl-hint wpwl-hint-expiryMonthWarning",
                                    'html': 'Warning: Expiry date is in the past.'
                                }).insertAfter($(this));
                            }
                        }
                    });
                },
                onError: function(error) {
                    self._toggleLoader(false);
                    // check if shopper paid after 30 minutes aka checkoutId is invalid
                    if (error.name === "InvalidCheckoutIdError") {
                        alert('Looks like "checkoutId" is invalid!');
                        window.location.reload();
                    } else if (error.name === "WidgetError") {
                        console.log("here we have extra properties: ");
                        console.log(error.brand + " and " + error.event);
                    }
                    // read the error message
                    console.log(error.message);
                }
            }
        },

        /**
         * @private
         */
        _showErrorMessage: function () {
            let errorMessage = $("<p />", {
                class: 'message error',
                text: $t('The One-click payment method is not available at the moment. Please try again later.'),
                style: 'padding-left: 10px'
            });

            $("#new-cart-form").html(errorMessage);
            this._toggleLoader(false);
        },

        /**
         * @param display
         * @private
         */
        _toggleLoader: function (display) {
            $('#payment-form-loader').toggle(display);
        }
    });

    return $.peach.addNewPaymentCard;
});
