/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y.io (https://x2y.io/)
 */

define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'underscore',
        'ko',
        'mage/translate',
        'PeachPayments_Hosted/js/action/embedded-checkout',
        'Magento_Checkout/js/model/quote',
        'Magento_Ui/js/model/messageList',
        'Magento_SalesRule/js/action/set-coupon-code',
        'Magento_SalesRule/js/action/cancel-coupon',
        'ppEmbeddedCheckoutSandbox',
        'ppEmbeddedCheckoutLive',
        'mage/cookies'
    ],
    function (
        Component,
        $,
        _,
        ko,
        $t,
        embeddedCheckoutAction,
        quote,
        messageContainer,
        setCouponCodeAction,
        cancelCouponAction,
        embeddedSandboxJs,
        embeddedLiveJs
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'PeachPayments_Hosted/payment/embedded-checkout',
                embeddedCheckoutFormPlaceholder: '#embedded-checkout-form',
            },

            renderedCheckout: '',
            isEmbeddedCheckoutLoaded: false,
            isEmbeddedCheckoutExpired: ko.observable(false), // need to init in
            hasError: ko.observable(false),

            initialize: function () {
                this._super();
                let self = this;
                (this.isChecked() === this.getCode() && !this.isEmbeddedCheckoutLoaded && this.loadEmbeddedCheckout());

                setCouponCodeAction.registerSuccessCallback(function () {
                    if (self.isEmbeddedCheckoutLoaded) {
                        self.loadEmbeddedCheckout();
                    }
                });

                cancelCouponAction.registerSuccessCallback(function () {
                    if (self.isEmbeddedCheckoutLoaded) {
                        self.loadEmbeddedCheckout();
                    }
                });
            },

            selectPaymentMethod: function () {
                (!this.isEmbeddedCheckoutLoaded && this.loadEmbeddedCheckout());
                return this._super();
            },

            loadEmbeddedCheckout: function () {
                let self = this;
                $.when(embeddedCheckoutAction(quote.getQuoteId(), messageContainer))
                    .fail(() => {
                        messageContainer.addErrorMessage({
                            message: $t('Something went wrong while prepare to Embedded Checkout.')
                        });
                        self.hasError(true);
                    }).done((checkoutDetails) => {
                        self.isEmbeddedCheckoutExpired(false);
                        checkoutDetails = JSON.parse(checkoutDetails);
                        if (checkoutDetails && checkoutDetails['checkout_id'] && checkoutDetails['entityId']) {
                            self.instantiateEmbeddedCheckout(checkoutDetails)
                        } else {
                            self.hasError(true);
                        }
                    }
                );
            },

            /**
             * @param checkoutDetails
             */
            instantiateEmbeddedCheckout: function (checkoutDetails) {
                let self = this,
                    embeddedJs;

                if (self.renderedCheckout) {
                    self.renderedCheckout.unmount();
                }

                embeddedJs = window.checkoutConfig.payment['hosted'].isLive ? embeddedLiveJs : embeddedSandboxJs;

                self.renderedCheckout = embeddedJs.Checkout.initiate({
                    key: checkoutDetails['entityId'],
                    checkoutId: checkoutDetails['checkout_id'],
                    events: {
                        onCompleted: (event) => {
                            if (typeof undefined === typeof window.checkoutConfig.payment[self.getCode()]) {
                                window.checkoutConfig.payment[self.getCode()] = {};
                            }

                            window.checkoutConfig.payment[self.getCode()].paymentResult = {
                                'transaction_id': event.id,
                                'checkout_id': event.checkoutId
                            };

                            self.placeOrder();
                        },
                        onCancelled: (event) => console.log(event),
                        onExpired: (event) => {
                            self.isEmbeddedCheckoutExpired(true);
                            console.log(event);
                        },
                    },
                });

                self.renderedCheckout.render(self.embeddedCheckoutFormPlaceholder);
                self.isEmbeddedCheckoutLoaded = true;
            },

            getData: function () {
                let data = this._super();
                const peachEmbeddedCheckoutData = window.checkoutConfig.payment[this.getCode()];
                data['additional_data'] = _.extend(
                    data['additional_data'] || {},
                    peachEmbeddedCheckoutData?.paymentResult || {}
                );

                return data;
            }
        });
    }
);
