/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */
/*browser:true*/
/*global define*/
define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'PeachPayments_Hosted/js/model/one-click-checkout',
        'Magento_Customer/js/model/customer',
        'underscore',
        'Magento_SalesRule/js/action/set-coupon-code',
        'Magento_SalesRule/js/action/cancel-coupon'
    ],
    function (
        Component,
        $,
        quote,
        oneClickCheckout,
        customer,
        _,
        setCouponCodeAction,
        cancelCouponAction
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                redirectAfterPlaceOrder: true,
                template: 'PeachPayments_Hosted/payment/copy-and-pay',
                form_placeholder: '#copy-and-pay-form',
            },

            initialize: function () {
                this._super();
                let self = this;

                setCouponCodeAction.registerSuccessCallback(function () {
                    self.loadOneCLickCheckoutPaymentWidget();
                });

                cancelCouponAction.registerSuccessCallback(function () {
                    self.loadOneCLickCheckoutPaymentWidget();
                });
            },

            loadOneCLickCheckoutPaymentWidget: function () {
                let paymentData = {
                    code: this.getCode(),
                    formId: this.form_placeholder,
                };

                paymentData.hideInitialPaymentForms = !!(customer.isLoggedIn() && this.savedCards()
                    && !quote.containsSubscription());

                oneClickCheckout.initPayment(paymentData);
            },

            getCode: function () {
                return 'peachpayments_server_to_server';
            },

            /**
             * Get payment method title.
             */
            getTitle: function () {
                let savedCards = this.savedCards();
                if (savedCards && !quote.containsSubscription()) {
                    return 'Pay with saved card (' + this.savedCards() + ')';
                } else {
                    return this.item.title;
                }
            },

            savedCards: function () {
                let savedCards = _.filter(window.checkoutConfig.payment.vault, function (config, name) {
                    return name.includes('peachpayments_server_to_server_vault');
                });

                return savedCards.length;
            },

            getLogo: function () {
                return require.toUrl('PeachPayments_Hosted/images/payment/visa-mastercard.svg');
            },
        });
    }
);
