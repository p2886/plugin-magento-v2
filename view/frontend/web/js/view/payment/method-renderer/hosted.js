/*
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

define(
    [
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/url-builder',
        'mage/storage',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/url',
    ],
    function (
        $,
        quote,
        urlBuilder,
        storage,
        customerData,
        Component,
        placeOrderAction,
        selectPaymentMethodAction,
        customer,
        checkoutData,
        additionalValidators,
        url
    ) {
        'use strict';

        const visiblePaymentsTitles = 4;
        const allPaymentsCode = 'peachpayments_hosted_allpayments';
        const peachPaymentsLogos = {
            'peachpayments_hosted_masterpass'  : 'PeachPayments_Hosted/images/payment/scan-to-pay.svg',
            'peachpayments_hosted_mobicred'    : 'PeachPayments_Hosted/images/payment/mobicred.svg',
            'peachpayments_hosted_zeropay'     : 'PeachPayments_Hosted/images/payment/zeropay.svg',
            'peachpayments_hosted_payflex'     : 'PeachPayments_Hosted/images/payment/payflex.svg',
            'peachpayments_hosted_stitcheft'   : 'PeachPayments_Hosted/images/payment/instanteft.svg',
            'peachpayments_hosted_eftsecure'   : 'PeachPayments_Hosted/images/payment/eftsecure.svg',
            'peachpayments_hosted_mpesa'       : 'PeachPayments_Hosted/images/payment/mpesa.svg',
            'peachpayments_hosted_card'        : 'PeachPayments_Hosted/images/payment/visa-mastercard.svg',
            'peachpayments_hosted_aplus'       : 'PeachPayments_Hosted/images/payment/aplus.svg',
            'peachpayments_hosted_1foryou'     : 'PeachPayments_Hosted/images/payment/1foryou.svg',
            'peachpayments_hosted_blinkbyemtel': 'PeachPayments_Hosted/images/payment/blinkbyemtel.svg',
            'peachpayments_hosted_finchoicepay': 'PeachPayments_Hosted/images/payment/finchoicepay.svg',
            'peachpayments_hosted_capitecpay'  : 'PeachPayments_Hosted/images/payment/capitecpay.svg',
            'peachpayments_hosted_allpayments' : 'PeachPayments_Hosted/images/payment/all-payments.svg',
            'peachpayments_hosted_applepay'    : 'PeachPayments_Hosted/images/payment/apple-pay.svg',
        };

        return Component.extend({
            redirectAfterPlaceOrder: false,
            defaults: {
                template: 'PeachPayments_Hosted/payment/hosted'
            },
            peachPlaceOrder: function (data, event) {

                if (event) {
                    event.preventDefault();
                }

                var self = this,
                    placeOrder,
                    emailValidationResult = customer.isLoggedIn(),
                    loginFormSelector = 'form[data-role=email-with-possible-login]';

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                if (emailValidationResult && this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);
                    placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);

                    $.when(placeOrder).fail(function () {
                        self.isPlaceOrderActionAllowed(true);
                    }).done(this.afterPlaceOrder.bind(this));
                    return true;
                }
                return false;
            },

            selectPaymentMethod: function () {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod(this.item.method);
                return true;
            },

            afterPlaceOrder: function () {
                let self = this;
                storage.get(
                    urlBuilder.createUrl('/pp-hosted/order-details', {}),
                    false
                ).done(function (orderDataJson) {
                    let orderData = JSON.parse(orderDataJson);
                    if (orderData && orderData.signature) {
                        self._processFormPostCheckout(orderData);
                    } else {
                        alert(orderData.error);
                    }
                });
            },

            _processFormPostCheckout: function (data) {
                $('<form>', {
                    'action': window.checkoutConfig.payment['hosted'].checkoutUrl,
                    'method': 'POST',
                    'id'    : 'peachSecureRedirect',
                    'style' : 'display: none;'
                }).appendTo(document.body);

                _.each(data, function (value, name) {
                    $('<input>').attr({
                        type: 'hidden',
                        name: name,
                        value: value
                    }).appendTo('#peachSecureRedirect');
                });

                $('#peachSecureRedirect').submit();
            },

            /**
             * Get payment method Logo.
             */
            getLogo: function () {
                let logo = false,
                    paymentCode = this.item.method;

                if (peachPaymentsLogos.hasOwnProperty(paymentCode)) {
                    logo = require.toUrl(peachPaymentsLogos[paymentCode]);
                }

                return logo;
            },

            isConsolidatedPayments: function () {
                return (this.item.method === allPaymentsCode &&
                    window.checkoutConfig.payment['hosted'].consolidatedPayments);
            },

            getAvailablePaymentsTitles: function () {
                let prepareUrl = 'rest/V1/pp-hosted/all-payments-title';

                storage.get(
                    window.BASE_URL + prepareUrl,
                    true,
                    'application/json',
                    {}
                ).done(function (data) {
                    data = JSON.parse(data);
                    let paymentMethodTitles = '',
                        paymentMethodsLength = 0,
                        paymentMethods = (data && data.paymentMethods);

                    if (paymentMethods) {
                        if (!(/iPad|iPhone|iPod/.test(navigator.userAgent)
                            || window.navigator.userAgent.indexOf("Mac OS") !== -1)) {
                            paymentMethods = paymentMethods.filter(payment => payment.name !== 'Apple Pay');
                        }

                        if (paymentMethods.length > visiblePaymentsTitles) {
                            paymentMethodsLength = paymentMethods.length - visiblePaymentsTitles;
                            paymentMethods = paymentMethods.slice(0, visiblePaymentsTitles);
                        }

                        paymentMethodTitles = Object.keys(paymentMethods).map(key => paymentMethods[key].name).join(', ');
                    }

                    if (paymentMethodsLength) {
                        paymentMethodTitles = paymentMethodTitles + ', +' + paymentMethodsLength;
                    }

                    $('#available-payments').html(paymentMethodTitles);
                });
            },
        });
    }
);
