define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'peachpayments_server_to_server',
                component: 'PeachPayments_Hosted/js/view/payment/method-renderer/server-to-server'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
