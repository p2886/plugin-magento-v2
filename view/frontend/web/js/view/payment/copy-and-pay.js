/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list',
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'peachpayments_server_to_server',
                component: 'PeachPayments_Hosted/js/view/payment/method-renderer/copy-and-pay'
            }
        );

        /** Add view logic here if needed */
        return Component.extend({});
    }
);
