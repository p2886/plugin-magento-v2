/*
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        let availableHostedMethods = [
            "card",
            "allpayments",
            "eftsecure",
            "masterpass",
            "mobicred",
            "mpesa",
            "aplus",
            "stitcheft",
            "payflex",
            "zeropay",
            "1foryou",
            "blinkbyemtel",
            "finchoicepay",
            "capitecpay"
        ];

        let storeBaseCurrency = window.checkoutConfig.totalsData.base_currency_code,
            isIOS = (/iPad|iPhone|iPod/.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Mac OS") !== -1);

        (isIOS && availableHostedMethods.push("applepay"));
        ((storeBaseCurrency === 'USD') && availableHostedMethods.push("paypal"));

        for (let k = 0; k < availableHostedMethods.length; k++) {
            rendererList.push(
                {
                    type: 'peachpayments_hosted_' + availableHostedMethods[k],
                    component: 'PeachPayments_Hosted/js/view/payment/method-renderer/hosted'
                }
            );
        }

        return Component.extend({});
    }
);
