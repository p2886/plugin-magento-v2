/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2023 X2Y.io (https://x2y.io/)
 */

define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'peachpayments_embedded_checkout',
                component: 'PeachPayments_Hosted/js/view/payment/method-renderer/embedded-checkout'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
