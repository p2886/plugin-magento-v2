/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

define([
    'mage/utils/wrapper',
    'underscore'
], function (wrapper, _) {

    /**
     * Remove duplicate payment methods as this cause switching order on payments list.
     */
    return function (methodConverter) {
        return wrapper.wrap(methodConverter, function (originalConverter, methods) {
            let paymentsMethod = originalConverter(methods);
            if (paymentsMethod && paymentsMethod.length) {
                paymentsMethod = _.uniq(methods, function (payment) {
                    return payment.method;
                });
            }

            return paymentsMethod;
        });
    };
});
