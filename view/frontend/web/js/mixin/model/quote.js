/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

define([
    'jquery',
    'ko',
    'underscore',
    'mage/translate',
    'Magento_Ui/js/model/messageList',
], function ($, ko, _) {
    'use strict';

    return function (quote) {

        /**
         * Check if quote has product subscription.
         *
         * @returns {number}
         */
        quote.containsSubscription = function () {
            let subscriptionItems = _.filter(this.getItems(), function (item) {
                return (item.product.subscription_active
                    && item.product.subscription_active === "1");
            });

            return (subscriptionItems && subscriptionItems.length);
        };

        return quote;
    };
});
