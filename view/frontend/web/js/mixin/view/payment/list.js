/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

define([
    'underscore',
    'Magento_Checkout/js/model/payment/renderer-list',
], function (_, rendererList) {
    'use strict';

    return function (paymentList) {
        return paymentList.extend({
            /**
             * Remove separate group form peachpayments vault methods
             * to render in default payments group.
             *
             * @param paymentMethodData
             */
            createRenderer: function (paymentMethodData) {
                _.each(rendererList(), function (renderer, index) {
                    let type = renderer.type;
                    if (type.substring(0, type.lastIndexOf('_')) === "peachpayments_server_to_server_vault") {
                        rendererList()[index].group = null;
                    }
                });

                this._super(paymentMethodData);
            }
        });
    };
});
