/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y.io (https://x2y.io/)
 */

define([
    'jquery',
    'mage/translate',
    'mage/cookies'
], function ($, $t) {
    'use strict';

    return function (shipping) {
        return shipping.extend({
            /**
             * @return {exports}
             */
            initialize: function () {
                this._super();

                let self = this;
                if ($.mage.cookies.get('pay-with-peach-payment')) {
                    $('a').on('click', (event) => {
                        (self._showConfirmation())
                            ? $.mage.cookies.clear('pay-with-peach-payment')
                            : event.preventDefault();
                    });

                    window.addEventListener("popstate", () => {
                        if (document.location.hash !== '#shipping' && document.location.hash !== '#payment') {
                            let isConfirmed = self._showConfirmation();
                            if (isConfirmed) {
                                $.mage.cookies.clear('pay-with-peach-payment');
                                history.back();
                            } else {
                                history.pushState(null, document.title, location.href);
                            }
                        }
                    });
                }

                return this;
            },

            _showConfirmation: function () {
                return confirm($t('Are you sure you want to abandon Peach Payment checkout process?'));
            }
        });
    };
});
