/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */
define([
    'jquery',
    'mage/translate',
    'mage/validation',
    'Magento_Ui/js/modal/modal'
], function($, $t) {
    "use strict";

    $.widget('peach.helpMeButtonWidget', {
        options: {
            ajaxUrl: '',
            peachModalForm: $('#peach_payment_modal_form'),
            peachModal: $('#peach_payment_modal'),
            modalMessage: '.help-me-message',
        },

        _create: function () {
            let self = this;
            $("#peach-payment-help-me-button").on('click', function (e) {
                e.preventDefault();
                let modalOptions = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: $t('Send Diagnostics to Peach Payments'),
                    buttons: [{
                        text: $t('Submit'),
                        class: '',
                        click: function () {
                            if (self.options.peachModalForm.validation() && self.options.peachModalForm.validation('isValid')) {
                                let formData = self.options.peachModalForm.serialize(),
                                    postUrl = self.options.ajaxUrl;
                                $.ajax({
                                    url: postUrl,
                                    type: 'POST',
                                    data: formData,
                                    showLoader: true,
                                    success: function (response) {
                                        if (response.success === true) {
                                            self._setResponseMessage('Your request has been processed successfully.', 'success');
                                            self.options.peachModalForm.trigger("reset")
                                        } else {
                                            self._setResponseMessage(response.message, 'error');
                                        }
                                    },
                                    error: function (response) {
                                        self._setResponseMessage(response.message, 'error');
                                    }
                                });
                            }
                        }
                    }]
                };

                self.options.peachModal.modal(modalOptions).on('modalclosed', function() {
                    $(self.options.modalMessage).removeClass('success error');
                });
                self.options.peachModal.modal('openModal');
            });
        },

        /**
         * @param message
         * @param status
         * @private
         */
        _setResponseMessage: function (message, status) {
            $(this.options.modalMessage).addClass(status);
            $(this.options.modalMessage + ' p').text($t(message));
        }
    });

    return $.peach.helpMeButtonWidget;
});
