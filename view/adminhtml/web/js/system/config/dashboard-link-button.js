/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

define([
    'jquery',
    'mage/translate',
], function ($, $t) {
    'use strict';

    $.widget('peach.dashboardLinkButton', {
        options: {
            liveLabel : 'Get Production credentials from Peach Payments',
            sandboxLabel  : 'Get Sandbox credentials from Peach Payments',
            liveURL : 'https://dashboard.peachpayments.com/magento-service/settings',
            sandboxURL : 'https://sandbox-dashboard.peachpayments.com/magento-service/settings'
        },

        /**
         * @private
         */
        _create: function () {
            let self = this,
                dashbordLink = $('#peach-payment-dashboard-link-button'),
                mode = $("select[id$='peachpayments_api_configuration_mode']"), // get mode by select ID suffix
                selectedMode = mode.find(":selected").val();

            this.updateButton(selectedMode, dashbordLink);

            mode.on('change', function() {
                self.updateButton(
                    $(this).find(":selected").val(),
                    dashbordLink
                )
            });
        },

        /**
         * 1 - live mode
         * 0 - sandbox mode
         *
         * @param selectedMode
         * @param dashbordLink
         */
        updateButton: function (selectedMode, dashbordLink) {
            if (selectedMode == 1) {
                this.makeLiveButton(dashbordLink);
            }
            else {
                this.makeSandboxButton(dashbordLink);
            }
        },

        /**
         * @param button
         */
        makeLiveButton: function (button) {
            let self = this;
            button.text(this.options.liveLabel)
            button.unbind('click')
            button.click(function(){
                window.open(self.options.liveURL, '_blank');
            })
        },

        /**
         * @param button
         */
        makeSandboxButton: function (button) {
            let self = this;
            button.text(this.options.sandboxLabel)
            button.unbind('click')
            button.click(function(){
                window.open(self.options.sandboxURL, '_blank');
            })
        },
    });

    return $.peach.dashboardLinkButton;
});
