/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */
var config = {
    map: {
        '*': {
            peachPaymentHelpMeButton: 'PeachPayments_Hosted/js/view/help-me-button',
            dashboardLinkButton: 'PeachPayments_Hosted/js/system/config/dashboard-link-button'
        }
    }
};
