<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Setup\Patch\Data;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHostedHelper;

class EncryptCredentialsPatch implements DataPatchInterface
{
    /**
     * @var array
     */
    private $xPaths = [
        'merchant_id',
        'client_id',
        'client_id_sandbox',
        'client_secret',
        'client_secret_sandbox',
        'entity_id',
        'entity_id_theed_secure',
        'sign_key',
        'entity_id_sandbox',
        'entity_id_theed_secure_sandbox',
        'sign_key_sandbox',
        'token',
        'webhook_decryption_key'
    ];

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param EncryptorInterface $encryptor
     * @param WriterInterface $configWriter
     * @param StoreManagerInterface $storeManager
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        EncryptorInterface $encryptor,
        WriterInterface $configWriter,
        StoreManagerInterface $storeManager,
        ResourceConnection $resourceConnection
    ) {
        $this->encryptor = $encryptor;
        $this->configWriter = $configWriter;
        $this->storeManager = $storeManager;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return void
     */
    public function apply()
    {
        foreach ($this->xPaths as $xPath) {
            $path = PeachPaymentsHostedHelper::XML_CONF . $xPath;
            /** Encrypt values for Scope Default */
            $this->encryptValue($path);

            /** Encrypt values for Scope Websites */
            foreach ($this->storeManager->getWebsites(true) as $website) {
                $this->encryptValue($path, ScopeInterface::SCOPE_WEBSITES, (int)$website->getId());
            }
        }
    }

    /**
     * @param string $path
     * @param string $scope
     * @param int $scopeId
     * @return void
     */
    private function encryptValue(
        string $path,
        string $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        int $scopeId = 0
    ) {
        $value = $this->getConfigValue($path, $scope, $scopeId);

        if ($value) {
            $encryptedValue = $this->encryptor->encrypt($value);
            $this->configWriter->save($path, $encryptedValue, $scope, $scopeId);
        }
    }

    /**
     * @param string $path
     * @param string $scope
     * @param int $scopeId
     * @return string
     */
    private function getConfigValue(string $path, string $scope, int $scopeId): string
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()->from(
            $connection->getTableName('core_config_data'),
            'value'
        )->where(
            'path = ?',
            $path
        )->where(
            'scope = ?',
            $scope
        )->where(
            'scope_id = ?',
            $scopeId
        );

        return (string)$connection->fetchOne($select);
    }
}
