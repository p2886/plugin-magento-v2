<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Logger;

use Magento\Framework\Serialize\SerializerInterface;
use Magento\Payment\Model\Method\Logger;
use PeachPayments\Hosted\Model\PaymentLogFactory as PaymentLogFactory;
use PeachPayments\Hosted\Model\PaymentLog as PaymentLog;
use PeachPayments\Hosted\Model\ResourceModel\PaymentLog as PaymentLogResourceModel;
use Psr\Log\LoggerInterface;
use Stringable;

class UnifiedLogger
{
    /**
     * @param PaymentLogFactory $paymentLogFactory
     * @param PaymentLogResourceModel $paymentLogResource
     * @param SerializerInterface $serializer
     * @param Logger $paymentLogger
     * @param LoggerInterface $logger
     */
    public function __construct(
        private PaymentLogFactory $paymentLogFactory,
        private PaymentLogResourceModel $paymentLogResource,
        private SerializerInterface $serializer,
        private Logger $paymentLogger,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @param array $message
     * @param array|null $maskKeys
     * @param $forceDebug
     * @return void
     */
    public function debug(array $message, array $maskKeys = null, $forceDebug = null): void
    {
        $this->paymentLogger->debug($message, $maskKeys, $forceDebug);
        $message = $this->serializer->serialize($message);
        $this->databaseLog($message);
    }

    /**
     * @param string|Stringable $message
     * @return void
     */
    public function critical(string|Stringable $message): void
    {
        $this->log($message, 'critical');
    }

    /**
     * @param string|Stringable $message
     * @return void
     */
    public function error(string|Stringable $message): void
    {
        $this->log($message, 'error');
    }

    /**
     * @param string|Stringable $message
     * @param string $method
     * @return void
     */
    private function log(string|Stringable $message, string $method): void
    {
        $this->logger->{$method}($message);
        $this->databaseLog((string) $message);
    }

    /**
     * @param string $message
     * @return void
     */
    private function databaseLog(string $message): void
    {
        /** @var PaymentLog $paymentLog */
        $paymentLog = $this->paymentLogFactory->create();
        $paymentLog->setMessage((string) $message);
        $this->paymentLogResource->save($paymentLog);
    }
}
