<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\DataObjectFactory;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;

class AssignData extends AbstractDataAssignObserver
{
    /**
     * @var DataObjectFactory
     */
    private $dataObjectFactory;

    /**
     * @param DataObjectFactory $dataObjectFactory
     */
    public function __construct(
        DataObjectFactory $dataObjectFactory
    ) {
        $this->dataObjectFactory = $dataObjectFactory;
    }


    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $method = $this->readMethodArgument($observer);

        if (false !== strpos($method->getCode(), 'peachpayments_server_to_server')) {
            $this->assignDataToServerToServer($observer);
        }

        if (false !== strpos($method->getCode(), 'peachpayments_embedded_checkout')) {
            $this->assignDataToEmbeddedCheckout($observer);
        }
    }

    private function assignDataToServerToServer(Observer $observer): void
    {
        $data = $this->readDataArgument($observer);
        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
            return;
        }

        $additionalData = $this->dataObjectFactory->create(['data' => $additionalData]);
        $expMonth = $additionalData->getData('cc_exp_month');
        $expMonth = sprintf("%02d", $expMonth);

        $payment = $this->readPaymentModelArgument($observer);
        $payment->setAdditionalInformation('cc_cid', $additionalData->getData('cc_cid'));
        $payment->setAdditionalInformation('cc_type', $additionalData->getData('cc_type'));
        $payment->setAdditionalInformation('cc_exp_year', $additionalData->getData('cc_exp_year'));
        $payment->setAdditionalInformation('cc_exp_month', $expMonth);
        $payment->setAdditionalInformation('cc_number', $additionalData->getData('cc_number'));
    }

    private function assignDataToEmbeddedCheckout(Observer $observer)
    {
        $data = $this->readDataArgument($observer);
        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
            return;
        }

        $additionalData = $this->dataObjectFactory->create(['data' => $additionalData]);
        $payment = $this->readPaymentModelArgument($observer);
        $payment->setAdditionalInformation('checkout_id', $additionalData->getData('checkout_id'));
        $payment->setAdditionalInformation(AuthorizationTrxIdHandler::KEY_TNX_ID, $additionalData->getData('transaction_id'));
    }
}
