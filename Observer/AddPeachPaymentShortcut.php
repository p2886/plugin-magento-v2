<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Observer;

use Magento\Catalog\Block\ShortcutButtons;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use PeachPayments\Hosted\Block\Product\Shortcut\Button;
use PeachPayments\Hosted\Helper\Config;

class AddPeachPaymentShortcut implements ObserverInterface
{
    /**
     * Block class
     */
    public const PEACH_PAYMENT_SHORTCUT_BLOCK = Button::class;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws LocalizedException
     */
    public function execute(Observer $observer): void
    {
        if ($this->config->isEmbeddedCheckoutEnabled() && $this->config->isBuyNowButtonEnabled()
            && $observer->getData('is_catalog_product')) {

            /** @var ShortcutButtons $shortcutButtons */
            $shortcutButtons = $observer->getEvent()->getContainer();
            $shortcut = $shortcutButtons->getLayout()->createBlock(self::PEACH_PAYMENT_SHORTCUT_BLOCK);
            $shortcutButtons->addShortcut($shortcut);
        }
    }
}
