# Peach Payments Magento payment extension changelog

## 2025.27.02 - version 1.3.9
* Enhancements - Move all configs to StoreView scope for better payment method control.
* Fix - Code refactor and small fixes.

## 2025.29.01 - version 1.3.8
* Enhancements - Replace Flag webhooks process locking with Cronjob.
* Enhancements - Refactor webhook process logic.
* Fix - Online refund failed.
* Fix - Send diagnostics feature.
* Fix - Error on Admin Order page.

## 2024.31.10 - version 1.3.7
* Fix - Remove direct using of LaminasClient (fixed compatibility issue for Magento v2.4.5).
* Enhancements - Updated Content Security Policy.
* Enhancements - Added Buy With Peach Payments button on PLP.
* Fix - Refunds for Hosted payment method and Embedded checkout.

## 2024.19.09 - version 1.3.6
* Enhancements - Removed Apple Pay for Android users.
* Enhancements - Update endpoint urls for CNP and server to server.
* Enhancements - Support for php version 8.3 and magento 2.4.7-p2.

## 2024.05.08 - version 1.3.5
* Enhancements - Added sort order for each Hosted payment method.
* Fix - Transaction was without information about coupon code applied.
* Enhancements - Removed Apple Pay for windows users.
* Enhancements - Added Unit tests.

## 2024.17.06 - version 1.3.4
* Enhancements - System configuration redesign.
* Enhancements - Add Pay With PeachPayments button on product details page.
* Enhancements - Skip additional page before redirect to Peach Payments hosted page.

## 2024.11.04 - version 1.3.3
* Enhancements - Add send diagnostics button.
* Enhancements - Add redirect to configurations button.

## 2024.11.04 - version 1.3.2
* Enhancements - update name "Authorisation bearer Token" on Magento config to access token
* Enhancements - new version available notification
* Enhancements - update requests parameters for subscription products
* Enhancements - code refactor
* Fix - force default payment method

## 2024.12.03 - version 1.3.1
* Fix - duplicate invoices
* Fix - redirect to wait controller after successful payment
* Enhancements - encrypt system configuration values
* Enhancements - show Apple Pay button on all browsers

## 2024.12.02 - version 1.3.0
* Feature - Embedded Checkout support
* Fix - Support Apple Pay as a defaultPaymentMethod
* Enhancements - Removed card expiry validation on Card widget
* Enhancements - Remove billing address fields on Card widget

## 2023.09.01 - version 1.2.9
* Fix - Check allowed currencies in store scope
* Enhancements - Add php v8.2 support into composer.json

## 2023.05.25 - version 1.2.8
* Enhancements - Added support for Magento v2.4.6
* Enhancements - Remove deprecated `Zend_HTTP`
* Feature - Added Payment Consolidation on checkout
* Feature - PWA Compatability
* Feature - Added new payment method CapitecPay
* Fix - "Magento Version" in admin panel to work with composer
* Fix - Refund bug
* Fix - Payment sort order changing after applying coupon
* Fix - Canceled orders
* Fix - Headers not exist in webhook (X-Initialization-Vector, X-Authentication-Tag)

## 2023.02.26 - version 1.2.7
* Feature - Show plugin, subscription, and Magento version in config for quick reference
* Enhancements - Added Playwright ID for autotesting
* Enhancements - Improved debug logging
* Enhancements - Add Blink By Emtel Payment method
* Enhancements - Add FinChoicePay Payment method
* Fix - Prevent changing order status from Completed to Processing by webhook
* Fix - Removed function that ignores empty data when signing webhook

## 2022.12.19 - version 1.2.6

- Enhancement - Added **New Card** via **My Account**.
- Enhancement - Added **Force Default Payment Method**.
- Fix - Minicart being cached on embedded card or saved card redirect.
- Fix - Embedded card form to pass transaction or order ID to Peach Payments.
- Fix - InstantEFT payments shown as card.

## 2022.11.14 - version 1.2.5

- Enhancement - Added billing fields on the embedded card payment form.
- Enhancement - Improved error logging.
- Enhancement - Updated PHP compatibility to 8.1 and added support for Magento 2.4.5.
- Fix - Transactions failing when buying subscription products.
- Fix - Subscription product + coupon totals calculations.
- Fix - Payment status stays pending after successful payment.
- Fix - Partial refund error.

## 2022.08.25 - version 1.2.4

- Feature - Added support for one-click checkout.
- Enhancement - Updated payment method logos.
- Fix - Removed billing state from initial payment requests.
- Fix - Added credential on file (COF) parameters in payment requests.
- Fix - Payment refund error.

## 2022.06.25 - version 1.2.3

- Enhancement - Updated payment method logos.
- Enhancement - Added logging.
- Fix - Refunds and other module errors.
- Fix - Added compatibility with PHP 7.3.

## 2022.04.11 - version 1.2.2

- Feature - Added integration into the ParadoxLabs Subscription plugin.
- Feature - Added webhook support when payments are done on the card widget.
- Enhancement - Added support for 3-D Secure 2.0.
- Fix - Applied new Peach Payments brand.
- Fix - Removed inactive payment methods.
- Fix - Fixed spelling errors.
- Fix - Used our Copy and Pay widget instead of the Magento native card form.
- Fix - Added logic to show the card widget only if a subscription product is in the cart.
- Fix - Fixed an error when viewing invoices for Masterpass payments.
- Fix - Made the recurring `EntityId` field optional.

