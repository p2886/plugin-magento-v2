<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Command\Webhook;

use Laminas\Http\Headers;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as DataHelper;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\Web\Hooks as Webhook;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;

/**
 * Process encrypted webhook
 *
 * @spi
 */
class ProcessEncrypted
{
    const REQUEST_BODY_KEY  = 'encryptedBody';
    const HEADER_IV         = 'X-Initialization-Vector';
    const HEADER_AUTH_TAG   = 'X-Authentication-Tag';

    /**
     * @param ConfigHelper $configHelper
     * @param DataHelper $dataHelper
     * @param WebhooksFactory $webhooksFactory
     * @param WebhooksResource $webhooksResource
     * @param JsonSerializer $jsonSerializer
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        private ConfigHelper $configHelper,
        private DataHelper $dataHelper,
        private WebhooksFactory $webhooksFactory,
        private WebhooksResource $webhooksResource,
        private JsonSerializer $jsonSerializer,
        private UnifiedLogger $unifiedLogger
    ) {
    }

    /**
     * Process request.
     *
     * @param string $requestData
     * @param Headers $headers
     *
     * @return void
     */
    public function execute(string $requestData, Headers $headers)
    {
        try {
            $decrypted = $this->decryptRequest($requestData, $headers);
            if (false === $decrypted) {
                $this->unifiedLogger->debug(
                    [
                        'message' => 'Decrypted webhook is empty.',
                        'class' => ProcessEncrypted::class . '::execute()',
                        'data' => $requestData
                    ]
                );

                return;
            }

            $this->unifiedLogger->debug(
                [
                    'message' => 'Encrypted webhook received',
                    'class' => ProcessEncrypted::class . '::execute()',
                    'request' => $decrypted
                ]
            );

            $request = $this->jsonSerializer->unserialize($decrypted);

            if (!isset($request['type'])) {
                return;
            }

            /** @var Webhook $webhook */
            $webhook = $this->webhooksFactory->create();
            if (isset($request['payload']['merchantTransactionId'])) {
                $this->webhooksResource->load(
                    $webhook,
                    $request['payload']['merchantTransactionId'],
                    'order_increment_id'
                );
            }

            $this->dataHelper->mapDataFromWebhookPayload($webhook, $request['payload']);
            $webhook->setData('request', $this->jsonSerializer->serialize($this->dataHelper->mapWebhookData($request)));
            $webhook->setData('is_processed', false);

            $this->unifiedLogger->debug(
                [
                    'message' => 'Webhook Request',
                    'class' => ProcessEncrypted::class . '::execute()',
                    'request' => $this->jsonSerializer->serialize($webhook->getData())
                ]
            );

            $this->webhooksResource->save($webhook);
            $this->unifiedLogger->debug(
                [
                    'message' => 'Successfully saved Encrypted webhook',
                    'class' => ProcessEncrypted::class . '::execute()',
                    'webhook' => $request
                ]
            );
        } catch (\Exception $e) {
            $this->unifiedLogger->debug(
                [
                    'message' => 'Error processing Encrypted webhook',
                    'class' => ProcessEncrypted::class . '::execute()',
                    'webhook' => $request ?? '',
                    'exception' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ]
            );
        }
    }

    /**
     * Decrypt request.
     *
     * @param string $requestData
     * @param Headers $headers
     *
     * @return false|string
     */
    private function decryptRequest(string $requestData, Headers $headers)
    {
        $content = $this->jsonSerializer->unserialize($requestData);
        $decryptionKey = $this->configHelper->getDecryptionKey();
        $headerIv = $headers->get(self::HEADER_IV);
        $headerAuthTag = $headers->get(self::HEADER_AUTH_TAG);

        if (isset($content[self::REQUEST_BODY_KEY]) && $headerIv && $headerAuthTag) {
            $key = hex2bin($decryptionKey);
            $iv = hex2bin($headerIv->getFieldValue());
            $authTag = hex2bin($headerAuthTag->getFieldValue());
            $cipherText = hex2bin($content[self::REQUEST_BODY_KEY]);

            return openssl_decrypt(
                $cipherText,
                "aes-256-gcm",
                $key,
                OPENSSL_RAW_DATA,
                $iv,
                $authTag
            );
        }

        return false;
    }
}
