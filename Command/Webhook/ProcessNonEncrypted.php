<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Command\Webhook;

use PeachPayments\Hosted\Helper\Data as DataHelper;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;

/**
 * Process general webhook
 *
 * @spi
 */
class ProcessNonEncrypted
{
    /**
     * @param DataHelper $dataHelper
     * @param WebhooksFactory $webhooksFactory
     * @param WebhooksResource $webhooksResource
     * @param JsonSerializer $jsonSerializer
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        private DataHelper $dataHelper,
        private WebhooksFactory $webhooksFactory,
        private WebhooksResource $webhooksResource,
        private JsonSerializer $jsonSerializer,
        private UnifiedLogger $unifiedLogger
    ) {
    }

    /**
     * Process webhook
     *
     * @param array $data
     * @return void
     */
    public function execute(array $data)
    {
        if (isset($data['customParameters'])) {
            foreach ($data['customParameters'] as $key => $value) {
                $data["customParameters[$key]"] = $value;
            }

            unset($data['customParameters']);
        }

        try {
            $signed = $this->dataHelper->signData($data, false);
            $webhook = $this->webhooksFactory->create();
            $this->webhooksResource->load($webhook, $data['merchantTransactionId'], 'order_increment_id');

            if (!$webhook->getId() || $signed['signature'] !== $data['signature']) {
                $this->unifiedLogger->debug(
                    [
                        'message' => 'Error checking request signature.',
                        'class' => ProcessNonEncrypted::class . '::execute()',
                        'webhook_id' => $webhook->getId(),
                        'signature_signed' => $signed['signature'],
                        'signature_in_request' => $data['signature'],
                        'request' => $data
                    ]
                );

                return;
            }

            $insert = $this->dataHelper->mapWebhookData($data);

            foreach ($insert as $key => $item) {
                $webhook->setData($key, $item);
            }

            // save full request
            $webhook->setData('request', $this->jsonSerializer->serialize($data));
            $webhook->setData('is_processed', false);

            $this->unifiedLogger->debug(
                [
                    'message' => 'Non Encrypted webhook received',
                    'class' => ProcessNonEncrypted::class . '::execute()',
                    'request' => json_encode($webhook->getData())
                ]);

            $this->webhooksResource->save($webhook);
            $this->unifiedLogger->debug(
                [
                    'message' => 'Non Encrypted webhook processed.',
                    'class' => ProcessNonEncrypted::class . '::execute()'
                ]
            );
        } catch (\Exception $e) {
            echo $e->getMessage();
            $this->unifiedLogger->debug(
                [
                    'message' => 'Error processing Non Encrypted webhook',
                    'class' => ProcessNonEncrypted::class . '::execute()',
                    'exception' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ]
            );
        }
    }
}
