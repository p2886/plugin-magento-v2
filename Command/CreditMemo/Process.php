<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Command\CreditMemo;

use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Framework\HTTP\ClientFactory as HttpClient;
use PeachPayments\Hosted\Helper\Config;
use PeachPayments\Hosted\Helper\Data;
use PeachPayments\Hosted\Logger\UnifiedLogger;

class Process
{
    /**
     * @var HttpClient
     */
    private $httpClient;
    /**
     * @var Config
     */
    private $config;
    /**
     * @var Data
     */
    private $helper;

    /**
     * @param HttpClient $httpClient
     * @param Config $config
     * @param Data $helper
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        HttpClient $httpClient,
        Config $config,
        Data $helper,
        private UnifiedLogger $unifiedLogger
    ) {
        $this->httpClient = $httpClient;
        $this->config = $config;
        $this->helper = $helper;
    }

    /**
     * @param InfoInterface $payment
     * @param $amount
     * @return void
     */
    public function execute(InfoInterface $payment, $amount): void
    {
        /** @var Creditmemo $creditmemo */
        $creditMemo = $payment->getData('creditmemo');
        $id = str_replace('-refund', '', $payment->getParentTransactionId());
        $amount = number_format((float) $amount, 2, '.', '');
        $entityId = $this->config->getEntityId3DSecure();

        $params = [
            'authentication.entityId' => $entityId,
            'amount' => $amount,
            'paymentType' => 'RF',
            'currency' => $creditMemo->getOrderCurrencyCode(),
            'id' => $id,
        ];

        $this->unifiedLogger->debug(
            [
                'message' => 'Refund start process.',
                'params' => $params
            ]
        );

        try {
            $client = $this->httpClient->create();
            $client->post(
                $this->config->getApiUrl('refund'),
                $this->helper->signData($params, false)
            );
            $result = json_decode($client->getBody(), true);

            $creditMemo->addComment($this->extractResponseDetails($result));
            $payment->setTransactionId($result['id'] ?? $payment->getLastTransId());

            $this->unifiedLogger->debug(
                [
                    'message' => 'Refund processed.',
                    'response' => $result
                ]
            );
        } catch (\Exception $e) {
            $this->unifiedLogger->debug(
                [
                    'message' => 'Refund error processing.',
                    'exception' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ]
            );
        }
    }

    /**
     * @param array $response
     * @return string
     */
    private function extractResponseDetails(array $response) : string
    {
        // For failed responses
        if (!isset($response['id'])) {
            return sprintf(
                "Transaction failed with code %s because of %s. Please check logs for details.",
                $response['result.code'] ?? '',
                $response['result.description'] ?? ''
            );
        }

        // For success responses
        return sprintf(
            "Successfully refunded. Transaction id is %s.",
            $response['id']
        );
    }
}
