<?php

/**
 * GetVaultTokensForCart
 * php version 7.4
 *
 * @category magento_2.4.3
 * @package  Magento2
 * @author   X2Y.io Dev Team <dev@x2y.io>
 * @license  https://http://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://x2y.io/
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Command;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use PeachPayments\Hosted\Model\Ui\ConfigProvider;

class GetVaultGatewayTokensForCart
{
    /**
     * @var PaymentTokenRepositoryInterface
     */
    private $repository;
    /**
     * @var SearchCriteriaBuilderFactory
     */
    private $criteriaBuilder;

    public function __construct(
        PaymentTokenRepositoryInterface $repository,
        SearchCriteriaBuilderFactory $criteriaBuilder
    ) {
        $this->repository = $repository;
        $this->criteriaBuilder = $criteriaBuilder;
    }

    /**
     * @param CartInterface|null $cart
     * @param int|null $customerId
     *
     * @return array
     */
    public function execute(CartInterface $cart = null, int $customerId = null): array
    {
        $gatewayTokens = [];
        $searchCriteria = $this->criteriaBuilder->create();
        $searchCriteria->addFilter(PaymentTokenInterface::PAYMENT_METHOD_CODE, ConfigProvider::CODE);
        $searchCriteria->addFilter(PaymentTokenInterface::CUSTOMER_ID, $customerId ?: $cart->getCustomer()->getId());
        $searchCriteria->addFilter(PaymentTokenInterface::IS_ACTIVE, '1');
        $searchCriteria->addFilter(PaymentTokenInterface::IS_VISIBLE, '1');

        $list = $this->repository->getList($searchCriteria->create());
        foreach ($list->getItems() as $item) {
            $gatewayTokens[] = $item->getGatewayToken();
        }

        return $gatewayTokens;
    }
}
