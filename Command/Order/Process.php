<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y Development team
 */

namespace PeachPayments\Hosted\Command\Order;

use Magento\Framework\Event\ManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender as InvoiceEmailSender;
use Magento\Sales\Model\Order\Email\Sender\OrderSender as OrderEmailSender;
use Magento\Sales\Model\OrderFactory;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\Web\Hooks;

class Process
{
    const RESULT_CODES_SUCCESS = ['000.000.000', '000.100.110', '000.100.111', '000.100.112'];
    const RESULT_CODES_PENDING = ['000.200.000', '000.200.100'];

    const METHOD_CODE_MAP = [
        'eftsecure' => 'eftsecure',
        'masterpass' => 'masterpass',
        'mobicred' => 'mobicred',
        'mpesa' => 'mpesa',
        'aplus' => 'aplus',
        'applepay' => 'applepay',
        'paypal' => 'paypal',
        'stitcheft' => 'stitcheft',
        'payflex' => 'payflex',
        'zeropay' => 'zeropay',
        '1foryou' => '1foryou',
        'blinkbyemtel' => 'blinkbyemtel',
        'finchoicepay' => 'finchoicepay',
        'capitecpay' => 'capitecpay',
        'allpayments' => 'allpayments',
        'default' => 'default',
        'card' => 'card'
    ];

    const FLAG_CODE = 'peachpayments_success_invoice';

    /**
     * @var OrderRepositoryInterface $orderRepository
     */
    private $orderRepository;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var OrderEmailSender
     */
    private $orderEmailSender;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var InvoiceEmailSender
     */
    private $invoiceEmailSender;
    /**
     * @var OrderFactory
     */
    private $orderFactory;
    private UnifiedLogger $unifiedLogger;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param ManagerInterface $eventManager
     * @param OrderEmailSender $orderEmailSender
     * @param InvoiceEmailSender $invoiceEmailSender
     * @param ConfigHelper $configHelper
     * @param OrderFactory $orderFactory
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        ManagerInterface $eventManager,
        OrderEmailSender $orderEmailSender,
        InvoiceEmailSender $invoiceEmailSender,
        ConfigHelper $configHelper,
        OrderFactory $orderFactory,
        UnifiedLogger $unifiedLogger,
    ) {
        $this->orderRepository = $orderRepository;
        $this->eventManager = $eventManager;
        $this->orderEmailSender = $orderEmailSender;
        $this->configHelper = $configHelper;
        $this->invoiceEmailSender = $invoiceEmailSender;
        $this->orderFactory = $orderFactory;
        $this->unifiedLogger = $unifiedLogger;
    }

    /**
     * Process webhook
     *
     * @param Hooks $webhook
     * @return void
     */
    public function execute(Hooks $webhook)
    {
        /** @var Order $order */
        $order = $this->orderFactory->create();
        $order->loadByIncrementId($webhook->getData('order_increment_id'));

        if (!$order->getEntityId()) {
            return;
        }

        $resultCode = $webhook->getResultCode();

        if ($this->isSuccessResultCode($resultCode) && $order->getPayment()) {
            try {
                $this->processSuccessRequest($order, $webhook);
            } catch (\Exception $e) {
                $this->unifiedLogger->debug(
                    [
                        'message' => 'Order processing error',
                        'exception' => $e->getMessage(),
                        'trace' => $e->getTrace()
                    ]
                );
            }

            return;
        }

        if (!$this->isPendingResultCode($resultCode)) {
            try {
                $this->unifiedLogger->debug(
                    [
                        'message'=> 'Cancelling order id: ' . $order->getId(),
                        'result_code' => $resultCode,
                        'reason' => 'Result code not equal to 000.200.000 or 000.200.100'
                    ]
                );

                $order->cancel();
                $this->orderRepository->save($order);
            } catch (\Exception $e) {
                $this->unifiedLogger->debug(
                    [
                        'message' => 'Cancelling order error, order id: ' . $order->getId(),
                        'exception'=> $e->getMessage(),
                        'trace' => $e->getTrace()
                    ]
                );
            }
        }

        $this->eventManager->dispatch('peachpayments_order_failed', ['result' => $webhook]);
    }

    private function isSuccessResultCode($code): bool
    {
        return in_array($code, self::RESULT_CODES_SUCCESS);
    }

    private function isPendingResultCode($code): bool
    {
        return in_array($code, self::RESULT_CODES_PENDING);
    }

    private function processSuccessRequest(Order $order, Hooks $webhook): void
    {
        $this->unifiedLogger->debug(
            [
                'message' => 'Webhook processing success result code.',
                'webhook' => @json_encode($webhook->getData())
            ]
        );

        if ($order->isCanceled()) {
            $this->unCancelOrder($order);
        }

        if ($order->canInvoice()) {
            $this->invoiceOrder($order, $webhook);
        }

        if (in_array($order->getState(), [Order::STATE_PENDING_PAYMENT, Order::STATE_NEW, Order::STATE_CANCELED])) {
            $order->setState(Order::STATE_PROCESSING)
                ->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING))
                ->addStatusHistoryComment(__('Approved payment online at PeachPayments.'));
        }

        $this->orderRepository->save($order);
        $this->eventManager->dispatch('peachpayments_order_succeed', ['result' => $webhook]);

        $this->unifiedLogger->debug(
            [
                'message' => 'Webhook with success result code processed.'
            ]
        );
    }

    private function invoiceOrder(Order $order, Hooks $webhook)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $order->getPayment();

        $payment->setTransactionId($webhook->getData('peach_id'));
        // check amount which can be invoiced, compare with amount in request
        $payment->registerCaptureNotification($webhook->getData('amount'), true);
        $payment->setAdditionalInformation('peach_request', $webhook->getData('request'));

        $paymentBrand = $webhook->getData('payment_brand') ?: 'N/A';
        $methodCode = self::METHOD_CODE_MAP[strtolower($paymentBrand)] ?? self::METHOD_CODE_MAP['card'];

        if ($payment->getMethod() !== 'peachpayments_hosted_applepay') {
            $payment->setMethod('peachpayments_hosted_' . $methodCode);
        }

        if ($order->getCanSendNewEmailFlag() && $this->configHelper->getIsSendOrderEmail()) {
            $this->orderEmailSender->send($order);
        }

        if ($this->configHelper->getIsSendInvoiceEmail()) {
            foreach ($order->getInvoiceCollection() as $invoice) {
                if ($invoice->getEmailSent()) {
                    continue;
                }

                $this->invoiceEmailSender->send($invoice);

                $order->addRelatedObject($invoice)
                    ->addCommentToStatusHistory(
                        __('You notified customer about invoice #%1.', $invoice->getIncrementId())
                    )
                    ->setIsCustomerNotified(true);
            }
        }
    }

    private function unCancelOrder($order)
    {
        $productStockQty = [];
        foreach ($order->getAllVisibleItems() as $item) {
            $productStockQty[$item->getProductId()] = $item->getQtyCanceled();
            foreach ($item->getChildrenItems() as $child) {
                $productStockQty[$child->getProductId()] = $item->getQtyCanceled();
                $child->setQtyCanceled(0);
                $child->setTaxCanceled(0);
                $child->setDiscountTaxCompensationCanceled(0);
            }

            $item->setQtyCanceled(0);
            $item->setTaxCanceled(0);
            $item->setDiscountTaxCompensationCanceled(0);
        }

        $order->setSubtotalCanceled(0);
        $order->setBaseSubtotalCanceled(0);

        $order->setTaxCanceled(0);
        $order->setBaseTaxCanceled(0);

        $order->setShippingCanceled(0);
        $order->setBaseShippingCanceled(0);

        $order->setDiscountCanceled(0);
        $order->setBaseDiscountCanceled(0);

        $order->setTotalCanceled(0);
        $order->setBaseTotalCanceled(0);

        if (!empty($comment)) {
            $order->addStatusHistoryComment($comment, false);
        }
    }
}
