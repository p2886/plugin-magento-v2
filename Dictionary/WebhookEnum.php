<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Dictionary;

enum WebhookEnum: string
{
    /** WEBHOOK FIELDS */
    case AMOUNT = 'amount';
    case CURRENCY = 'currency';
    case CHECKOUT_ID = 'checkout_id';
    case MERCHANT_NAME = 'merchant_name';
    case PAYMENT_BRAND = 'payment_brand';
    case PAYMENT_TYPE = 'payment_type';
    case RESULT_CODE = 'result_code';
    case RESULT_DESCRIPTION = 'result_description';
    case ORDER_INCREMENT_ID = 'order_increment_id';
}
