<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Plugin;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Session\SessionManager;

/**
 * Set correct cookie PHPSESSID to prevent session lost
 * because request.cookie doesn't have it after redirect from PeachPayments
 */
class SetSessionId
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * SetSessionId constructor.
     * @param RequestInterface $request
     */
    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * Set PHPSESSID cookie from param customerParameters[PHPSESSID]
     *
     * @param SessionManager $subject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeStart(
        SessionManager $subject
    ) {
        $customParameters = $this->request->getParam('customParameters') ?? [];
        $sessId           = $customParameters['PHPSESSID'] ?? null;
        $currentSessId    = $_COOKIE['PHPSESSID'] ?? null;
        $isPathPP         = $this->request->getPathInfo() == '/pp-hosted/secure/payment/';

        if ($sessId && $isPathPP && null === $currentSessId) {
            $_COOKIE['PHPSESSID'] = $sessId;
        }
    }
}
