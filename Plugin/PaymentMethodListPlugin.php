<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Plugin;

use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Payment\Api\PaymentMethodListInterface;
use PeachPayments\Hosted\Helper\Config;

/**
 * Display payment methods from config.
 */
class PaymentMethodListPlugin
{
    /**
     * @var CookieManagerInterface
     */
    private CookieManagerInterface $cookieManager;
    /**
     * @var Config
     */
    private Config $config;

    /**
     * @param CookieManagerInterface $cookieManager
     * @param Config $config
     */
    public function __construct(
        CookieManagerInterface $cookieManager,
        Config $config
    ) {
        $this->cookieManager = $cookieManager;
        $this->config = $config;
    }

    /**
     * @param PaymentMethodListInterface $subject
     * @param $activeList
     * @return array
     */
    public function afterGetActiveList(PaymentMethodListInterface $subject, $activeList): array
    {
        if (!$this->cookieManager->getCookie('pay-with-peach-payment')) {
            return $activeList;
        }

        $allowedPayments = $this->config->getPayWithPeachMethods();
        $activeListFiltered = [];
        $allowedPayments = explode(',', $allowedPayments);
        foreach ($activeList as $method) {
            foreach ($allowedPayments as $val) {
                if (str_contains($method->getCode(), $val)) {
                    $activeListFiltered[] = $method;
                }
            }
        }

        return $activeListFiltered;
    }
}
