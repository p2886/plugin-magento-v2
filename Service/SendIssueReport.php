<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Service;

use Laminas\Mime\Message;
use Laminas\Mime\Mime;
use Laminas\Mime\Part;
use Magento\Contact\Model\ConfigInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\Directory\ReadFactory;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SendIssueReport
{
    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ReadFactory
     */
    private $readFactory;

    /**
     * @var File
     */
    private $file;

    /**
     * @param TransportBuilder $transportBuilder
     * @param StoreManagerInterface $storeManager
     * @param Emulation $emulation
     * @param ScopeConfigInterface $scopeConfig
     * @param ReadFactory $readFactory
     * @param File $file
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        Emulation $emulation,
        ScopeConfigInterface $scopeConfig,
        ReadFactory $readFactory,
        File $file
    ) {
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->emulation = $emulation;
        $this->scopeConfig = $scopeConfig;
        $this->readFactory = $readFactory;
        $this->file = $file;
    }

    /**
     * @param array $data
     * @param string $copyTo
     * @param string $archivePath
     * @return void
     * @throws LocalizedException
     * @throws MailException
     * @throws NoSuchEntityException
     * @throws FileSystemException
     */
    public function sendEmail(array $data, string $copyTo, string $archivePath)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $this->emulation->startEnvironmentEmulation($storeId, Area::AREA_FRONTEND, true);

        $transport = $this->transportBuilder
            ->setTemplateIdentifier('peach_payments_issue_report')
            ->setTemplateOptions(['area' => 'frontend', 'store' => $this->storeManager->getStore()->getId()])
            ->setTemplateVars(['magento_data' => $data])
            ->addTo('support@peachpayments.com')
            ->setFromByScope($this->getSendEmailFrom());

        if ($copyTo) {
            $emails = explode(',', $copyTo);
            foreach ($emails as $email) {
                $transport->addCc(trim($email));
            }
        }

        $transport = $transport->getTransport();

        $body = $transport->getMessage()->getBody();
        $mimeMessage = new Message();

        /** Set exists parts of message */
        $mimeMessage->setParts($body->getParts());

        /** Build Mime part for attach archive with logs and version and list of modules */
        $fileInfo = $this->file->getPathInfo($archivePath);
        $directoryRead = $this->readFactory->create($fileInfo['dirname'] ?? '');
        $fileContents = $directoryRead->readFile($fileInfo['basename'] ?? '');
        $attachment = new Part($fileContents);
        $attachment->type = 'application/zip';
        $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
        $attachment->encoding = Mime::ENCODING_BASE64;
        $attachment->filename = 'logs_modules_versions.zip';

        $mimeMessage->addPart($attachment);
        $transport->getMessage()->setBody($mimeMessage);
        $transport->sendMessage();

        $this->emulation->stopEnvironmentEmulation();
    }

    /**
     * Get send from email address.
     *
     * @return string
     */
    private function getSendEmailFrom(): string
    {
        return (string) $this->scopeConfig->getValue(ConfigInterface::XML_PATH_EMAIL_SENDER);
    }
}
