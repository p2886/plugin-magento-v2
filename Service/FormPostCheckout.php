<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Service;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as HelperData;
use PeachPayments\Hosted\Model\Method\ApplePay;

class FormPostCheckout
{
    /**
     * @var HelperData
     */
    protected HelperData $helperData;

    /**
     * @var ConfigHelper
     */
    private ConfigHelper $configHelper;

    /**
     * @var RemoteAddress
     */
    private RemoteAddress $remoteAddress;

    /**
     * @var SessionManagerInterface
     */
    private SessionManagerInterface $checkoutSession;

    /**
     * @var UrlInterface
     */
    private UrlInterface $urlBuilder;

    /**
     * @var CookieManagerInterface
     */
    private CookieManagerInterface $cookieManager;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @param HelperData $helperData
     * @param RemoteAddress $remoteAddress
     * @param ConfigHelper $configHelper
     * @param CheckoutSession $checkoutSession
     * @param UrlInterface $urlBuilder
     * @param CookieManagerInterface $cookieManager
     * @param SerializerInterface $serializer
     */
    public function __construct(
        HelperData $helperData,
        RemoteAddress $remoteAddress,
        ConfigHelper $configHelper,
        CheckoutSession $checkoutSession,
        UrlInterface $urlBuilder,
        CookieManagerInterface $cookieManager,
        SerializerInterface $serializer
    ) {
        $this->helperData = $helperData;
        $this->remoteAddress = $remoteAddress;
        $this->configHelper = $configHelper;
        $this->checkoutSession = $checkoutSession;
        $this->urlBuilder = $urlBuilder;
        $this->cookieManager = $cookieManager;
        $this->serializer = $serializer;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getFormData(): array
    {
        return $this->helperData->signData($this->getUnsortedFormData());
    }

    /**
     * @return Order
     */
    private function getOrder(): Order
    {
        return $this->checkoutSession->getLastRealOrder();
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    private function getUnsortedFormData(): array
    {
        $order = $this->getOrder();
        $amount = number_format(
            (float) $order->getPayment()->getAmountOrdered(),
            2,
            '.',
            ''
        );

        $methodCode = strtoupper(
            str_replace(
                'peachpayments_hosted_',
                '',
                $order->getPayment()->getMethodInstance()->getCode()
            )
        );

        $defaultPaymentMethod = $this->getDefaultPaymentMethod($methodCode);

        $billingStreet = (array)$order->getBillingAddress()->getStreet();
        array_push($billingStreet, 'N/A', 'N/A');
        list($billingStreetOne, $billingStreetTwo) = $billingStreet;

        $billingStreetOne = $order->getBillingAddress()->getCountryId() . ' ' . $billingStreetOne;

        return array_merge([
            'authentication.entityId' => $this->configHelper->getEntityId3DSecure(),
            'amount' => $amount,
            'paymentType' => 'DB',
            'currency' => $order->getOrderCurrencyCode(),
            'shopperResultUrl' => $this->urlBuilder->getUrl('pp-hosted/secure/payment/'),
            'customParameters[PHPSESSID]' => $this->checkoutSession->getSessionId(),
            'merchantTransactionId' => $order->getIncrementId(),
            'plugin' => $this->helperData->getPlatformName(),

            'customer.givenName' => $order->getBillingAddress()->getFirstname(),
            'customer.surname' => $order->getBillingAddress()->getLastname(),
            'customer.mobile' => $order->getBillingAddress()->getTelephone(),
            'customer.email' => $order->getBillingAddress()->getEmail(),
            'customer.status' => $order->getCustomerIsGuest() ? 'NEW' : 'EXISTING',
            'customer.ip' => $this->remoteAddress->getRemoteAddress(),

            'billing.street1' => $billingStreetOne,
            'billing.street2' => $billingStreetTwo,
            'billing.city' => $order->getBillingAddress()->getCity(),
            'billing.country' => $order->getBillingAddress()->getCountryId(),
            'billing.postcode' => $order->getBillingAddress()->getPostcode()
        ], $this->getShippingDetails(), $this->getTrackingValue(), $defaultPaymentMethod);
    }

    /**
     * @return array
     */
    private function getShippingDetails(): array
    {
        $order = $this->getOrder();
        $shippingDetails = [];

        // @note exclude shipping address on virtual products
        if ($order->getShippingAddress()) {

            $shippingStreet = (array)$order->getShippingAddress()->getStreet();
            $shippingCity = $order->getShippingAddress()->getCity();
            $shippingCountry = $order->getShippingAddress()->getCountryId();

            array_push($shippingStreet, 'N/A', 'N/A');
            list($shippingStreetOne, $shippingStreetTwo) = $shippingStreet;

            $shippingDetails = [
                'shipping.street1' => $shippingStreetOne,
                'shipping.street2' => $shippingStreetTwo,
                'shipping.city' => $shippingCity,
                'shipping.country' => $shippingCountry,
            ];
        }

        return $shippingDetails;
    }

    /**
     * @param string $code
     * @return array
     */
    private function getDefaultPaymentMethod(string $code): array
    {
        if ($code == 'ALLPAYMENTS') {
            return [];
        }

        $defaultPaymentMethod = [
            'defaultPaymentMethod' => $code,
            'forceDefaultMethod' => $this->configHelper->getIsForceDefaultMethod($code),
        ];

        if ($code == ApplePay::PAYMENT_CODE) {
            $defaultPaymentMethod['defaultPaymentMethod'] = 'APPLE PAY';
        }

        if ($code == 'BLINKBYEMTEL') {
            $defaultPaymentMethod['defaultPaymentMethod'] = strtolower($code);
        }

        return $defaultPaymentMethod;
    }

    /**
     * @return array
     */
    private function getTrackingValue(): array
    {
        $cookies = $this->cookieManager->getCookie('pay-with-peach-payment');
        if (!$cookies) {
            return [];
        }

        $result = [];
        $cookiesData = $this->serializer->unserialize($cookies);
        if ($cookiesData['tracking']) {
            $result['customParameters[tracking]'] = $cookiesData['tracking'];
        }

        return $result;
    }
}
