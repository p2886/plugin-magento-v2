<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Service;

use DateTime;
use DateTimeZone;
use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Module\ModuleList;
use Magento\Framework\Module\ModuleList\Loader;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use PeachPayments\Hosted\Helper\Config as HostedConfig;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\IssueReporter;

class MagentoDataCollection
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var File
     */
    private $file;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Loader
     */
    private $loader;

    /**
     * @var ModuleList
     */
    private $moduleList;
    private UnifiedLogger $unifiedLogger;
    private ResourceConnection $resource;
    private HostedConfig $hostedConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param SerializerInterface $serializer
     * @param File $file
     * @param Filesystem $filesystem
     * @param Loader $loader
     * @param ModuleList $moduleList
     * @param UnifiedLogger $unifiedLogger
     * @param ResourceConnection $resource
     * @param HostedConfig $hostedConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        SerializerInterface $serializer,
        File $file,
        Filesystem $filesystem,
        Loader $loader,
        ModuleList $moduleList,
        UnifiedLogger $unifiedLogger,
        ResourceConnection $resource,
        HostedConfig $hostedConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->serializer = $serializer;
        $this->file = $file;
        $this->filesystem = $filesystem;
        $this->loader = $loader;
        $this->moduleList = $moduleList;
        $this->unifiedLogger = $unifiedLogger;
        $this->resource = $resource;
        $this->hostedConfig = $hostedConfig;
    }

    /**
     * @param IssueReporter $item
     * @param string $archivePath
     * @param string $logDir
     * @return array
     * @throws Exception
     */
    public function processCollection(IssueReporter $item, string $archivePath, string $logDir): array
    {
        $data = [];
        $currentDateTime = new DateTime('now', new DateTimeZone('UTC'));

        $data['current_date'] = $currentDateTime->format('Y-m-d H:i:s');
        $data['php_version'] = phpversion();
        $data['description'] = $item->getDescription();
        $data['primary_domain'] = $this->scopeConfig->getValue('web/unsecure/base_url');
        $data['merchant_id'] = $this->hostedConfig->getMerchantId($this->storeManager->getStore()->getId());
        $data['magento_version'] = $this->getMagentoVersion();
        $data['websites_data'] = $this->getWebsitesAndStores();

        $this->generateArchive($logDir, $archivePath);

        return $data;
    }

    /**
     * @return string
     */
    private function getMagentoVersion(): string
    {
        $output = (string)system('php bin/magento --version');

        return $this->stripAnsiCodes($output);
    }

    /**
     * @param string $output
     * @return array|string|string[]|null
     */
    private function stripAnsiCodes(string $output)
    {
        return preg_replace('/\e\[[\d;]+m/', '', $output);
    }

    /**
     * @return array
     */
    private function getWebsitesAndStores(): array
    {
        $websites = $this->storeManager->getWebsites();
        $websitesData = [];

        foreach ($websites as $website) {
            $websiteData = [];
            foreach ($website->getStores() as $store) {
                $websiteData[$store->getName()] = $store->getBaseUrl();
            }
            $websitesData[$website->getName()] = $websiteData;
        }

        return $websitesData;
    }

    /**
     * @param string $logDir
     * @param string $archivePath
     * @return void
     * @throws LocalizedException
     */
    private function generateArchive(string $logDir, string $archivePath): void
    {
        $exceptionLog = $this->getLastLines($logDir . 'exception.log');
        $systemLog = $this->getLastLines($logDir . 'system.log');
        $peachPaymentsLog = $this->getPeachPaymentsLogs();
        $installedPackages = $this->getInstalledPackages();
        $moduleList = $this->getAllModules();

        $files = [
            'exception.log' => $exceptionLog,
            'system.log' => $systemLog,
            'peach_payments.log' => $peachPaymentsLog,
            'installed_packages.txt' => $installedPackages,
            'list_of_modules.txt' => $moduleList,
        ];

        $this->createZip($files, $archivePath);
    }

    /**
     * @param array $files
     * @param string $zipPath
     * @return void
     * @throws LocalizedException
     */
    public function createZip(array $files, string $zipPath): void
    {
        $zipData = $this->generateZipData($files);

        if (file_put_contents($zipPath, $zipData) === false) {
            throw new LocalizedException(__('Couldn\'t write a ZIP file at the path: %1', $zipPath));
        }
    }

    /**
     * @param array $files
     * @return string
     */
    private function generateZipData(array $files): string
    {
        $zipData = '';
        $centralDirectory = '';
        $offset = 0;
        $currentTimestamp = $this->getDosTime();

        foreach ($files as $fileName => $content) {
            $localHeader = $this->createLocalFileHeader($fileName, $content, $currentTimestamp);
            $zipData .= $localHeader['header'] . $content;

            $centralDirectory .= $this->createCentralDirectoryHeader(
                $fileName,
                $localHeader['crc32'],
                $localHeader['compressedSize'],
                $localHeader['uncompressedSize'],
                $offset,
                $currentTimestamp
            );

            $offset += strlen($localHeader['header']) + strlen($content);
        }

        $zipData .= $centralDirectory;
        $zipData .= $this->createEndOfCentralDirectoryRecord(count($files), strlen($centralDirectory), $offset);

        return $zipData;
    }

    /**
     * @return int
     */
    private function getDosTime(): int
    {
        $now = getdate();

        $year = max(1980, min($now['year'], 2107)) - 1980;
        $month = max(1, min($now['mon'], 12));
        $day = max(1, min($now['mday'], 31));
        $hour = max(0, min($now['hours'], 23));
        $minute = max(0, min($now['minutes'], 59));
        $second = max(0, min($now['seconds'], 59)) >> 1;

        return ($year << 25) |
        ($month << 21) |
        ($day << 16) |
        ($hour << 11) |
        ($minute << 5) |
        $second;
    }

    /**
     * @param string $fileName
     * @param string $content
     * @param int $timestamp
     * @return array
     */
    private function createLocalFileHeader(string $fileName, string $content, int $timestamp): array
    {
        $uncompressedSize = strlen($content);
        $compressedSize = $uncompressedSize;
        $crc32 = crc32($content);

        $header =
            "\x50\x4b\x03\x04" .
            "\x14\x00" .
            "\x00\x00" .
            "\x00\x00" .
            pack('V', $timestamp) .
            pack('V', $crc32) .
            pack('V', $compressedSize) .
            pack('V', $uncompressedSize) .
            pack('v', strlen($fileName)) .
            pack('v', 0) .
            $fileName;

        return [
            'header' => $header,
            'crc32' => $crc32,
            'compressedSize' => $compressedSize,
            'uncompressedSize' => $uncompressedSize,
        ];
    }

    /**
     * @param string $fileName
     * @param int $crc32
     * @param int $compressedSize
     * @param int $uncompressedSize
     * @param int $offset
     * @param int $timestamp
     * @return string
     */
    private function createCentralDirectoryHeader(
        string $fileName,
        int $crc32,
        int $compressedSize,
        int $uncompressedSize,
        int $offset,
        int $timestamp
    ): string {
        return
            "\x50\x4b\x01\x02" .
            "\x14\x00" .
            "\x14\x00" .
            "\x00\x00" .
            "\x00\x00" .
            pack('V', $timestamp) .
            pack('V', $crc32) .
            pack('V', $compressedSize) .
            pack('V', $uncompressedSize) .
            pack('v', strlen($fileName)) .
            pack('v', 0) .
            pack('v', 0) .
            pack('v', 0) .
            pack('v', 0) .
            pack('V', 0) .
            pack('V', $offset) .
            $fileName;
    }

    /**
     * @param int $totalEntries
     * @param int $centralDirectorySize
     * @param int $offset
     * @return string
     */
    private function createEndOfCentralDirectoryRecord(
        int $totalEntries,
        int $centralDirectorySize,
        int $offset
    ): string {
        return
            "\x50\x4b\x05\x06" .
            "\x00\x00" .
            "\x00\x00" .
            pack('v', $totalEntries) .
            pack('v', $totalEntries) .
            pack('V', $centralDirectorySize) .
            pack('V', $offset) .
            "\x00\x00";
    }

    /**
     * @return string
     */
    private function getPeachPaymentsLogs(): string
    {
        $connection = $this->resource->getConnection();
        $tableName = $this->resource->getTableName('peachpayments_hosted_logs');

        $select = $connection->select()->from($tableName);
        $logs = $connection->fetchAll($select);

        if (!empty($logs)) {
            return $this->formatTxtData($logs);

        } else {
            return '';
        }
    }

    /**
     * @param array $logs
     * @return string
     */
    private function formatTxtData(array $logs): string
    {
        return implode(PHP_EOL, array_map(function ($log) {
            return implode(' | ', array_map(fn($key, $value) => "{$key}: {$value}", array_keys($log), $log));
        }, $logs));
    }

    /**
     * @param string $filePath
     * @return string
     */
    private function getLastLines(string $filePath): string
    {
        $content = '';
        if ($this->file->fileExists($filePath)) {
            $content = implode(
                "\n",
                array_slice(file($filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES), -1000)
            );
        }
        return $content;
    }

    /**
     * @return string
     */
    private function getInstalledPackages(): string
    {
        $moduleDirectory = $this->filesystem->getDirectoryRead(DirectoryList::APP)->getAbsolutePath() . 'code';
        $packages = [];
        $composerLockPath = BP . '/composer.lock';
        $lockFileContent = file_get_contents($composerLockPath);
        $lockData = $this->serializer->unserialize($lockFileContent);

        $lockPackages = [];
        foreach ($lockData['packages'] as $package) {
            $lockPackages[$package['name']] = $package['version'];
        }

        $modules = [];
        $this->findModuleFiles($moduleDirectory, $modules);

        $allModules = array_merge($lockPackages, $modules);

        foreach ($allModules as $name => $version) {
            $packages[] = $name . ': ' . $version;
        }

        return implode("\n", $packages);
    }

    /**
     * @param string $dir
     * @param array $modules
     * @return void
     */
    private function findModuleFiles(string $dir, array &$modules)
    {
        $hasComposerJson = false;

        if (!is_dir($dir)) {
            return;
        }

        $composerPath = $dir . '/composer.json';
        if (file_exists($composerPath)) {
            $content = file_get_contents($composerPath);
            $json = $this->serializer->unserialize($content);
            if (isset($json['name']) && isset($json['version'])) {
                $modules[$json['name']] = $json['version'];
                $hasComposerJson = true;
            }
        }

        $etcDir = $dir . '/etc';
        $moduleXmlPath = $etcDir . '/module.xml';
        if (!$hasComposerJson) {
            if (is_dir($etcDir) && file_exists($moduleXmlPath)) {
                $content = file_get_contents($moduleXmlPath);
                $moduleName = '';
                $moduleVersion = '';

                if (preg_match('/<module.*?name="([^"]+)"/', $content, $nameMatch)) {
                    $moduleName = $nameMatch[1];
                }

                if (preg_match('/setup_version="([^"]+)"/', $content, $versionMatch)) {
                    $moduleVersion = $versionMatch[1];
                }

                if ($moduleName) {
                    $modules[$moduleName] = $moduleVersion ?: 'unknown';
                }
            }

            foreach (scandir($dir) as $subDir) {
                if ($subDir == '.' || $subDir == '..') continue;
                $path = $dir . DIRECTORY_SEPARATOR . $subDir;
                if (is_dir($path)) {
                    $this->findModuleFiles($path, $modules);
                }
            }
        }
    }

    /**
     * @return string
     */
    private function getAllModules(): string
    {
        $modules = [];
        $all = [];

        try {
            $all = $this->loader->load();
        } catch (Exception $e) {
            $this->unifiedLogger->critical('Error receiving modules. Exception. ' . $e->getMessage());
        }

        if (empty($all)) {
            return '';
        }

        foreach ($all as $key => $value) {
            if ($this->moduleList->has($key)) {
                $modules[] = $key . ' => ' . '1';
            } else {
                $modules[] = $key . ' => ' . '0';
            }
        }

        return implode("\n", $modules);
    }
}
