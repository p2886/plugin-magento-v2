<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Response;

use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Api\Data\OrderPaymentExtensionInterface;
use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Sales\Api\Data\OrderPaymentExtensionInterfaceFactory;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHelper;

class VaultTokenHandler implements HandlerInterface
{
    /**
     * @var SubjectReader
     */
    private $subjectReader;
    /**
     * @var PaymentTokenFactoryInterface
     */
    private $paymentTokenFactory;
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;
    /**
     * @var OrderPaymentExtensionInterfaceFactory
     */
    private $paymentExtensionFactory;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var PeachPaymentsHelper
     */
    private $peachPaymentsHelper;

    /**
     * @param SubjectReader $subjectReader
     * @param PaymentTokenFactoryInterface $paymentTokenFactory
     * @param JsonSerializer $jsonSerializer
     * @param OrderPaymentExtensionInterfaceFactory $paymentExtensionFactory
     * @param ConfigHelper $configHelper
     * @param PeachPaymentsHelper $peachPaymentsHelper
     */
    public function __construct(
        SubjectReader $subjectReader,
        PaymentTokenFactoryInterface $paymentTokenFactory,
        JsonSerializer $jsonSerializer,
        OrderPaymentExtensionInterfaceFactory $paymentExtensionFactory,
        ConfigHelper $configHelper,
        PeachPaymentsHelper $peachPaymentsHelper
    ) {
        $this->subjectReader = $subjectReader;
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->jsonSerializer = $jsonSerializer;
        $this->paymentExtensionFactory = $paymentExtensionFactory;
        $this->configHelper = $configHelper;
        $this->peachPaymentsHelper = $peachPaymentsHelper;
    }

    /**
     * @inheritDoc
     */
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = $this->subjectReader->readPayment($handlingSubject);
        $payment = $paymentDO->getPayment();
        $vaultToken = $this->getVaultToken($response);

        if (null !== $vaultToken) {
            $extensionAttributes = $this->getExtensionAttributes($payment);
            $extensionAttributes->setVaultPaymentToken($vaultToken);
            $payment->setAdditionalInformation('is_active_payment_token_enabler', true);
        }
    }

    /**
     * @param array $response
     * @return PaymentTokenInterface|null
     * @throws \Exception
     */
    private function getVaultToken(array $response)
    {
        $paymentToken = null;

        if (array_key_exists('registrationId', $response)) {
            $expirationDate = $this->peachPaymentsHelper->getCardExpirationDateForVault(
                $response['card']['expiryYear'],
                $response['card']['expiryMonth']
            );

            $paymentToken = $this->paymentTokenFactory->create(PaymentTokenFactoryInterface::TOKEN_TYPE_CREDIT_CARD);
            $paymentToken->setGatewayToken($response['registrationId']);
            $paymentToken->setExpiresAt($expirationDate);
            $paymentToken->setIsVisible(true);
            $paymentToken->setTokenDetails($this->jsonSerializer->serialize([
                'type' => $this->configHelper->getPaymentBrandByPeachPaymentsCode($response['paymentBrand']),
                'maskedCC' => $response['card']['last4Digits'],
                'expirationDate' => $expirationDate
            ]));
        }

        return $paymentToken;
    }

    /**
     * Get payment extension attributes
     *
     * @param InfoInterface $payment
     * @return OrderPaymentExtensionInterface
     */
    private function getExtensionAttributes(InfoInterface $payment)
    {
        $extensionAttributes = $payment->getExtensionAttributes();
        if (null === $extensionAttributes) {
            $extensionAttributes = $this->paymentExtensionFactory->create();
            $payment->setExtensionAttributes($extensionAttributes);
        }

        return $extensionAttributes;
    }
}
