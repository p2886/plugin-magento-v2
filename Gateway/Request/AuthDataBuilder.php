<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Request;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHelper;

class AuthDataBuilder implements BuilderInterface
{

    const ID = 'entityId';

    /**
     * @var PeachPaymentsHelper
     */
    private $peachPaymentsHelper;

    /**
     * @param PeachPaymentsHelper $peachPaymentsHelper
     */
    public function __construct(
        PeachPaymentsHelper $peachPaymentsHelper
    ) {
        $this->peachPaymentsHelper = $peachPaymentsHelper;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject)
    {
        return [
            self::ID => $this->peachPaymentsHelper->getEntityId()
        ];
    }
}
