<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Helper\Config;

class PaymentTypeDataBuilder implements BuilderInterface
{
    const CC_TYPE = 'paymentBrand';

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @param SubjectReader $subjectReader
     */
    public function __construct(SubjectReader $subjectReader)
    {
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject)
    {
        $payment = $this->subjectReader->readPayment($buildSubject)->getPayment();
        $ccType = $payment->getAdditionalInformation('cc_type');

        return [
            self::CC_TYPE => Config::CC_BRANDS[$ccType]['code'],
        ];
    }
}
