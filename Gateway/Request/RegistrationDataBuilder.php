<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Vault\Api\PaymentTokenManagementInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;

class RegistrationDataBuilder implements BuilderInterface
{
    const TOKEN = 'registration_token';

    /**
     * @var SubjectReader
     */
    private $subjectReader;
    /**
     * @var PaymentTokenManagementInterface
     */
    private $paymentTokenManagement;

    /**
     * @param SubjectReader $subjectReader
     * @param PaymentTokenManagementInterface $paymentTokenManagement
     */
    public function __construct(
        SubjectReader $subjectReader,
        PaymentTokenManagementInterface $paymentTokenManagement
    ) {
        $this->subjectReader = $subjectReader;
        $this->paymentTokenManagement = $paymentTokenManagement;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject)
    {
        $payment = $this->subjectReader->readPayment($buildSubject)->getPayment();
        $order = $this->subjectReader->readPayment($buildSubject)->getOrder();
        $publicHash = $payment->getAdditionalInformation('public_hash');
        $vaultToken = $this->paymentTokenManagement->getByPublicHash($publicHash, $order->getCustomerId());

        if (null !== $vaultToken) {
            return [
                self::TOKEN => $vaultToken->getGatewayToken()
            ];
        }

        return [];
    }
}
