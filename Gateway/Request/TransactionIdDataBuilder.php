<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;

use PeachPayments\Hosted\Gateway\Helper\SubjectReader;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;

class TransactionIdDataBuilder implements BuilderInterface
{
    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @param SubjectReader $subjectReader
     */
    public function __construct(SubjectReader $subjectReader)
    {
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject)
    {
        $payment = $this->subjectReader->readPayment($buildSubject)->getPayment();
        $tnxId = $payment->getAdditionalInformation(AuthorizationTrxIdHandler::KEY_TNX_ID);
        if ($tnxId) {
            return [
                AuthorizationTrxIdHandler::KEY_TNX_ID => $tnxId
            ];
        }

        return [];
    }
}
