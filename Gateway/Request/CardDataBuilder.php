<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Request;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use PeachPayments\Hosted\Gateway\Helper\SubjectReader;

class CardDataBuilder implements BuilderInterface
{
    const NUMBER    = 'card.number';
    const NAME      = 'card.holder';
    const EXP_YEAR  = 'card.expiryYear';
    const EXP_MONTH = 'card.expiryMonth';
    const CVV       = 'card.cvv';
    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @param SubjectReader $subjectReader
     */
    public function __construct(SubjectReader $subjectReader)
    {
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritDoc
     */
    public function build(array $buildSubject)
    {
        $payment = $this->subjectReader->readPayment($buildSubject)->getPayment();
        $order = $this->subjectReader->readPayment($buildSubject)->getOrder();

        return [
            self::NUMBER    => $payment->getAdditionalInformation('cc_number'),
            self::CVV       => $payment->getAdditionalInformation('cc_cid'),
            self::EXP_MONTH => $payment->getAdditionalInformation('cc_exp_month'),
            self::EXP_YEAR  => $payment->getAdditionalInformation('cc_exp_year'),
            self::NAME      => $order->getBillingAddress()->getFirstname() . ' ' .
                $order->getBillingAddress()->getLastname()
        ];
    }
}
