<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Http;

use Magento\Framework\HTTP\Client\Curl as CurlClient;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use PeachPayments\Hosted\Logger\UnifiedLogger;

class Client extends CurlClient implements ClientInterface
{
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;

    /**
     * @param JsonSerializer $jsonSerializer
     * @param UnifiedLogger $unifiedLogger
     * @param null $sslVersion
     */
    public function __construct(
        JsonSerializer $jsonSerializer,
        private UnifiedLogger $unifiedLogger,
        $sslVersion = null
    ) {
        parent::__construct($sslVersion);
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @inheritDoc
     */
    public function placeRequest(TransferInterface $transferObject)
    {
        $transferBody = $transferObject->getBody();
        $log = [
            'Request uri' => $transferObject->getUri()
        ];

        $this->setHeaders($transferObject->getHeaders());
        switch ($transferObject->getMethod()) {
            case 'GET':
                $this->get($transferObject->getUri());
                break;
            case 'POST':
                $this->post($transferObject->getUri(), $transferBody);
                break;
            default:
                $this->makeRequest($transferObject->getMethod(), $transferObject->getUri());
        }

        try {
            $response = $this->getBody();
            $log['Response'] = $response;
            $result = $this->jsonSerializer->unserialize($response);
            $this->unifiedLogger->debug($log);

            return $result;
        } catch (\Exception $exception) {
            $log['Exception'] = $exception->getMessage();
            $this->unifiedLogger->debug($log);

            throw new ClientException(__($exception->getMessage()));
        }
    }
}
