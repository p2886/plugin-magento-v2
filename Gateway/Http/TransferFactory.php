<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Http;

use Magento\Payment\Gateway\Http\TransferFactoryInterface;
use PeachPayments\Hosted\Gateway\Request\RegistrationDataBuilder;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;

class TransferFactory implements TransferFactoryInterface
{
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var HttpTransferObject
     */
    private $httpTransferObject;

    /**
     * @param ConfigHelper $configHelper
     * @param HttpTransferObject $httpTransferObject
     */
    public function __construct(
        ConfigHelper $configHelper,
        HttpTransferObject $httpTransferObject
    ) {
        $this->configHelper = $configHelper;
        $this->httpTransferObject = $httpTransferObject;
    }

    /**
     * @inheritDoc
     */
    public function create(array $request)
    {
        $token = $this->readToken($request);
        $transactionId = $this->readTransactionId($request);
        $uri = null !== $token ?
            $this->configHelper->getApiRegistrationUri($token) :
            $this->configHelper->getApiPaymentsUri($transactionId);

        return $this->httpTransferObject->create(
            $uri,
            'POST',
            $request
        );
    }

    /**
     * @param array $request
     * @return string|null
     */
    private function readToken(array &$request)
    {
        $token = $request[RegistrationDataBuilder::TOKEN] ?? null;
        if (null !== $token) {
            unset($request[RegistrationDataBuilder::TOKEN]);
        }

        return $token;
    }

    /**
     * @param array $request
     * @return string|null
     */
    private function readTransactionId(array &$request)
    {
        $id = $request[AuthorizationTrxIdHandler::KEY_TNX_ID] ?? null;
        if (null !== $id) {
            unset($request[AuthorizationTrxIdHandler::KEY_TNX_ID]);
        }

        return $id;
    }
}
