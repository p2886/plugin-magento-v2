<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Helper;

use Magento\Payment\Gateway\Helper\SubjectReader as MagentoSR;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;

class SubjectReader
{
    /**
     * @param array $subject
     * @return PaymentDataObjectInterface
     */
    public function readPayment(array $subject)
    {
        return MagentoSR::readPayment($subject);
    }

    /**
     * @param array $subject
     * @return string
     */
    public function readField(array $subject)
    {
        return MagentoSR::readField($subject);
    }
}
