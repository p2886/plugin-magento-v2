<?php

namespace PeachPayments\Hosted\Gateway\Helper;

use Magento\Payment\Gateway\Http\TransferBuilderFactory as TransferBuilder;
use Magento\Framework\UrlInterface;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Query\GetAccessToken as AccessTokenQuery;

class HttpTransferObject
{
    /**
     * @var TransferBuilder
     */
    private $transferBuilder;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var AccessTokenQuery
     */
    private $accessTokenQuery;
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @param TransferBuilder $transferBuilder
     * @param ConfigHelper $configHelper
     * @param AccessTokenQuery $accessTokenQuery
     * @param UrlInterface $url
     */
    public function __construct(
        TransferBuilder $transferBuilder,
        ConfigHelper $configHelper,
        AccessTokenQuery $accessTokenQuery,
        UrlInterface $url
    ) {
        $this->transferBuilder = $transferBuilder;
        $this->configHelper = $configHelper;
        $this->accessTokenQuery = $accessTokenQuery;
        $this->url = $url;
    }

    public function create(
        string $uri,
        string $method,
        array $body = []
    ) {
        return $this->transferBuilder
            ->create()
            ->setUri($uri)
            ->setMethod($method)
            ->setBody($body)
            ->setHeaders(
                [
                    'Authorization' => 'Bearer ' . $this->configHelper->getAccessToken(),
                    CURLOPT_SSL_VERIFYPEER => $this->configHelper->isLiveMode(),
                    CURLOPT_RETURNTRANSFER => true
                ]
            )
            ->build();
    }

    public function createV2(
        string $uri,
        string $method,
        array $body = []
    ) {
        return $this->transferBuilder
            ->create()
            ->setUri($uri)
            ->setMethod($method)
            ->setBody(json_encode($body))
            ->setHeaders(
                [
                    'Authorization' => 'Bearer ' . $this->accessTokenQuery->execute(),
                    'Content-Type' => 'application/json',
                    'Origin' => $this->url->getBaseUrl(),
                    'Referer' => $this->url->getBaseUrl()
                ]
            )
            ->build();
    }
}
