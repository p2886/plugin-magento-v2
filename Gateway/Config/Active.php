<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Gateway\Config;

use Magento\Payment\Gateway\ConfigInterface;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\ObjectManagerInterface;

class Active implements ConfigInterface
{
    const PATH_PATTERN = 'payment/%s/%s';

    /**
     * @var string|null
     */
    private $methodCode;

    /**
     * @var string|null
     */
    private $pathPattern;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @param ConfigHelper $configHelper
     * @param CheckoutSession $checkoutSession
     * @param ObjectManagerInterface $objectManager
     * @param string|null $methodCode
     * @param string $pathPattern
     */
    public function __construct(
        ConfigHelper $configHelper,
        CheckoutSession $checkoutSession,
        ObjectManagerInterface $objectManager,
        $methodCode = null,
        $pathPattern = self::PATH_PATTERN
    ) {
        $this->methodCode = $methodCode;
        $this->pathPattern = $pathPattern;
        $this->configHelper = $configHelper;
        $this->checkoutSession = $checkoutSession;
        $this->objectManager = $objectManager;
    }

    /**
     * @inheritDoc
     */
    public function getValue($field, $storeId = null)
    {
        if ($this->methodCode === null || $this->pathPattern === null) {
            return null;
        }

        if ($this->methodCode == 'peachpayments_embedded_checkout') {
            return $this->configHelper->isEmbeddedCheckoutEnabled($storeId);
        }

        if ($this->configHelper->isOnlyForSubscription() && !$this->isSubscriptionProductsInQuote()) {
            return false;
        }

        return $this->configHelper->isServerToServerEnabled($storeId);
    }

    /**
     * @inheritDoc
     */
    public function setMethodCode($methodCode)
    {
        $this->methodCode = $methodCode;
    }

    /**
     * @inheritDoc
     */
    public function setPathPattern($pathPattern)
    {
        $this->pathPattern = $pathPattern;
    }

    /**
     * @return bool
     */
    private function isSubscriptionProductsInQuote()
    {
        try {
            $subsModuleEnabled = $this->configHelper->isModuleOutputEnabled('ParadoxLabs_Subscriptions');
            $quote = $this->checkoutSession->getQuote();
            if ($subsModuleEnabled) {
                $quoteManager = $this->objectManager->get('ParadoxLabs\Subscriptions\Model\Service\QuoteManager');
                if ($quoteManager->quoteContainsSubscription($quote)) {
                    return true;
                }
            }
        } catch (\Exception $exception) {
            return false;
        }

        return false;
    }
}
