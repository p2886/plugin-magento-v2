<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Model\System\Config\Source;

use PeachPayments\Hosted\Helper\Config;

class PaymentBrands
{
    /**
     * Get payment brands
     *
     * @return \string[][]
     */
    public function toOptionArray()
    {
        $options = [];
        foreach (Config::CC_BRANDS as $code => $brand) {
            $options[] = ['value' => $code, "label" => $brand['label']];
        }

        return $options;
    }
}
