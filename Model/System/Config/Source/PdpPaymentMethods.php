<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y.io (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Model\System\Config\Source;

class PdpPaymentMethods
{
    /**
     * Peach payment option would be rendered when user purchases directly from the product page.
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => 'peachpayments_embedded_checkout', 'label' => __('Embedded Checkout')],
            ['value' => 'peachpayments_server_to_server', 'label' => __('Server To Server')],
            ['value' => 'peachpayments_hosted', 'label' => __('Hosted Payments')],
        ];
    }
}
