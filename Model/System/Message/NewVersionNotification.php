<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Model\System\Message;

use Magento\Framework\Notification\MessageInterface;
use Magento\Framework\Phrase;

class NewVersionNotification implements MessageInterface
{
    public const PEACHPAYMENT_URL = 'https://packagist.org/packages/peachpayments/magento2-plugin';
    public const MESSAGE_IDENTITY = 'version_system_notification';

    /**
     * @var VersionManager
     */
    private $versionManager;

    /**
     * @param VersionManager $versionManager
     */
    public function __construct(
        VersionManager $versionManager
    ) {
        $this->versionManager = $versionManager;
    }

    /**
     * @return string
     */
    public function getIdentity(): string
    {
        return self::MESSAGE_IDENTITY;
    }

    /**
     * @return bool
     */
    public function isDisplayed(): bool
    {
        $currentVersion = $this->getCurrentVersion();
        $highestVersion = $this->versionManager->getHighestVersion();

        if ($currentVersion && $highestVersion && version_compare($highestVersion, $currentVersion, '>')) {
            return true;
        }

        return false;
    }

    /**
     * @return string|null
     */
    private function getCurrentVersion(): ?string
    {
        $data = $this->versionManager->getModuleInfo('PeachPayments_Hosted');
        return $data['version'] ?? null;
    }

    /**
     * @return Phrase
     */
    public function getText()
    {
        $availableVersion = $this->versionManager->getHighestVersion();

        return __(
            '<a href="%1">Peach Payments</a> module version %2 is available.' .
            'Please update at your earliest convenience.',
            self::PEACHPAYMENT_URL,
            $availableVersion
        );
    }

    /**
     * @return int
     */
    public function getSeverity()
    {
        return self::SEVERITY_MAJOR;
    }
}
