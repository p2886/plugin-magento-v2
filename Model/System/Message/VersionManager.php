<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Model\System\Message;

use Exception;
use Laminas\Http\Request;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\HTTP\ClientFactory;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Serialize\SerializerInterface;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use Psr\Log\LoggerInterface;

class VersionManager
{
    public const PACKAGE_URI = 'https://packagist.org/packages/peachpayments/magento2-plugin.json';
    public const LAST_VERSION_CACHE_ID = 'peachpayment_lastversion';

    /**
     * @var array
     */
    protected $moduleDataStorage = [];

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var ClientFactory
     */
    private $client;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var Reader
     */
    private $moduleReader;

    /**
     * @var File
     */
    private $filesystem;

    /**
     * @param CacheInterface $cache
     * @param ClientFactory $client
     * @param SerializerInterface $serializer
     * @param Reader $moduleReader
     * @param File $filesystem
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        CacheInterface $cache,
        ClientFactory $client,
        SerializerInterface $serializer,
        Reader $moduleReader,
        File $filesystem,
        private UnifiedLogger $unifiedLogger
    ) {
        $this->cache = $cache;
        $this->client = $client;
        $this->serializer = $serializer;
        $this->moduleReader = $moduleReader;
        $this->filesystem = $filesystem;
    }

    /**
     * @return array
     */
    public function getModulePackage(): array
    {
        /** @var \Magento\Framework\HTTP\Client\Curl $client */
        $client = $this->client->create();
        $result = [];

        try {
            $client->get(self::PACKAGE_URI);
            $result = $this->serializer->unserialize($client->getBody());
        } catch (Exception $e) {
            $this->unifiedLogger->error(
                __(
                    'Couldn\'t get package data, Package_uri: %1, Error: %2',
                    self::PACKAGE_URI,
                    $e->getMessage()
                )
            );
        }

        return $result;
    }

    /**
     * @return string|null
     */
    public function getHighestVersion(): ?string
    {
        $cache = $this->cache->load(self::LAST_VERSION_CACHE_ID);
        if ($cache) {
            return $cache;
        }

        $lastVersion = $this->getLastVersion();
        $this->cache->save($lastVersion, self::LAST_VERSION_CACHE_ID, [], 86400);

        return $lastVersion;
    }

    /**
     * @param string $moduleCode
     * @return array
     */
    public function getModuleInfo(string $moduleCode): array
    {
        if (isset($this->moduleDataStorage[$moduleCode])) {
            return $this->moduleDataStorage[$moduleCode];
        }

        try {
            $dir = $this->moduleReader->getModuleDir('', $moduleCode);
            $file = "$dir/composer.json";

            $string = $this->filesystem->fileGetContents($file);
            $this->moduleDataStorage[$moduleCode] = $this->serializer->unserialize($string);
        } catch (FileSystemException $e) {
            $this->moduleDataStorage[$moduleCode] = [];
        }

        return $this->moduleDataStorage[$moduleCode];
    }

    /**
     * @return string|null
     */
    private function getLastVersion(): ?string
    {
        $modulePackage = $this->getModulePackage();
        if (!isset($modulePackage['package']['versions'])) {
            return null;
        }
        $versions = preg_grep('/^\d+\.\d+\.\d$/', array_keys($modulePackage['package']['versions']));

        $lastVersion = '0.0.0';
        foreach ($versions as $version) {
            if (version_compare($version, $lastVersion, '>')) {
                $lastVersion = $version;
            }
        }

        return $lastVersion;
    }
}
