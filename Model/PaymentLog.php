<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Model;

use Magento\Framework\Model\AbstractModel;
use PeachPayments\Hosted\Model\ResourceModel\PaymentLog as ResourceModel;

/**
 * @method int         getEntityId()
 * @method self        setEntityId(int $entityId)
 * @method string      getMessage()
 * @method self        setMessage(string $message)
 * @method string      getCreatedAt()
 * @method self        setCreatedAt(string $createdAt)
 */
class PaymentLog extends AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'peachpayments_hosted_payment_log_model';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
