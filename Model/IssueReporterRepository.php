<?php

namespace PeachPayments\Hosted\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use PeachPayments\Hosted\Api\Data\IssueReporterInterface;
use PeachPayments\Hosted\Api\Data\IssueReporterInterfaceFactory;
use PeachPayments\Hosted\Api\IssueReporterRepositoryInterface;
use PeachPayments\Hosted\Model\ResourceModel\IssueReporter as IssueReporterResourceModel;
use PeachPayments\Hosted\Model\ResourceModel\IssueReporter\Collection as Collection;
use PeachPayments\Hosted\Model\ResourceModel\IssueReporter\CollectionFactory as CollectionFactory;

class IssueReporterRepository implements IssueReporterRepositoryInterface
{
    /**
     * @var IssueReporterResourceModel
     */
    private $issueReporterResource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var IssueReporterInterfaceFactory
     */
    private $issueReporterFactory;

    /**
     * @param IssueReporterResourceModel $issueReporterResource
     * @param CollectionFactory $collectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     * @param IssueReporterInterfaceFactory $issueReporterFactory
     */
    public function __construct(
        IssueReporterResourceModel $issueReporterResource,
        CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        SearchResultsInterfaceFactory $searchResultsFactory,
        IssueReporterInterfaceFactory $issueReporterFactory
    ) {
        $this->issueReporterResource = $issueReporterResource;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->issueReporterFactory = $issueReporterFactory;
    }

    /**
     * @inheritDoc
     */
    public function save(IssueReporterInterface $issueReporter): IssueReporterInterface
    {
        try {
            $this->issueReporterResource->save($issueReporter);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __('Could not save product configuration: %1', $e->getMessage())
            );
        }

        return $issueReporter;
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function getById(int $entityId): IssueReporterInterface
    {
        /** @var IssueReporterInterface $issueReporter */
        $issueReporter = $this->issueReporterFactory->create();
        $this->issueReporterResource->load($issueReporter, $entityId);
        if (!$issueReporter->getEntityId()) {
            throw new NoSuchEntityException(
                __(
                    'Issue Reporter doesn\'t exist with id = `%1`.',
                    $entityId
                )
            );
        }

        return $issueReporter;
    }
}
