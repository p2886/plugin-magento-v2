<?php

/**
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

namespace PeachPayments\Hosted\Model\Method;

class BlinkByEmtel extends Hosted
{
    protected $_code = 'peachpayments_hosted_blinkbyemtel';
}
