<?php
/**
 * Copyright (c) 2020 Peach Payments. All rights reserved.
 */

namespace PeachPayments\Hosted\Model\Method;

/**
 * Class \PeachPayments\Hosted\Model\Method\HostedDefault
 *
 * This payment Method is used for PWA.
 *
 */
class HostedDefault extends Hosted
{
    /**
     * @var string
     */
    protected $_code = 'peachpayments_hosted_default';

    /**
     * @return false
     */
    public function canUseCheckout(): bool
    {
        return true;
    }
}
