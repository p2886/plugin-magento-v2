<?php
/**
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

/**
 * Class \PeachPayments\Hosted\Model\Method\APlus
 */
namespace PeachPayments\Hosted\Model\Method;

/**
 * Class APlus
 */
class APlus extends Hosted
{
    protected $_code = 'peachpayments_hosted_aplus';
}
