<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

/**
 * Class \PeachPayments\Hosted\Model\Method\ApplePay
 */
namespace PeachPayments\Hosted\Model\Method;

class ApplePay extends Hosted
{
    /**
     * ApplePay Payment code.
     */
    const PAYMENT_CODE = 'APPLEPAY';

    /**
     * @var string
     */
    protected $_code = 'peachpayments_hosted_applepay';
}
