<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

/**
 * Class \PeachPayments\Hosted\Model\Method\StitchEFT
 */
namespace PeachPayments\Hosted\Model\Method;

class StitchEFT extends Hosted
{
    protected $_code = 'peachpayments_hosted_stitcheft';
}
