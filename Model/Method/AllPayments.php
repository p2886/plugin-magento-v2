<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

/**
 * Class \PeachPayments\Hosted\Model\Method\AllPayments
 */
namespace PeachPayments\Hosted\Model\Method;

use Magento\Store\Model\ScopeInterface;

class AllPayments extends Hosted
{
    /**
     * Payment code.
     */
    const PAYMENT_CODE = 'ALLPAYMENTS';

    protected $_code = 'peachpayments_hosted_allpayments';

    /**
     * @return bool
     */
    public function canUseCheckout(): bool
    {
        $enabled = $this->scopeConfig->isSetFlag(
            'payment/peachpayments_hosted/enable_redirect_payments',
            ScopeInterface::SCOPE_STORE,
            $this->getStore()
        );

        $isConsolidatedPayments = $this->scopeConfig->isSetFlag(
            'payment/peachpayments_hosted/enable_consolidated_payments',
            ScopeInterface::SCOPE_STORE,
            $this->getStore()
        );

        return ($enabled && $isConsolidatedPayments);
    }
}
