<?php

namespace PeachPayments\Hosted\Model\ResourceModel\IssueReporter;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use PeachPayments\Hosted\Model\IssueReporter as Model;
use PeachPayments\Hosted\Model\ResourceModel\IssueReporter as ResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'peachpayments_hosted_help_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
