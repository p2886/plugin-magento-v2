<?php

namespace PeachPayments\Hosted\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class IssueReporter extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'peachpayments_hosted_help_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('peachpayments_hosted_help', 'entity_id');
        $this->_useIsObjectNew = true;
    }
}
