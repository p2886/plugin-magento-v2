<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class PaymentLog extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'peachpayments_hosted_payment_log_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('peachpayments_hosted_logs', 'entity_id');
        $this->_useIsObjectNew = true;
    }
}
