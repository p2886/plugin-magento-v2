<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Model\ResourceModel\PaymentLog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use PeachPayments\Hosted\Model\PaymentLog as Model;
use PeachPayments\Hosted\Model\ResourceModel\PaymentLog as ResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'peachpayments_hosted_payment_log_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
