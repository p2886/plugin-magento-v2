<?php

namespace PeachPayments\Hosted\Model;

use Magento\Framework\Model\AbstractModel;
use PeachPayments\Hosted\Api\Data\IssueReporterInterface;
use PeachPayments\Hosted\Model\ResourceModel\IssueReporter as ResourceModel;

class IssueReporter extends AbstractModel implements IssueReporterInterface
{
    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return (string) $this->getData(self::DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function getCopyTo(): string
    {
        return (string) $this->getData(self::COPY_TO);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): ?string
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function getDateSent(): ?string
    {
        return $this->getData(self::DATE_SENT);
    }

    /**
     * @inheritDoc
     */
    public function setId($value)
    {
        return $this->setData(self::ENTITY_ID, $value);
    }

    /**
     * @inheritDoc
     */
    public function setDescription(?string $description): IssueReporterInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @inheritDoc
     */
    public function setCopyTo(string $copyTo): IssueReporterInterface
    {
        return $this->setData(self::COPY_TO, $copyTo);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt(?string $createdAt): IssueReporterInterface
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function setDateSent(?string $dateSent): IssueReporterInterface
    {
        return $this->setData(self::DATE_SENT, $dateSent);
    }
}
