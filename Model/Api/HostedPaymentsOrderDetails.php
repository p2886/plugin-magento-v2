<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

namespace PeachPayments\Hosted\Model\Api;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use PeachPayments\Hosted\Api\HostedPaymentsOrderDetailsInterface;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;
use PeachPayments\Hosted\Service\FormPostCheckout;

class HostedPaymentsOrderDetails implements HostedPaymentsOrderDetailsInterface
{
    /**
     * @var JsonSerializer
     */
    private JsonSerializer $json;
    /**
     * @var WebhooksFactory
     */
    private WebhooksFactory $webhookFactory;
    /**
     * @var WebhooksResource
     */
    private WebhooksResource $webhookResource;
    /**
     * @var CheckoutSession
     */
    private CheckoutSession $checkoutSession;
    /**
     * @var FormPostCheckout
     */
    private FormPostCheckout $formPostCheckout;

    /**
     * @param JsonSerializer $json
     * @param WebhooksResource $webhookResource
     * @param WebhooksFactory $webhookFactory
     * @param CheckoutSession $checkoutSession
     * @param FormPostCheckout $formPostCheckout
     */
    public function __construct(
        JsonSerializer   $json,
        WebhooksResource $webhookResource,
        WebhooksFactory  $webhookFactory,
        CheckoutSession  $checkoutSession,
        FormPostCheckout $formPostCheckout
    )
    {
        $this->json = $json;
        $this->webhookFactory = $webhookFactory;
        $this->webhookResource = $webhookResource;
        $this->checkoutSession = $checkoutSession;

        $this->formPostCheckout = $formPostCheckout;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(): string
    {
        $this->initWebHook();
        try {
            return $this->json->serialize($this->formPostCheckout->getFormData());
        } catch (\Exception | \InvalidArgumentException $e) {
            return $this->json->serialize(['error' => $e->getMessage()]);
        }
    }

    /**
     * @return void
     */
    private function initWebHook(): void
    {
        try {
            $orderId = $this->checkoutSession->getData('last_order_id');
            $orderIncrementId = $this->checkoutSession->getData('last_real_order_id');
            $webHook = $this->webhookFactory->create();

            $this->webhookResource->load($webHook, $orderId, 'order_id');

            if (!$webHook->getId()) {
                $webHook->addData([
                    'order_id' => $orderId,
                    'order_increment_id' => $orderIncrementId
                ]);
            } else {
                $webHook->setData('order_id', $orderId);
                $webHook->setData('order_increment_id', $orderIncrementId);
            }

            $this->webhookResource->save($webHook);
        } catch (\Exception $e) {
        }
    }
}
