<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Model\Api;

use Laminas\Http\Request;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use PeachPayments\Hosted\Api\AvailableHostedPaymentMethodsTitleInterface;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Gateway\Request\PaymentDataBuilder;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as DataHelper;
use GuzzleHttp\ClientFactory;
use PeachPayments\Hosted\Query\GetAccessToken as AccessTokenQuery;

class AvailableHostedPaymentMethodsTitle implements AvailableHostedPaymentMethodsTitleInterface
{
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var JsonSerializer
     */
    private $json;
    /**
     * @var HttpTransferObject
     */
    private $httpTransferObject;
    /**
     * @var DataHelper
     */
    private $dataHelper;
    /**
     * @var ClientFactory
     */
    private $clientFactory;
    /**
     * @var AccessTokenQuery
     */
    private $accessTokenQuery;

    /**
     * @param ConfigHelper $configHelper
     * @param JsonSerializer $json
     * @param HttpTransferObject $httpTransferObject
     * @param DataHelper $dataHelper
     * @param ClientFactory $clientFactory
     * @param AccessTokenQuery $accessTokenQuery
     */
    public function __construct(
        ConfigHelper $configHelper,
        JsonSerializer $json,
        HttpTransferObject $httpTransferObject,
        DataHelper $dataHelper,
        ClientFactory $clientFactory,
        AccessTokenQuery $accessTokenQuery
    )
    {
        $this->configHelper = $configHelper;
        $this->json = $json;
        $this->httpTransferObject = $httpTransferObject;
        $this->dataHelper = $dataHelper;
        $this->clientFactory = $clientFactory;
        $this->accessTokenQuery = $accessTokenQuery;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(): string
    {
        try {
            /** @var \GuzzleHttp\Client $client */
            $client = $this->clientFactory->create();
            $result = $client->request(
                Request::METHOD_POST,
                $this->configHelper->getAvailablePaymentMethodsApiUrl(),
                [
                    'body' => $this->json->serialize(
                        $this->dataHelper->signData([
                            'authentication.entityId' => $this->configHelper->getEntityId3DSecure(),
                            PaymentDataBuilder::CURRENCY => 'ZAR',
                        ], false)
                    ),
                    'headers' => [
                        'content-type' => 'application/json',
                        'Authorization' => 'Bearer ' . $this->accessTokenQuery->execute(),
                        CURLOPT_SSL_VERIFYPEER => $this->configHelper->isLiveMode(),
                        CURLOPT_RETURNTRANSFER => true
                    ]
                ]
            );

            $contents = $result->getBody()->getContents();
            if (strlen($contents) > 0) {
                return $contents;
            }

            return $this->json->serialize([]);
        } catch (\Exception $e) {
            return $this->json->serialize([]);
        }
    }
}
