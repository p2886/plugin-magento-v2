<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2023 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Model\Api;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface as QuoteMaskToQuoteId;
use PeachPayments\Hosted\Command\GetVaultGatewayTokensForCart as RegistrationIdsCommand;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use PeachPayments\Hosted\Gateway\Request\AuthDataBuilder;
use PeachPayments\Hosted\Gateway\Request\PaymentDataBuilder;
use PeachPayments\Hosted\Gateway\Response\AuthorizationTrxIdHandler;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as DataHelper;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\ResourceModel\Web\Hooks as WebhooksResource;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;
use PeachPayments\Hosted\Query\GetSubscriptionExpiryAndFrequency;

abstract class InitiateCheckoutAbstract
{
    /**
     * @var ConfigHelper
     */
    protected $configHelper;
    /**
     * @var QuoteMaskToQuoteId
     */
    private $maskToQuoteId;
    /**
     * @var CustomerSession
     */
    protected $customerSession;
    /**
     * @var JsonSerializer
     */
    protected $jsonSerializer;
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;
    /**
     * @var HttpTransferObject
     */
    private $httpTransferObject;
    /**
     * @var CurlClient
     */
    private $curlClient;
    /**
     * @var CartInterface
     */
    protected $cart;
    /**
     * @var RemoteAddress
     */
    private $remoteAddress;
    /**
     * @var WebhooksResource
     */
    private $webhookResource;
    /**
     * @var WebhooksFactory
     */
    private $webhookFactory;
    /**
     * @var RegistrationIdsCommand
     */
    protected $registrationIdsCommand;
    /**
     * @var DataHelper
     */
    protected $dataHelper;
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;
    /**
     * @var GetSubscriptionExpiryAndFrequency
     */
    protected $expiryAndFrequencyQeury;

    /**
     * @var CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * @param ConfigHelper $configHelper
     * @param QuoteMaskToQuoteId $maskToQuoteId
     * @param CustomerSession $customerSession
     * @param JsonSerializer $jsonSerializer
     * @param CartRepositoryInterface $cartRepository
     * @param HttpTransferObject $httpTransferObject
     * @param CurlClient $curlClient
     * @param RemoteAddress $remoteAddress
     * @param WebhooksResource $webhookResource
     * @param WebhooksFactory $webhookFactory
     * @param RegistrationIdsCommand $registrationIdsCommand
     * @param DataHelper $dataHelper
     * @param UrlInterface $urlBuilder
     * @param GetSubscriptionExpiryAndFrequency $expiryAndFrequencyQeury
     * @param CookieManagerInterface $cookieManager
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        ConfigHelper $configHelper,
        QuoteMaskToQuoteId $maskToQuoteId,
        CustomerSession $customerSession,
        JsonSerializer $jsonSerializer,
        CartRepositoryInterface $cartRepository,
        HttpTransferObject $httpTransferObject,
        CurlClient $curlClient,
        RemoteAddress $remoteAddress,
        WebhooksResource $webhookResource,
        WebhooksFactory $webhookFactory,
        RegistrationIdsCommand $registrationIdsCommand,
        DataHelper $dataHelper,
        UrlInterface $urlBuilder,
        GetSubscriptionExpiryAndFrequency $expiryAndFrequencyQeury,
        CookieManagerInterface $cookieManager,
        protected UnifiedLogger $unifiedLogger
    ) {
        $this->configHelper = $configHelper;
        $this->maskToQuoteId = $maskToQuoteId;
        $this->customerSession = $customerSession;
        $this->jsonSerializer = $jsonSerializer;
        $this->cartRepository = $cartRepository;
        $this->httpTransferObject = $httpTransferObject;
        $this->curlClient = $curlClient;
        $this->remoteAddress = $remoteAddress;
        $this->webhookResource = $webhookResource;
        $this->webhookFactory = $webhookFactory;
        $this->registrationIdsCommand = $registrationIdsCommand;
        $this->dataHelper = $dataHelper;
        $this->urlBuilder = $urlBuilder;
        $this->expiryAndFrequencyQeury = $expiryAndFrequencyQeury;
        $this->cookieManager = $cookieManager;
    }

    public function prepareRequest(string $cartId): array
    {
        if (!$this->customerSession->isLoggedIn()) {
            $cartId = $this->maskToQuoteId->execute($cartId);
        }

        $this->cart = $this->cartRepository->get($cartId);
        $amount = number_format((float) $this->cart->getGrandTotal(), 2, '.', '');
        $entityId3DSecure = $this->configHelper->getEntityId3DSecure();

        return [
            AuthDataBuilder::ID => $entityId3DSecure,
            PaymentDataBuilder::AMOUNT => $amount,
            PaymentDataBuilder::CURRENCY => $this->cart->getCurrency()->getQuoteCurrencyCode(),
            'merchantTransactionId' => $this->cart->reserveOrderId()->getReservedOrderId()
        ];
    }

    public function makeRequestV2(string $url, array $data): array
    {
        $this->setTrackingValue($data);
        $transfer = $this->httpTransferObject->createV2($url, 'POST', $data);
        return  $this->makeRequest($transfer);
    }

    public function makeRequestV1(string $url, array $data): array
    {
        $this->setTrackingValue($data);
        $transfer = $this->httpTransferObject->create($url, 'POST', $data);
        return  $this->makeRequest($transfer);
    }

    private function saveWebhook(string $checkoutId): void
    {
        try {
            $webhook = $this->webhookFactory->create();
            $this->webhookResource->load($webhook, $checkoutId, 'checkout_id');
            if (!$webhook->getId()) {
                $webhook->setData('checkout_id', $checkoutId);
                $this->webhookResource->save($webhook);
            }
        } catch (\Exception $e) {
        }
    }

    private function makeRequest(\Magento\Payment\Gateway\Http\TransferInterface $transfer): array
    {
        $response = $this->curlClient->placeRequest($transfer);
        $result['checkout_id'] = $response['id'] ?? $response['checkoutId'] ?? '';
        $this->cart->getPayment()->setAdditionalInformation(
            AuthorizationTrxIdHandler::KEY_TNX_ID,
            $result['checkout_id']
        );
        $this->cartRepository->save($this->cart);
        $this->saveWebhook($result['checkout_id']);

        return array_merge(
            [
                'merchantTransactionId' => $this->cart->getReservedOrderId(),
                'customParameters[PAYMENT_PLUGIN]' => 'MAGENTO',
                'customer.ip' => $this->remoteAddress->getRemoteAddress(),
                AuthDataBuilder::ID => $this->configHelper->getEntityId3DSecure(),
                PaymentDataBuilder::AMOUNT => number_format($this->cart->getGrandTotal(), 2, '.', ''),
                PaymentDataBuilder::CURRENCY => $this->cart->getCurrency()->getQuoteCurrencyCode(),
                PaymentDataBuilder::PAYMENT_TYPE => 'DB',
            ],
            $result
        );
    }

    /**
     * @param array $data
     * @return void
     */
    private function setTrackingValue(array &$data): void
    {
        $cookies = $this->cookieManager->getCookie('pay-with-peach-payment');
        if (!$cookies) {
            return;
        }

        $cookiesData = $this->jsonSerializer->unserialize($cookies);
        if ($cookiesData['tracking']) {
            $data['customParameters'] = ['tracking' => $cookiesData['tracking']];
        }
    }
}
