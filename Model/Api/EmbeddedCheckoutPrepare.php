<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2023 X2Y.io (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Model\Api;

use PeachPayments\Hosted\Api\EmbeddedCheckoutPrepareInterface;
use PeachPayments\Hosted\Gateway\Request\AuthDataBuilder;
use PeachPayments\Hosted\Gateway\Request\PaymentDataBuilder;

class EmbeddedCheckoutPrepare extends InitiateCheckoutAbstract implements EmbeddedCheckoutPrepareInterface
{
    public function execute(string $cartId): string
    {
        if (!$this->configHelper->isEmbeddedCheckoutEnabled()) {
            return $this->jsonSerializer->serialize([]);
        }
//        TODO: Add logger and log errors
        try {
            $requestData = $this->prepareRequest($cartId);
            $requestData['authentication'] = [
                AuthDataBuilder::ID => $requestData[AuthDataBuilder::ID]
            ];
            $requestData['shopperResultUrl'] = $this->urlBuilder->getUrl('checkout');
            $requestData['forceDefaultMethod'] = false;
            $requestData[PaymentDataBuilder::AMOUNT] = (float) $requestData[PaymentDataBuilder::AMOUNT];

            unset($requestData[AuthDataBuilder::ID]);
            $requestData['nonce'] = $this->dataHelper->getUuid();

            $response = $this->makeRequestV2(
                $this->configHelper->getCheckoutUrlV2(),
                $requestData
            );

            return $this->jsonSerializer->serialize($response);
        } catch (\Exception $e) {
            return $this->jsonSerializer->serialize([]);
        }
    }
}
