<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Model\Api;

use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use PeachPayments\Hosted\Gateway\Request\AuthDataBuilder;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHelper;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;

/**
 * @spi
 */
class CopyAndPayCheckPaymentStatus
{
    /**
     * @var PeachPaymentsHelper
     */
    private $dataHelper;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var CurlClient
     */
    private $curlClient;
    /**
     * @var HttpTransferObject
     */
    private $httpTransferObject;

    /**
     * @param PeachPaymentsHelper $dataHelper
     * @param ConfigHelper $configHelper
     * @param CurlClient $curlClient
     * @param HttpTransferObject $httpTransferObject
     */
    public function __construct(
        PeachPaymentsHelper $dataHelper,
        ConfigHelper $configHelper,
        CurlClient $curlClient,
        HttpTransferObject $httpTransferObject
    ) {
        $this->dataHelper = $dataHelper;
        $this->configHelper = $configHelper;
        $this->curlClient = $curlClient;
        $this->httpTransferObject = $httpTransferObject;
    }

    public function execute(string $paymentId)
    {
        $uri = $this->configHelper->getCheckoutsUri($paymentId) .
             '?' . AuthDataBuilder::ID . '=' . $this->configHelper->getEntityId3DSecure();
        $transfer = $this->httpTransferObject->create($uri, 'GET');
        return $this->curlClient->placeRequest($transfer);
    }
}
