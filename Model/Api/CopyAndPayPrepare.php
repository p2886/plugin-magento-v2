<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Model\Api;

use Magento\Framework\App\ObjectManager;
use Magento\Quote\Api\Data\CartInterface;
use PeachPayments\Hosted\Api\CopyAndPayPrepareInterface;
use PeachPayments\Hosted\Gateway\Request\PaymentDataBuilder;
use PeachPayments\Hosted\Gateway\Request\VaultDataBuilder;
use PeachPayments\Hosted\Model\Web\HooksFactory as WebhooksFactory;

/**
 * Prepare copy and pay checkout
 */
class CopyAndPayPrepare extends InitiateCheckoutAbstract implements CopyAndPayPrepareInterface
{
    const REQUEST_SAVE_CARD_DETAILS = 'createRegistration';

    /**
     * @inheritDoc
     */
    public function execute(string $cartId): string
    {
        if (!$this->configHelper->isServerToServerEnabled()) {
            return $this->jsonSerializer->serialize([]);
        }

        try {
            $requestData = $this->prepareRequest($cartId);

            $requestData[PaymentDataBuilder::PAYMENT_TYPE] = 'DB';
            $requestData[VaultDataBuilder::MODE] = 'INITIAL';
            $requestData[VaultDataBuilder::TYPE] = 'UNSCHEDULED';
            $requestData[VaultDataBuilder::SOURCE] = 'CIT';

            if ($this->cartHasSubscriptions($this->cart)) {
                list($expiry, $frequency) = $this->expiryAndFrequencyQeury->execute($this->cart);
                $requestData[self::REQUEST_SAVE_CARD_DETAILS] = true;
                $requestData[VaultDataBuilder::MODE] = 'INITIAL';
                $requestData[VaultDataBuilder::TYPE] = 'RECURRING';
                $requestData[VaultDataBuilder::SOURCE] = 'CIT';
                $requestData[VaultDataBuilder::EXPIRY] = $expiry;
                $requestData[VaultDataBuilder::FREQUENCY] = $frequency;
            }

            $requestData = $this->getRegistrationIdsRequestParams($this->cart, $requestData);
            $this->unifiedLogger->debug($requestData);

            $response = $this->makeRequestV1($this->configHelper->getCheckoutsUri(), $requestData);
            $this->unifiedLogger->debug($response);

            return $this->jsonSerializer->serialize($response);
        } catch (\Exception $e) {
            return $this->jsonSerializer->serialize([]);
        }
    }

    /**
     * @param $cart CartInterface
     * @return bool
     */
    private function cartHasSubscriptions(CartInterface $cart): bool
    {
        try {
            $subsModuleEnabled = $this->configHelper->isModuleOutputEnabled('ParadoxLabs_Subscriptions');
            if ($subsModuleEnabled) {
                $quoteManager = ObjectManager::getInstance()->get('ParadoxLabs\Subscriptions\Model\Service\QuoteManager');
                if ($quoteManager->quoteContainsSubscription($cart)) {
                    return true;
                }
            }
        } catch (\Exception $exception) {
            return false;
        }

        return false;
    }

    private function getRegistrationIdsRequestParams(CartInterface $cart, array $requestData): array
    {
        $registrationIds = $this->registrationIdsCommand->execute($cart);
        if (empty($registrationIds)) {
            return $requestData;
        }

        foreach ($registrationIds as $key => $registrationId) {
            $requestData["registrations[$key].id"] = $registrationId;
        }

        $requestData[VaultDataBuilder::MODE] = 'REPEATED';
        $requestData[VaultDataBuilder::TYPE] = 'UNSCHEDULED';
        $requestData[VaultDataBuilder::SOURCE] = 'CIT';

        return $requestData;
    }
}
