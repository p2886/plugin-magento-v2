<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Gateway\ConfigInterface;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;

class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'peachpayments_server_to_server';

    const CODE_CC_VAULT = 'peachpayments_server_to_server_vault';

    /**
     * @var ConfigInterface
     */
    private $config;
    /**
     * @var ConfigHelper
     */
    private $helper;

    /**
     * @param ConfigInterface $config
     * @param ConfigHelper $helper
     */
    public function __construct(
        ConfigInterface $config,
        ConfigHelper $helper
    )
    {
        $this->config = $config;
        $this->helper = $helper;
    }

    /**
     * @inheritDoc
     */
    public function getConfig()
    {
        $availableCardTypes = $this->config->getValue('cctypes');
        $types = array_map(function ($code) {
            return [$code => ConfigHelper::CC_BRANDS[$code]['code']];
        }, explode(',', $availableCardTypes));

        $isLiveMode = $this->helper->isLiveMode();
        return [
            'payment' => [
                self::CODE => [
                    'isActive' => $this->helper->isServerToServerEnabled(),
                    'ccVaultCode' => self::CODE_CC_VAULT,
                    'availableTypes' => $types,
                    'isLive' => $isLiveMode
                ],
                'hosted' => [
                    'consolidatedPayments' => $this->helper->isConsolidatedPayments(),
                    'isLive' => $isLiveMode,
                    'checkoutUrl' => $this->helper->getBaseApiUrl() . 'checkout'
                ]
            ]
        ];
    }
}
