# Peach Payments Magento payment extension

All-in-one payment solution for African markets. For more information on the various functionality offered by the plugin, see the [Peach Payment documentation hub](https://developer.peachpayments.com/docs/magento).

## Install using Composer

1. Install **Composer** by following the instructions on [https://getcomposer.org/doc/00-intro.md](https://getcomposer.org/doc/00-intro.md).
2. Install the **Peach Payments Magento payment extension**:

   1. Install the plugin by executing `composer require peachpayments/magento2-plugin`.
   2. Enable the payment module by executing:

      ```sh
      php bin/magento module:enable PeachPayments_Hosted
      ```

      ```sh
      php bin/magento setup:upgrade
      ```

   3. If necessary, deploy Magento static content by executing `php bin/magento setup:static-content:deploy`

## Configure the payment extension

1. Log in to your Magento admin dashboard and navigate to **Stores** > **Configuration** > **Sales** > **Payment Methods**.
2. If the Peach Payments module is not visible in the list of available payment methods, go to **System** > **Cache Management** and click **Flush Magento Cache**.
3. Navigate back to **Stores** > **Configuration** > **Sales** > **Payment Methods** and click **Configure** next to the Peach Payments payment method to expand the available settings.
4. Set **Enabled** to **Yes**, configure the correct credentials, specify your preferred settings, and click **Save Config**.

## PHP compatibility

This payment extension supports the `7.3`, `7.4`, `8.1`, `8.2` PHP versions. PHP 7.1 and 7.2 have been deprecated, so please use version 1.0.7, which excludes GraphQL.

## Events

Two events are available to supplement 3rd-party tracking. On order success, use `peachpayments_order_succeed`. On order failure, use `peachpayments_order_failed`.

Both events have a `result` data object. Take special care to prevent duplicates in your observer. These events are dispatched on both the customer-facing and webhook controllers.

## GraphQL

Need to Install additional module `PeachPayments_HostedGraphQl`.

## Log File Permissions

Ensure that the peach_payments.log file in var/log has the following permissions:

>chmod 660 var/log/peach_payments.log

>chown www-data:www-data var/log/peach_payments.log

## Subscription support

To support subscriptions, install the [ParadoxLabs Adaptive Subscriptions plugin](https://marketplace.magento.com/paradoxlabs-subscriptions.html). Navigate to **Stores** > **Configuration** > **Sales** > **Payment Methods** > **Peach Payments** > **Server to Server** and enable the server to server integration.

### Compatability matrix

| Magento       | Peach Payments module | ParadoxLabs Subscription module |
|---------------|-----------------------|---------------------------------|
| 2.4.0 - 2.4.3 | Version: 1.2.4        | Version: 3.3.4                  |
| 2.4.3 - 2.4.4 | Version: 1.2.5        | Version: 3.5.2                  |
| 2.4.3 - 2.4.4 | Version: 1.2.6        | Version: 3.5.2                  |
| 2.4.3 - 2.4.4 | Version: 1.2.7        | Version: 3.5.2                  |
| 2.4.3 - 2.4.4 | Version: 1.2.8        | Version: 3.5.3                  |
| 2.4.3 - 2.4.4 | Version: 1.2.9        | Version: 3.5.3                  |
| 2.4.3 - 2.4.4 | Version: 1.3.0        | Version: 3.5.3                  |
| 2.4.3 - 2.4.4 | Version: 1.3.1        | Version: 3.6.0                  |
| 2.4.5 - 2.4.6 | Version: 1.3.2        | Version: 3.6.0                  |
| 2.4.5 - 2.4.6 | Version: 1.3.3        | Version: 3.6.0                  |
| 2.4.6 - 2.4.7 | Version: 1.3.4        | Version: 3.6.0                  |
| 2.4.6 - 2.4.7 | Version: 1.3.5        | Version: 3.6.0                  |
| 2.4.6 - 2.4.7 | Version: 1.3.6        | Version: 3.6.0                  |
| 2.4.6 - 2.4.7 | Version: 1.3.7        | Version: 3.6.0                  |
| 2.4.6 - 2.4.7 | Version: 1.3.8        | Version: 3.6.0                  |
| 2.4.6 - 2.4.7 | Version: 1.3.9        | Version: 3.6.0                  |
