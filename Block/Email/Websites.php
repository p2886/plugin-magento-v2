<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Block\Email;

use Magento\Framework\View\Element\Template;

class Websites extends Template
{
    /**
     * @var string
     */
    protected $_template = 'PeachPayments_Hosted::email/websites.phtml';
}
