<?php
/**
 * Get magento, Peach Payments plugin and Subscription module versions.
 *
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field as FormField;
use Magento\Framework\Data\Form\Element\AbstractElement;

class DashboardLinkButton extends FormField
{
    const BUTTON_TEMPLATE = 'system/config/button/dashboard-link-button.phtml';

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(static::BUTTON_TEMPLATE);
        }
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
