<?php
/**
 * Get magento, Peach Payments plugin and Subscription module versions.
 *
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2022 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Block\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field as FormField;
use Magento\Framework\Module\PackageInfo;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Magento\Framework\App\ProductMetadataInterface;

class Versions extends FormField
{
    /**
     * @var PackageInfo
     */
    private $packageInfo;

    /**
     * @var ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * @param Context $context
     * @param PackageInfo $packageInfo
     * @param ProductMetadataInterface $productMetadata
     * @param array $data
     * @param SecureHtmlRenderer|null $secureRenderer
     */
    public function __construct(
        Context $context,
        PackageInfo $packageInfo,
        ProductMetadataInterface $productMetadata,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        $this->packageInfo = $packageInfo;
        $this->productMetadata = $productMetadata;

        parent::__construct($context, $data, $secureRenderer);
    }

    /**
     * {@inheritDoc}
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $vMagento = $this->getVersionElementHtml(
            'v_magento',
            'Magento Version:',
            $this->productMetadata->getVersion()
        );
        $vPeach = $this->getVersionElementHtml(
            'v_peach',
            'Peach Payments Plugin Version:',
            $this->packageInfo->getVersion('PeachPayments_Hosted')
        );
        $vParadox = $this->getVersionElementHtml(
            'v_paradox',
            'ParadoxLabs Plugin Version:',
            $this->packageInfo->getVersion('ParadoxLabs_Subscriptions') ?: 'Not Installed'
        );

        return $vMagento . $vPeach . $vParadox;
    }

    /**
     * Get version html.
     *
     * @param string $class
     * @param string $label
     * @param string $version
     *
     * @return string
     */
    private function getVersionElementHtml(string $class, string $label, string $version): string
    {
        return sprintf(
            '<tr id="row_payment_us_peachpayments_hosted_%s">
<td class="label" style="padding: 0; font-weight: normal"><label for="payment_us_peachpayments_hosted_%s"><span>%s</span></label></td>
<td class="value" style="padding: 0 0 0 1rem;"><b>%s</b></td><td></td>
</tr>',
            $class,
            $class,
            $label,
            $version
        );
    }
}
