<?php
/**
 * @author     X2Y Development team <dev@x2y.io>
 * @copyright  2024 X2Y.io (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Block\Product\Shortcut;

use Magento\Catalog\Block\ShortcutInterface;
use Magento\Framework\View\Element\Template;

class Button extends Template implements ShortcutInterface
{
    const ALIAS_ELEMENT_INDEX = 'alias';

    const BUTTON_ELEMENT_INDEX = 'button_id';

    /**
     * @inheritdoc
     */
    public function getAlias(): string
    {
        return $this->getData(self::ALIAS_ELEMENT_INDEX);
    }

    /**
     * @return string
     */
    public function getContainerId(): string
    {
        return $this->getData(self::BUTTON_ELEMENT_INDEX);
    }
}
