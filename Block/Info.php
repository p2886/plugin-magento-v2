<?php
/**
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

/**
 * Class \PeachPayments\Hosted\Block\Info
 */
namespace PeachPayments\Hosted\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;

/**
 * Class Info
 * @package PeachPayments\Hosted\Block
 */
class Info extends \Magento\Payment\Block\Info
{
    protected $_template = 'PeachPayments_Hosted::info/default.phtml';
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;

    /**
     * @param JsonSerializer $jsonSerializer
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        JsonSerializer $jsonSerializer,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->jsonSerializer = $jsonSerializer;
    }

    public function getPeachRequest()
    {
        $peachRequest = $this->getInfo()->getAdditionalInformation('peach_request') ?: '{}';
        return $this->jsonSerializer->unserialize($peachRequest);
    }
}
