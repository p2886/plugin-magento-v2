<?php
/**
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

/**
 * Class \PeachPayments\Hosted\Controller\Secure\AbstractSecure
 */
namespace PeachPayments\Hosted\Controller\Secure;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\LayoutFactory;
use PeachPayments\Hosted\Model\Web\HooksFactory;

abstract class AbstractSecure extends Action
{
    /**
     * @var HooksFactory
     */
    protected $webHooksFactory;

    /**
     * @var LayoutFactory
     */
    protected $viewLayoutFactory;

    public function __construct(
        Context $context,
        HooksFactory $webHooksFactory,
        LayoutFactory $viewLayoutFactory
    ) {
        $this->webHooksFactory = $webHooksFactory;
        $this->viewLayoutFactory = $viewLayoutFactory;

        parent::__construct($context);
    }

    /**
     * @param string $resultCode
     * @return bool
     */
    protected function isSuccessful($resultCode): bool
    {
        return in_array($resultCode, ['000.000.000', '000.100.110', '000.100.111', '000.100.112']);
    }

    /**
     * @param string $resultCode
     * @return bool
     */
    protected function isWaiting($resultCode): bool
    {
        return in_array($resultCode, ['000.200.000', '000.200.100']);
    }

    /**
     * @return \PeachPayments\Hosted\Model\Web\Hooks
     */
    public function getWebHook()
    {
        return $this->webHooksFactory->create();
    }

    /**
     * @return \Magento\Checkout\Model\Session
     */
    protected function getSession()
    {
        return ObjectManager::getInstance()->get('Magento\Checkout\Model\Session');
    }

    /**
     * @param string $orderId
     * @param string $orderIncrementId
     */
    protected function getInternalRedirect($orderId, $orderIncrementId)
    {
        $block = $this->viewLayoutFactory->create()
            ->createBlock(
                'PeachPayments\Hosted\Block\Redirect',
                'redirect',
                ['order_id' => $orderId, 'order_increment_id' => $orderIncrementId]
            )->toHtml();

        $this->getResponse()
            ->setBody($block);
    }
}
