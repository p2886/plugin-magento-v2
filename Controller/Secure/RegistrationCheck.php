<?php

namespace PeachPayments\Hosted\Controller\Secure;

use Exception;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\Http as HttpResponse;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\UrlInterface;
use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use PeachPayments\Hosted\Gateway\Helper\HttpTransferObject;
use PeachPayments\Hosted\Gateway\Http\Client as CurlClient;
use PeachPayments\Hosted\Gateway\Request\AuthDataBuilder;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHelper;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\Ui\ConfigProvider;
use Magento\Vault\Model\ResourceModel\PaymentToken as PaymentTokenResource;

class RegistrationCheck implements HttpGetActionInterface
{
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var CurlClient
     */
    private $curlClient;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var PeachPaymentsHelper
     */
    private $peachPaymentsHelper;
    /**
     * @var PaymentTokenFactoryInterface
     */
    private $paymentTokenFactory;
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;
    /**
     * @var PaymentTokenRepositoryInterface
     */
    private $paymentTokenRepository;
    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var HttpResponse
     */
    private $httpResponse;
    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var EncryptorInterface
     */
    private $encryptor;
    /**
     * @var HttpTransferObject
     */
    private $httpTransferObject;
    /**
     * @var MessageManagerInterface
     */
    private $messageManager;
    /**
     * @var PaymentTokenResource
     */
    private $paymentTokenResource;

    /**
     * @param RequestInterface $request
     * @param CurlClient $curlClient
     * @param ConfigHelper $configHelper
     * @param PeachPaymentsHelper $peachPaymentsHelper
     * @param PaymentTokenFactoryInterface $paymentTokenFactory
     * @param JsonSerializer $jsonSerializer
     * @param PaymentTokenRepositoryInterface $paymentTokenRepository
     * @param UrlInterface $url
     * @param HttpResponse $httpResponse
     * @param CustomerSession $customerSession
     * @param EncryptorInterface $encryptor
     * @param HttpTransferObject $httpTransferObject
     * @param MessageManagerInterface $messageManager
     * @param PaymentTokenResource $paymentTokenResource
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        RequestInterface $request,
        CurlClient $curlClient,
        ConfigHelper $configHelper,
        PeachPaymentsHelper $peachPaymentsHelper,
        PaymentTokenFactoryInterface $paymentTokenFactory,
        JsonSerializer $jsonSerializer,
        PaymentTokenRepositoryInterface $paymentTokenRepository,
        UrlInterface $url,
        HttpResponse $httpResponse,
        CustomerSession $customerSession,
        EncryptorInterface $encryptor,
        HttpTransferObject $httpTransferObject,
        MessageManagerInterface $messageManager,
        PaymentTokenResource $paymentTokenResource,
        private UnifiedLogger $unifiedLogger
    ) {
        $this->request = $request;
        $this->curlClient = $curlClient;
        $this->configHelper = $configHelper;
        $this->peachPaymentsHelper = $peachPaymentsHelper;
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->jsonSerializer = $jsonSerializer;
        $this->paymentTokenRepository = $paymentTokenRepository;
        $this->url = $url;
        $this->httpResponse = $httpResponse;
        $this->customerSession = $customerSession;
        $this->encryptor = $encryptor;
        $this->httpTransferObject = $httpTransferObject;
        $this->messageManager = $messageManager;
        $this->paymentTokenResource = $paymentTokenResource;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function execute()
    {
        $id = $this->request->getParam("id");
        $uri = str_replace('/payment', '/registration', $this->configHelper->getCheckoutsUri($id)) .
            '?' . AuthDataBuilder::ID . '=' . $this->configHelper->getEntityId3DSecure();
        $transfer = $this->httpTransferObject->create($uri, 'GET');
        $registrations = $this->curlClient->placeRequest($transfer);
        $this->createVaultEntity($registrations);

        return $this->httpResponse->setRedirect($this->url->getUrl('vault/cards/listaction'));
    }

    /**
     * Create or update vault entity for card.
     *
     * @param array $response
     */
    private function createVaultEntity(array $response)
    {
        $paymentToken = $this->paymentTokenFactory->create(PaymentTokenFactoryInterface::TOKEN_TYPE_CREDIT_CARD);
        if (!array_key_exists('registrationId', $response)) {
            return;
        }

        $expirationDate = $this->peachPaymentsHelper->getCardExpirationDateForVault(
            $response['card']['expiryYear'],
            $response['card']['expiryMonth']
        );
        $paymentToken->setGatewayToken($response['registrationId']);
        $paymentToken->setExpiresAt($this->getTimestampExpAt($expirationDate));
        $paymentToken->setIsVisible(true);
        $paymentToken->setIsActive(true);
        $paymentToken->setPaymentMethodCode(ConfigProvider::CODE);
        $paymentToken->setCustomerId($this->customerSession->getCustomerId());
        $paymentToken->setTokenDetails($this->jsonSerializer->serialize([
            'type' => $this->configHelper->getPaymentBrandByPeachPaymentsCode($response['paymentBrand']),
            'maskedCC' => $response['card']['last4Digits'],
            'expirationDate' => $expirationDate
        ]));

        $hashKey = $paymentToken->getCustomerId()
            . $paymentToken->getPaymentMethodCode()
            . $paymentToken->getType()
            . $paymentToken->getTokenDetails();

        $paymentToken->setPublicHash($this->encryptor->getHash($hashKey));

        try {
            $successMessage = __("New card successfully added!");
            $token = $this->paymentTokenResource->getByPublicHash(
                $paymentToken->getPublicHash(),
                $paymentToken->getCustomerId()
            );

            if (isset($token['entity_id'])) {
                $paymentToken->setEntityId($token['entity_id']);

                if ($token['is_active'] && $token['is_visible']) {
                    $successMessage = __("Card has been updated!");
                }
            }

            $this->paymentTokenRepository->save($paymentToken);
            $this->messageManager->addSuccessMessage($successMessage);
        } catch (\Exception $exception) {
            $this->unifiedLogger->debug(
                [
                    'message' => "Error saving vault token when adding new Peach Payments card.",
                    'exception' => $exception->getMessage()
                ]
            );
            $this->messageManager->addErrorMessage(__("Something went wrong. Please try again later"));
        }
    }

    /**
     * Fix issue with max timestamp allowed date
     *
     * @param string $expirationDate
     * @return string
     */
    private function getTimestampExpAt(string $expirationDate): string
    {
        // TODO: remove in case `vault_payment_token.expires_at` will be converted to datetime
        $max = $this->peachPaymentsHelper->getCardExpirationDateForVault('2038', '00');
        if (strtotime($expirationDate) > strtotime($max)) {
            return $max;
        }
        return $expirationDate;
    }
}
