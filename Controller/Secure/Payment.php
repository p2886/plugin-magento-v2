<?php
/**
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

namespace PeachPayments\Hosted\Controller\Secure;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\LayoutFactory;
use PeachPayments\Hosted\Helper\Data as HelperData;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\Web\Hooks;
use PeachPayments\Hosted\Model\Web\HooksFactory;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use PeachPayments\Hosted\Command\Order\Process as OrderProcessor;

class Payment extends AbstractSecure implements CsrfAwareActionInterface
{
    /**
     * @var HelperData
     */
    protected $helperData;
    /**
     * @var JsonSerializer
     */
    private $jsonSerializer;
    /**
     * @var OrderProcessor
     */
    private $orderProcessor;

    /**
     * @param Context $context
     * @param HooksFactory $webHooksFactory
     * @param LayoutFactory $viewLayoutFactory
     * @param HelperData $helperData
     * @param JsonSerializer $jsonSerializer
     * @param OrderProcessor $orderProcessor
     * @param UnifiedLogger $unifiedLogger
     */
    public function __construct(
        Context $context,
        HooksFactory $webHooksFactory,
        LayoutFactory $viewLayoutFactory,
        HelperData $helperData,
        JsonSerializer $jsonSerializer,
        OrderProcessor $orderProcessor,
        private UnifiedLogger $unifiedLogger
    ) {
        $this->helperData = $helperData;
        $this->jsonSerializer = $jsonSerializer;
        $this->orderProcessor = $orderProcessor;
        parent::__construct($context, $webHooksFactory, $viewLayoutFactory);
    }

    /**
     * Payment action
     */
    public function execute()
    {
        try {
            $incrementId = $this->getRequest()->getParam('merchantTransactionId');
            $webHookM = $this->getWebHook()->loadByIncrementId($incrementId);

            if (!$webHookM->getId()) {
                throw new \Exception('PEACH PAYMENTS ERROR: No webhook id.');
            }

            $params = $this->getRequest()->getParams();
            if (isset($params['customParameters'])) {
                unset($params['customParameters']);
            }

            $this->unifiedLogger->debug(
                [
                    'message' => 'Hosted payment redirect request received',
                    'request' => json_encode($params)
                ]
            );

            $insert = $this->helperData->mapWebhookData($params);
            foreach ($insert as $key => $param) {
                $webHookM->setData($key, $param);
            }

            $webHookM->setData(
                'request',
                $this->jsonSerializer->serialize($insert)
            );

            $webHookM->save();

            $this->orderProcessor->execute($webHookM);

            if ($this->isSuccessful($webHookM->getData('result_code'))) {
                return $this->_redirect('checkout/onepage/success');
            }

            if ($this->isWaiting($webHookM->getData('result_code'))) {
                return $this->_redirect('*/*/wait', ['real_order_id' => $incrementId]);
            }
        } catch (\Exception $exception) {
            $this->helperData->restoreQuote();
            return $this->_redirect('checkout/cart');
        }

        $this->helperData->restoreQuote();
        return $this->_redirect('checkout/cart');
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
