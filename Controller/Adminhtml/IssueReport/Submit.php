<?php
/**
 * @author    X2Y Development team <dev@x2y.io>
 * @copyright 2024 X2Y Development team
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Controller\Adminhtml\IssueReport;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\DataObject;
use PeachPayments\Hosted\Api\Data\IssueReporterInterface;
use PeachPayments\Hosted\Api\Data\IssueReporterInterfaceFactory;
use PeachPayments\Hosted\Api\IssueReporterRepositoryInterface;
use PeachPayments\Hosted\Logger\UnifiedLogger;
use PeachPayments\Hosted\Model\ResourceModel\IssueReporter\Collection;
use PeachPayments\Hosted\Model\ResourceModel\IssueReporter\CollectionFactory;

class Submit extends Action implements HttpPostActionInterface
{
    /**
     * @var IssueReporterInterfaceFactory
     */
    private $issueReporterFactory;

    /**
     * @var IssueReporterRepositoryInterface
     */
    private $issueReporterRepository;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param IssueReporterInterfaceFactory $issueReporterFactory
     * @param IssueReporterRepositoryInterface $issueReporterRepository
     * @param CollectionFactory $collectionFactory
     * @param UnifiedLogger $unifiedLogger
     * @param Context $context
     */
    public function __construct(
        IssueReporterInterfaceFactory $issueReporterFactory,
        IssueReporterRepositoryInterface $issueReporterRepository,
        CollectionFactory $collectionFactory,
        Context $context,
        private UnifiedLogger $unifiedLogger
    ) {
        parent::__construct($context);
        $this->request = $context->getRequest();
        $this->issueReporterFactory = $issueReporterFactory;
        $this->issueReporterRepository = $issueReporterRepository;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if (count($this->getIssueReporterItems()) > 0) {
            $message = 'Your report has already been submitted. Please wait a few minutes before trying again.';

            return $resultJson->setData($this->createJsonResponse($message, false));
        }

        $copyTo = $this->request->getParam('copyTo');
        $problemDescription = $this->request->getParam('problem');

        try {
            $issueReporter = $this->createIssueReporter($problemDescription, $copyTo);
            $this->issueReporterRepository->save($issueReporter);
            $result = $this->createJsonResponse();
        } catch (Exception $e) {
            $message = 'Error during saving report. Please check logs for more details.';
            $this->unifiedLogger->error(__('%1 Exception: %2', $message, $e->getMessage()));
            $result = $this->createJsonResponse($message, false);
        }

        return $resultJson->setData($result);
    }

    /**
     * @return DataObject[]
     */
    private function getIssueReporterItems(): array
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter(IssueReporterInterface::DATE_SENT, ['null' => true]);

        return $collection->getItems();
    }

    /**
     * @param string|null $problemDescription
     * @param string|null $copyTo
     * @return IssueReporterInterface
     */
    private function createIssueReporter(?string $problemDescription, ?string $copyTo): IssueReporterInterface
    {
        $issueReporter = $this->issueReporterFactory->create();
        $issueReporter->setDescription($problemDescription);
        $issueReporter->setCopyTo($copyTo);

        return $issueReporter;
    }

    /**
     * @param bool $status
     * @param string|null $message
     * @return array
     */
    private function createJsonResponse(?string $message = null, bool $status = true): array
    {
        $response = ['success' => $status];

        if ($message !== null) {
            $response['message'] = $message;
        }

        return $response;
    }
}
