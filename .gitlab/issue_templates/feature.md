/assign @

/milestone %

/estimate 

/due 

/label ~Feature

/label ~"Needs Estimate"

/label ~"Needs Test Cases"

/label ~

# Overview
_Describe general purpose of this feature._



# Assets (UX/UI)
_Include any mockups, designs, diagrams, or visual resources._



# Requirements
_Describe functional requirements of how this feature will work._



# Configurations
_Describe any configurations that need to be set or updated._



# Resources
_Include any additional resources that will help in development of this feature._



# Test Cases (QA)
_Write out test cases to cover business and functional requirements._



# Out of Scope
_Describe any out of scope functionality of this feature._



# Future Scope
_Describe any future functionality that is planned for development of this feature at a later phase._



# Business Impact
_(Optional) Describe what is the business impact of this feature. How does this feature benefit the business? What is the problem we are trying to solve? What is the need? Is this feature for an internal or external target audience?_



# Open Questions
_Include any open question pertaining to this feature._

| Question | Answer |
| ------ | ------ |
|  ....  |  ....  |
