/assign @

/milestone %

/estimate 

/due 

/label ~Bug 

/label ~Backlog 

/label ~"Needs Estimate"

/label ~

# Steps to Reproduce
_Describe the steps to reproduce experienced bug._

1. 


# Actual Result
_What is the actual result?_



##### Browser Console Log
```
Paste log stack trace here if available...
```

##### Application Error Log
```
Paste log stack trace here if available...
```

##### Attachements
_Attach any visual proof i.e. screenshots, videos, artifacts, etc. if available._



# Expected Result
_What is the expected result?_



# Environment
_Populate the device, browser, and URL the bug was encountered if available._
- Device:
- Browser:
- Source URL:

# Impacted Areas
_Describe what areas of the application this bug is affecting if possible._



# Business Impact
_(Optional) Describe what is the business impact of this bug._

