<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

namespace PeachPayments\Hosted\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use PeachPayments\Hosted\Helper\Data as PeachPaymentsHostedHelper;
use Magento\Store\Model\StoreManagerInterface;

class Config extends AbstractHelper
{
    const XPATH_CONF = 'payment/peachpayments_hosted/';
    const HOST_LIVE = 'https://card.peachpayments.com/';
    const HOST_TEST = 'https://sandbox-card.peachpayments.com/';

    const API_LIVE = 'https://api.peachpayments.com/v1/checkout/';
    const API_TEST = 'https://testapi.peachpayments.com/v1/checkout/';
    const EMBEDDED_CHECKOUT = 'https://dashboard.peachpayments.com/';
    const EMBEDDED_CHECKOUT_TEST = 'https://sandbox-dashboard.peachpayments.com/';
    const CC_BRANDS = [
        'VI' => ['code' => 'VISA', 'label' => 'Visa'],
        'MC' => ['code' => 'MASTER', 'label' => 'MasterCard']
    ];
    const BASE_API_URL_LIVE = 'https://secure.peachpayments.com/';
    const BASE_API_URL_TEST = 'https://testsecure.peachpayments.com/';
    private StoreManagerInterface $storeManager;

    /**
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
    }


    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isServerToServerEnabled($storeId = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'peachpayments_server_to_server/active',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        ) && $this->isPeachPaymentsActive($storeId);
    }

    /**
     * @return bool
     */
    public function isEmbeddedCheckoutEnabled($storeId = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'enable_embedded_checkout',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        ) && $this->isPeachPaymentsActive($storeId);
    }

    public function isBuyNowButtonEnabled($storeId = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'buy_now_button/enable',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        ) && $this->isPeachPaymentsActive($storeId);
    }

    public function isBuyWithPeachButtonEnabled($storeId = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'buy_now_button/enable_plp',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        ) && $this->isPeachPaymentsActive($storeId);
    }

    /**
     * @param $storeId
     * @return string
     */
    public function getPayWithPeachMethods($storeId = null): string
    {
        return $this->scopeConfig->getValue(
            self::XPATH_CONF . 'buy_now_button/methods',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @return bool
     */
    public function isOnlyForSubscription($storeId = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'peachpayments_server_to_server/subs_only',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @return string
     */
    public function getHostUrl($storeId = null)
    {
        return $this->isLiveMode($storeId) ? self::HOST_LIVE : self::HOST_TEST;
    }

    public function getApiUrl(string $loc = '', $storeId = null)
    {
        return ($this->isLiveMode($storeId) ? self::API_LIVE : self::API_TEST) . $loc;
    }

    /**
     * @return string
     */
    public function getBaseApiUrl($storeId = null)
    {
        return $this->isLiveMode($storeId) ? self::BASE_API_URL_LIVE : self::BASE_API_URL_TEST;
    }

    public function getAuthenticationApiUrl($storeId = null): string
    {
        return ($this->isLiveMode($storeId) ? self::EMBEDDED_CHECKOUT : self::EMBEDDED_CHECKOUT_TEST) .
            'api/oauth/token';
    }

    /**
     * @return string
     */
    public function getAccessToken($storeId = null): string
    {
        return (string) $this->scopeConfig->getValue(
            self::XPATH_CONF . 'token',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @param string $registrationId
     * @return string
     */
    public function getApiRegistrationUri(string $registrationId)
    {
        return sprintf('%sv1/registrations/%s/payments', $this->getHostUrl(), $registrationId);
    }

    /**
     * @param string $registrationId
     * @param string $entityId
     * @return string
     */
    public function getApiDeleteRegistrationUri(string $registrationId, string $entityId)
    {
        return sprintf('%sv1/registrations/%s?entityId=%s', $this->getHostUrl(), $registrationId, $entityId);
    }

    /**
     * @param string|null $authorizationTrxId
     * @return string
     */
    public function getApiPaymentsUri(string $authorizationTrxId = null)
    {
        if (null !== $authorizationTrxId) {
            $authorizationTrxId = '/' . $authorizationTrxId;
        }

        return sprintf('%sv1/payments%s', $this->getHostUrl(), $authorizationTrxId ?? '');
    }

    /**
     * @param string $code
     * @return string
     */
    public function getPaymentBrandByPeachPaymentsCode(string $code)
    {
        foreach (self::CC_BRANDS as $brand => $data) {
            if ($data['code'] == $code) {
                return $brand;
            }
        }

        return '';
    }

    /**
     * @return bool
     */
    public function isLiveMode($storeId = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'mode',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getEntityId3DSecure($storeId = null)
    {
        if ($this->isLiveMode($storeId)) {
            return $this->scopeConfig->getValue(
                self::XPATH_CONF . 'entity_id_theed_secure',
                ScopeInterface::SCOPE_STORE,
                $storeId ?? $this->storeManager->getStore()->getId()
            );
        }

        return $this->scopeConfig->getValue(
            self::XPATH_CONF . 'entity_id_theed_secure_sandbox',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @param string $id
     * @return string
     */
    public function getCheckoutsUri(string $id = '')
    {
        return $id ? $this->getHostUrl() . 'v1/checkouts/' . $id . '/payment' :
            $this->getHostUrl() . 'v1/checkouts';
    }

    public function getCheckoutUrlV2(): string
    {
        return $this->getBaseApiUrl() . 'v2/checkout';
    }

    /**
     * @return string
     */
    public function getDecryptionKey($storeId = null)
    {
        return (string) $this->scopeConfig->getValue(
            self::XPATH_CONF . 'webhook_decryption_key',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @return string
     */
    public function getPaymentFormUrl(): string
    {
        return $this->getHostUrl() . 'v1/paymentWidgets.js?checkoutId=';
    }

    /**
     * @see https://developer.peachpayments.com/reference/post_checkout
     *
     * @param string $code
     * @return string
     */
    public function getIsForceDefaultMethod(string $code, $storeId = null): string
    {
        $selected = (string) $this->scopeConfig->getValue(
            self::XPATH_CONF . 'force_default_methods',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );

        if (strpos(strtolower($selected), strtolower($code)) !== false) {
            return 'true';
        }

        return 'false';
    }

    public function isConsolidatedPayments($storeId = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'enable_consolidated_payments',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        ) && $this->isPeachPaymentsActive($storeId);
    }

    /**
     * @return string
     */
    public function getAvailablePaymentMethodsApiUrl($storeId = null): string
    {
        return $this->isLiveMode($storeId)
            ? self::BASE_API_URL_LIVE
            : self::BASE_API_URL_TEST
            . 'merchant_specs';
    }

    public function getClientId($storeId = null): string
    {
        return $this->scopeConfig->getValue(
            self::XPATH_CONF . ($this->isLiveMode($storeId) ? 'client_id' : 'client_id_sandbox'),
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    public function getClientSecret($storeId = null): string
    {
        return $this->scopeConfig->getValue(
            self::XPATH_CONF . ($this->isLiveMode($storeId) ? 'client_secret' : 'client_secret_sandbox'),
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    public function getMerchantId($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XPATH_CONF . 'merchant_id',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    public function getIsSendOrderEmail($storeId = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'send_order_email',
            ScopeInterface::SCOPE_STORE,
            $storeId ?? $this->storeManager->getStore()->getId()
        );
    }

    public function getIsSendInvoiceEmail($storeId = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'send_invoice_email',
            ScopeInterface::SCOPE_STORE,
            $storeId ?: $this->storeManager->getStore()->getId()
        );
    }

    private function isPeachPaymentsActive($storeId = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XPATH_CONF . 'active',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
