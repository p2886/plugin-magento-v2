<?php
/**
 * Copyright (c) 2020 Peach Payments. All rights reserved. Developed by Francois Raubenheimer
 */

declare(strict_types=1);

namespace PeachPayments\Hosted\Helper;

use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Exception\InvalidArgumentException;
use Magento\Framework\HTTP\ClientFactory;
use Magento\Framework\UrlInterface;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\ScopeInterface;
use PeachPayments\Hosted\Dictionary\WebhookEnum;
use PeachPayments\Hosted\Model\Web\Hooks;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    /**
     * @param Context $context
     * @param ScopeConfigInterface $storeConfig
     * @param RequestInterface $appRequestInterface
     * @param UrlInterface $urlBuilder
     * @param OrderFactory $modelOrderFactory
     * @param QuoteFactory $modelQuoteFactory
     * @param ClientFactory $httpClientFactory
     * @param EventManager $eventManager
     * @param Config $config
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Context $context,
        private ScopeConfigInterface $storeConfig,
        private RequestInterface $appRequestInterface,
        private UrlInterface $urlBuilder,
        private OrderFactory $modelOrderFactory,
        private QuoteFactory $modelQuoteFactory,
        private ClientFactory $httpClientFactory,
        private EventManager $eventManager,
        private Config $config,
        private CheckoutSession $checkoutSession,
        private StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
    }

    const CHECKOUT_LIVE = 'https://secure.peachpayments.com/checkout';
    const CHECKOUT_TEST = 'https://testsecure.peachpayments.com/checkout';
    const PLATFORM = 'MAGENTO';
    const XML_CONF = 'payment/peachpayments_hosted/';

    /** @var array */
    private $sandboxVariables = [
        'entity_id',
        'sign_key',
    ];

    /**
     * @param $path
     * @return bool|string
     */
    private function getConfig($path)
    {
        if (!$this->getMode() && in_array($path, $this->sandboxVariables)) {
            $path .= '_sandbox';
        }

        return $this->storeConfig->getValue(
            self::XML_CONF . $path,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @return bool
     */
    private function getMode()
    {
        return $this->storeConfig->isSetFlag(
            self::XML_CONF . 'mode',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @return string
     */
    public function getCheckoutUrl()
    {
        return $this->getMode() ? self::CHECKOUT_LIVE : self::CHECKOUT_TEST;
    }

    /**
     * @return string
     */
    public function getWaitingUrl()
    {
        $orderId = $this->appRequestInterface->getParam('id');
        return $this->urlBuilder->getUrl('peachpayments_hosted/secure/payment', ['merchantTransactionId' => $orderId]);
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->getConfig('entity_id');
    }

    /**
     * @return string
     */
    private function getSignKey()
    {
        return $this->getConfig('sign_key');
    }

    /**
     * @return string
     */
    public function getPlatformName()
    {
        return self::PLATFORM;
    }

    /**
     * @param array $data unsigned data
     * @param bool $includeNonce
     *
     * @return array signed data
     * @throws Exception
     */
    public function signData($data = [], $includeNonce = true)
    {
        if (count($data) === 0) {
            throw new InvalidArgumentException(__('Error: Sign data can not be empty'));
        }

        if (!function_exists('hash_hmac')) {
            throw new InvalidArgumentException(__('Error: hash_hmac function does not exist'));
        }

        if ($includeNonce && strlen($this->getUuid()) === 0) {
            throw new InvalidArgumentException(__('Error: Nonce can not be empty, something went horribly wrong'));
        }

        if ($includeNonce) {
            $data = array_merge($data, ['nonce' => $this->getUuid()]);
        }

        $tmp = [];
        foreach ($data as $key => $datum) {
            // NOTE: Zend framework s/./_/g fix
            $tmp[str_replace('_', '.', $key)] = $datum;
        }

        ksort($tmp, SORT_STRING);

        $signDataRaw = '';
        foreach ($tmp as $key => $datum) {
            if ($key !== 'signature') {
                // NOTE: Zend framework s/./_/g fix
                $signDataRaw .= str_replace('_', '.', $key) . $datum;
            }
        }

        $signData = hash_hmac('sha256', $signDataRaw, $this->getSignKey());

        return array_merge($data, ['signature' => $signData]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getUuid()
    {
        $data = random_bytes(16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * Restore last active quote based on checkout session
     *
     * @return bool True if quote restored successfully, false otherwise
     * @throws Exception
     */
    public function restoreQuote()
    {
        $order = $this->checkoutSession->getLastRealOrder();
        if ($order->getId()) {
            $quote = $this->getQuoteById($order->getQuoteId());
            if ($quote->getId()) {
                return $this->checkoutSession->restoreQuote();
            }
        }
        return false;
    }

    /**
     * Return sales quote instance for specified ID
     *
     * @param int $quoteId Quote identifier
     * @return \Magento\Quote\Model\Quote
     */
    protected function getQuoteById($quoteId)
    {
        return $this->modelQuoteFactory->create()->load($quoteId);
    }

    /**
     * @param array $payload
     * @param Hooks $webhook
     * @return void
     */
    public function mapDataFromWebhookPayload(Hooks $webhook, array $payload): void
    {
        $customer = ['givenName' => '', 'surname' => ''];
        if (isset($payload['customer']['givenName'], $payload['customer']['surname'])) {
            $customer['givenName'] = $payload['customer']['givenName'];
            $customer['surname'] = $payload['customer']['surname'];
        }

        $webhook->setData(WebhookEnum::AMOUNT->value, $payload['amount'] ?? $payload['presentationAmount'] ?? '');
        $webhook->setData(WebhookEnum::CURRENCY->value, $payload['currency'] ?? $payload['presentationCurrency'] ?? '');
        $webhook->setData(WebhookEnum::CHECKOUT_ID->value, $payload['id']);
        $webhook->setData(WebhookEnum::MERCHANT_NAME->value, $customer['givenName'] . ' ' . $customer['surname']);
        $webhook->setData(WebhookEnum::PAYMENT_BRAND->value, $payload['paymentBrand'] ?? '');
        $webhook->setData(WebhookEnum::PAYMENT_TYPE->value, $payload['paymentType'] ?? '');
        $webhook->setData(WebhookEnum::RESULT_CODE->value, $payload['result']['code']);
        $webhook->setData(WebhookEnum::RESULT_DESCRIPTION->value, $payload['result']['description']);

        if (isset($payload['merchantTransactionId'])) {
            $webhook->setData(WebhookEnum::ORDER_INCREMENT_ID->value, $payload['merchantTransactionId']);
        }
    }

    /**
     * Map request data to webhook object keys
     *
     * @param array $data
     * @return array
     */
    public function mapWebhookData(array $data): array
    {
        $result = [];
        foreach ($data as $key => $datum) {
            if ($key === 'id') {
                $result['peach_id'] = $datum;
                $data['transaction_id'] = $datum;
            } else {
                $result[$this->convertWebhookDataKey($key)] = $datum;
            }
        }

        return $result;
    }

    /**
     * Convert webhook object key
     *
     * @param string $key
     * @return string
     */
    private function convertWebhookDataKey(string $key): string
    {
        return strtolower(
            preg_replace(
                ['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'],
                '$1_$2',
                str_replace('.', '_', $key)
            )
        );
    }

    public function getCardExpirationDateForVault(string $expiryYear, string $expiryMonth): string
    {
        $expDate = new \DateTime(
            $expiryYear
            . '-'
            . $expiryMonth
            . '-'
            . '01'
            . ' '
            . '00:00:00',
            new \DateTimeZone('UTC')
        );
        $expDate->add(new \DateInterval('P1M'));

        return $expDate->format('Y-m-d 00:00:00');
    }
}
